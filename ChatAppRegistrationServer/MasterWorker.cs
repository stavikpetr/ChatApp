﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppRegistrationServer
{
    class MasterWorker
    {
        public Mailer mailer { get; private set; }
        public Logger logger { get; private set; }
        public MessageReporter messageReporter { get; private set; }
        public KeyGenerator keyGenerator { get; private set; }
        public DbWorker dbWorker { get; private set; }

        static MasterWorker mf = new MasterWorker();
        MasterWorker()
        {
            mailer = Mailer.mailer;
            logger = Logger.logger;
            messageReporter = MessageReporter.messageReporter;
            keyGenerator = KeyGenerator.keyGenerator;
            dbWorker = DbWorker.dbWorker;
        }
        internal static MasterWorker masterWorker { get { return mf; } }
    }
}
