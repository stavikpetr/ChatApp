﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppRegistrationServer
{
    class MessageReporter
    {
        MessageReporter() { }
        static MessageReporter mr = new MessageReporter();
        internal static MessageReporter messageReporter { get { return mr; } }

        public string ERR_REG_WrongUsername(string name)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ERR Sorry, but provided username ");
            sb.Append(name);
            sb.Append(" is already taken");
            return sb.ToString();
        }

        public string OK_REG_Successful(string name)
        {
            return "OK registration was successful, proceed with validation";
        }

        public string OK_VAL_Successful(string key)
        {
            return "OK validation was successful";
        }

        public string ERR_VAL_WrongKey()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ERR Sorry, but provided key doesn't match any username");
            return sb.ToString();
        }

        public string ERR_MAILERROR()
        {
            return "ERR Sorry, error with sending mail to given address";
        }

        public string ERR_DBERROR()
        {
            return "ERR Sorry, database error, please try again later";
        }
    }
}
