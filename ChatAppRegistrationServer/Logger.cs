﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ChatAppRegistrationServer
{
    class Logger
    {
        static Logger l = new Logger();
        Logger() { }
        internal static Logger logger { get { return l; } }
        TextWriter tw = Console.Out;       

        internal void Log(string msg)
        {         
            tw.WriteLine(msg);
        }
    }
}
