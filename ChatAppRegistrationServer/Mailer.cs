﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Mail;
using System.Net;

namespace ChatAppRegistrationServer
{
    class Mailer
    {
        static Mailer m = new Mailer();
        internal static Mailer mailer { get { return m; } }

        MailAddress senderAddress;
        const string senderPassword = "q7w8e9";
        const string mailSubect = "Regstration Key";
        SmtpClient client;      

        public async Task<bool> SendMailWithKeyAsync(string key, string receiver)
        {
            return await Task.Factory.StartNew<bool>(() =>
            {
                try
                {
                    var y = Thread.CurrentThread.ManagedThreadId;
                    MailAddress receiverAddress = new MailAddress(receiver);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Hello, thank you for your registration on ChatApp!");
                    sb.Append(" Here is your key, use it for validation of your account.");
                    sb.Append("\n\n");
                    sb.Append("Key: ");
                    sb.Append(key);

                    MailMessage message = new MailMessage(senderAddress, receiverAddress);
                    message.Subject = mailSubect;
                    message.Body = sb.ToString();

                    client.Send(message);
                    return true;
                }
                catch (SmtpException)
                {
                    return false;
                }
            });
        }     

        public bool SendMailWithKey(string key, string receiver)
        {
            try
            {
                MailAddress receiverAddress = new MailAddress(receiver);
                StringBuilder sb = new StringBuilder();
                sb.Append("Hello, thank you for your registration on ChatApp!");
                sb.Append(" Here is your key, use it for validation of your account.");
                sb.Append("\n\n");
                sb.Append("Key: ");
                sb.Append(key);

                MailMessage message = new MailMessage(senderAddress, receiverAddress);
                message.Subject = mailSubect;
                message.Body = sb.ToString();

                client.Send(message);
                return true;
            }
            catch (SmtpException e)
            {
                return false;
            }
        }

        Mailer()
        {
            senderAddress = new MailAddress("chatappregistration@seznam.cz", "ChatApp reg. server");
            client = new SmtpClient
            {
                Host = "smtp.seznam.cz",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderAddress.Address, senderPassword)
            };
        }
    }
}


#region commentedOut

/*Ok, tohle si zaslouží komentář: momentálně řeším situaci že client se nejdříve chce registrovat
        * ,ale nastane nějaká smtp exception, samozřejmě, já prostě musím teď udělat dvě operace, sice
        * poslat maila a pak také updatnout db, ale musím počkat, jestli se tyto operace OBĚ provedou správně,
        * tedy abych klinetovi mohl poslat OK message, tak musí obě doběhnout dobře,
        * 
        * což, pohoda, prostě zabálím update db do transakce, provedu ho, pak provedu poslání mailu
        * a pokud poslání mailu bylo ok, tak transakci commitnu
        * 
        * Jenže, mně se nelíbilo, že jak jsem to měl předtím, tak mi tahle metoda vracela Task<bool>
        * a říkal jsem si, že je to přece zbytečný, pokud tady budu mít await, a vznikne mi tady
        * smtp exception, což indikuje špatné poslání emailu, tak se vyšíří ven a zachytí se v hlavní
        * smyčce a správně se obslouží, ale neuvědomil jsem si, že pokud to skutečně dělám takto, tak
        * jsem v tom kódu zapomněl zavírat connection a dořešit tu transakci... 
        * 
        * ale, kdybych tedy někdy potřeboval jenom takovej "void async task, tak se dá udělat
        * tak jak jej tady mám 
        *  */


//public async Task SendMailWithKeyAsync(string key, string receiver)
//{
//    var x = Thread.CurrentThread.ManagedThreadId;
//    await Task.Factory.StartNew(() =>
//    {
//        var y = Thread.CurrentThread.ManagedThreadId;
//        MailAddress receiverAddress = new MailAddress(receiver);
//        StringBuilder sb = new StringBuilder();
//        sb.Append("Hello, thank you for your registration on ChatApp!");
//        sb.Append(" Here is your key, use it for validation of your account.");
//        sb.Append("\n\n");
//        sb.Append("Key: ");
//        sb.Append(key);

//        MailMessage message = new MailMessage(senderAddress, receiverAddress);
//        message.Subject = mailSubect;
//        message.Body = sb.ToString();

//        client.Send(message);
//    });

#endregion