﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using ChatAppCommon;

namespace ChatAppRegistrationServer
{
    class Program
    {
        static void Main(string[] args)
        {
            IPEndPoint localEP = new IPEndPoint(CommonData.RegServer_IpAdress, CommonData.RegServer_ListeningPort);
            RegistrationServer rs = new RegistrationServer(localEP);
            rs.StartProcessing();
        }                
    }   
}
