﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppRegistrationServer
{
    class KeyGenerator
    {
        static KeyGenerator kg = new KeyGenerator();
        KeyGenerator() { }
        internal static KeyGenerator keyGenerator { get { return kg; } }

        private const int keyLength = 32;
        private const string charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        internal Task<string> NewKeyAsync()
        {
            return Task.Factory.StartNew<string>(() =>
            {
                StringBuilder keySB = new StringBuilder();
                var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
                var buffer = new byte[keyLength];
                rng.GetBytes(buffer);
                for (int i = 0; i < keyLength; i++)
                {
                    keySB.Append(charSet[buffer[i] % charSet.Length]);
                }

                return keySB.ToString();
            });
        }

        internal string NewKey()
        {
            StringBuilder keySB = new StringBuilder();
            var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            var buffer = new byte[keyLength];
            rng.GetBytes(buffer);
            for (int i = 0; i < keyLength; i++)
            {
                keySB.Append(charSet[buffer[i] % charSet.Length]);
            }

            return keySB.ToString();
        }
    }
}
