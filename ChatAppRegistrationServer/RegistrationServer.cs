﻿//#define LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections.Concurrent;
using ChatAppCommon;
using System.Data.SqlClient;
using System.Net.Mail;

namespace ChatAppRegistrationServer
{
    class RegistrationServer
    {
        MasterWorker masterWorker;
        BlockingCollection<Tuple<MessageID, ClientContext>> taskQueue;
        Socket listener;

        int connectedClients;
        const int timeoutPeriod = 300000; //not implemented
        const string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;Initial Catalog=Northwind;Integrated Security=True;Pooling=True";

        public void StartProcessing()
        {
#if LOG
            Console.WriteLine($"main thread id: {Thread.CurrentThread.ManagedThreadId}");
#endif
            listener.Listen(0);
            NewAcceptingClientTask(listener);

#if LOG
            Console.WriteLine("starting the while(true) loop");
#endif
            while (true)
            {
                var resultTask = taskQueue.Take();
                switch (resultTask.Item1)
                {
                    case MessageID.NEWCONNECTION:
                        connectedClients++;
#if LOG
                        masterWorker.logger.Log($"new client connected, number of connected clients: {connectedClients}");
#endif
                        NewAcceptingClientTask(listener);
                        NewClientServingTask(resultTask.Item2);
                        break;
                    case MessageID.OK:
#if LOG
                        masterWorker.logger.Log("client was served ok");
#endif
                        NewClientServingTask(resultTask.Item2);
                        break;
                    case MessageID.FATAL:
                        connectedClients--;
#if LOG
                        masterWorker.logger.Log($"fatal error during client serving, shutting him down, num of connected clients: {connectedClients}");
#endif
                        resultTask.Item2.socket.Close();
                        break;

                    case MessageID.END:
                        connectedClients--;
#if LOG
                        masterWorker.logger.Log($"client is ending, number of connected clients now: {connectedClients}");
#endif
                        resultTask.Item2.socket.Close();
                        break;
                    case MessageID.DISCONNECTED:
                        //this is probably nonsense... 
                        //in case i would like to implement it, its easy...
                        //.http://stackoverflow.com/questions/4238345/asynchronously-wait-for-taskt-to-complete-with-timeout
                        connectedClients--;
#if LOG
                        masterWorker.logger.Log($"disconnecting client due to inactivity for {timeoutPeriod / 60 / 1000} mins");
#endif
                        resultTask.Item2.socket.Close();
                        break;
                }
            }
        }

        private async Task NewClientServingTask(ClientContext clientContext)
        {
            try
            {
                string input = await clientContext.input.ReadLineAsync();
                if (input.StartsWith("REG"))
                {
                    //[0] = REG
                    //[1] = username
                    //[2] = password
                    //[3] = email
                    var usersTable = masterWorker.dbWorker.GetUsersAsync();
                    var newKey = masterWorker.keyGenerator.NewKeyAsync();

                    string[] tokens = input.Split(' ');

                    bool username = await FindOutAboutUsernameAsync(tokens[1], await usersTable);
                    if (username) // username == true -> username is not taken
                    {
                        await masterWorker.dbWorker.InsertNewUserAndSendMailAsync(tokens[1], tokens[2], tokens[3], await newKey, MasterWorker.masterWorker.mailer);
                        //pokud je něco špatně tak by tady sama o sobě měla vzniknout vyjímka...
                        await SendMessage(MessageID.OK, clientContext, masterWorker.messageReporter.OK_REG_Successful(tokens[1]));
                        taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.OK, clientContext));
                    }
                    else
                    {
                        await SendMessage(MessageID.OK, clientContext, masterWorker.messageReporter.ERR_REG_WrongUsername(tokens[1]));
                        taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.OK, clientContext));
                    }
                }
                else if (input.StartsWith("VAL"))
                {
                    var usersTable = masterWorker.dbWorker.GetUsersAsync();
                    string[] tokens = input.Split(' ');

                    string keyExists = await FindOutAbouKeyAsync(tokens[1], await usersTable); //keyExists == null -> null then such key doesn't exist in db
                    if (keyExists != null)
                    {
                        await masterWorker.dbWorker.UpdateValidationStatusAsync(keyExists);
                        await SendMessage(MessageID.OK, clientContext, masterWorker.messageReporter.OK_VAL_Successful(tokens[1]));
                        taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.OK, clientContext));
                    }
                    else
                    {
                        await SendMessage(MessageID.OK, clientContext, masterWorker.messageReporter.ERR_VAL_WrongKey());
                        taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.OK, clientContext));
                    }
                }
                else if (input.StartsWith("END"))
                {
                    clientContext.input.Close();
                    clientContext.output.Close();
                    taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.END, clientContext));
                }
                else
                {
#if LOG
                    masterWorker.logger.Log("undefined message type from client, shutting him down");
#endif
                    clientContext.input.Close();
                    clientContext.output.Close();
                    taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.FATAL, clientContext));
                }
            }
            catch (IOException e)
            {
#if LOG
                masterWorker.logger.Log("caught IO exception");
#endif
                taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.FATAL, clientContext));
            }
            catch (SmtpException)
            {
#if LOG
                masterWorker.logger.Log("caught smtp exception");
#endif
                await SendMessage(MessageID.OK, clientContext, masterWorker.messageReporter.ERR_MAILERROR());
                taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.OK, clientContext));

            }
            catch (SqlException e)
            {
#if LOG
                masterWorker.logger.Log($"caught db exception: {e.Message}");
#endif
                await SendMessage(MessageID.OK, clientContext, masterWorker.messageReporter.ERR_DBERROR());
                taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.OK, clientContext));
            }
            catch (SocketException)
            {
#if LOG
                masterWorker.logger.Log("caught socket exception");
#endif
                clientContext.input.Close();
                clientContext.output.Close();
                taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.FATAL, clientContext));
            }

            catch (Exception e)
            {
#if LOG
                masterWorker.logger.Log($"caught general exception, {e.Message}");
#endif
                clientContext.input.Close();
                clientContext.output.Close();
                taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.FATAL, clientContext));
            }
        }
               

        private async Task SendMessage(MessageID messageID, ClientContext clientContext, string message)
        {
            await clientContext.output.WriteLineAsync(message);
            await clientContext.output.FlushAsync();
        }

        private async Task<string> FindOutAbouKeyAsync(string key, SqlDataReader reader)
        {
            while (reader.ReadAsync().Result != false)
            {
                string dbKey = await reader.GetFieldValueAsync<string>(3);
                if (key == dbKey)
                {
                    var x = await reader.GetFieldValueAsync<string>(0);
                    reader.Close();
                    return x;
                }
            }
            reader.Close();
            return null;
        }

        //return true if username DOESN'T EXISTS
        //this method is called during registration - i need to find out if this username is taken
        private async Task<bool> FindOutAboutUsernameAsync(string name, SqlDataReader reader)
        { 
            while (reader.ReadAsync().Result != false)
            {
                string dbName = await reader.GetFieldValueAsync<string>(0);
                if (dbName.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    reader.Close();
                    return false;
                }
            }
            reader.Close();
            return true;
        }

        private async Task NewAcceptingClientTask(Socket listener)
        {
            Socket s = await AsyncAccept(listener);

            NetworkStream ns = new NetworkStream(s);
            ClientContext cs = new ClientContext();
            cs.socket = s;
            cs.input = new StreamReader(ns);
            cs.output = new StreamWriter(ns);
            taskQueue.Add(new Tuple<MessageID, ClientContext>(MessageID.NEWCONNECTION, cs));                       
        }

        //the implementation of asyncAccept doesn't return Task<Socket>, hence, i need a small workaround
        private Task<Socket> AsyncAccept(Socket listener)
        {
            TaskCompletionSource<Socket> tcs = new TaskCompletionSource<Socket>();
            SocketAsyncEventArgs args = new SocketAsyncEventArgs();
            args.Completed += new EventHandler<SocketAsyncEventArgs>((_, e) =>
            {
                tcs.SetResult(args.AcceptSocket);
            });
    
            bool willRaiseEvent = listener.AcceptAsync(args);
            if (!willRaiseEvent)
            {
                tcs.SetResult(args.AcceptSocket);
            }

            return tcs.Task;
        }

        public RegistrationServer(IPEndPoint localEP)
        {
            masterWorker = MasterWorker.masterWorker;
            taskQueue = new BlockingCollection<Tuple<MessageID, ClientContext>>();
            listener = new Socket(SocketType.Stream, ProtocolType.Tcp);
            listener.DualMode = true;
            listener.Bind(localEP);            
        }
    }
}