﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Data.SqlClient;

namespace ChatAppRegistrationServer
{
    class DbWorker
    {
        static DbWorker dw = new DbWorker();
        DbWorker() { }
        internal static DbWorker dbWorker { get { return dw; } }
        const string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;Initial Catalog=ChatAppTesting;Integrated Security=True;Pooling=True";

        public async Task<SqlDataReader> GetUsersAsync()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            await connection.OpenAsync();
            SqlCommand command = new SqlCommand("select * from users", connection);
            return await command.ExecuteReaderAsync(System.Data.CommandBehavior.CloseConnection);
        }

        public async Task UpdateValidationStatusAsync(string login)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            await connection.OpenAsync();
            string commandText = $"UPDATE users set isValidated='true' where login = @login";
            SqlCommand command = new SqlCommand(commandText, connection);
            command.Parameters.AddWithValue("@login", login);

            await command.ExecuteNonQueryAsync();
            connection.Close();
            return;
        }

        /*This method will work like this: i want to do transaction to db with adding the row to table, but i need
        * to wait for the result of sending email, if it wasn't succesful, i want to rollback the transaction, so, i will
        * work as normal to the point of commit, after commit, i will wait for the task sendEmailTaskResult, if the result
        * of this task is true, i can continue without rollback, if false, i need to rollback the transaction and return false*/

        public async Task InsertNewUserAndSendMailAsync(string login, string password, string email, string key, Mailer mailer)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            await connection.OpenAsync();

            SqlTransaction transaction = connection.BeginTransaction();
            string commandText = $"INSERT INTO users (login, password, email, validationKey, isValidated) Values (@login, @password, @email, '{key}', '{false}');";
            SqlCommand command = new SqlCommand(commandText, connection, transaction);
            command.Parameters.AddWithValue("@login", login);
            command.Parameters.AddWithValue("@password", password);
            command.Parameters.AddWithValue("@email", email);

            await command.ExecuteNonQueryAsync();
            bool sendMailResult = await mailer.SendMailWithKeyAsync(key, email);

            //Poznámka: vyhození vyjímky by tady šlo řešit i přes try catch block a rethrow...
            // s tím, že bych to v Maileru udělal tak, jak je tam popsáno v komentáři
            if (sendMailResult)
            {
                transaction.Commit();
                connection.Close();
            }
            else
            {
                transaction.Rollback();
                connection.Close();
                throw new System.Net.Mail.SmtpException();
            }
            return;
        }
    }
}



