﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;

namespace ChatAppRegistrationServer
{
    public enum MessageID { OK, END, ERROR, DISCONNECTED, FATAL, NEWCONNECTION };
    public class ClientContext
    {
        public Socket socket { get; set; }
        public StreamReader input { get; set; }
        public StreamWriter output { get; set; }
    }
}
