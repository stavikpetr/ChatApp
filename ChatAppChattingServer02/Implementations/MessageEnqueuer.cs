﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class MessageEnqueuer : IMessageEnqueuer
    {
        IMessageWriter messageWriter;


        public MessageEnqueuer(IMessageWriter messageWriter)
        {
            this.messageWriter = messageWriter;
        }

        private Message buildMessage(StringBuilder sb, IConnectionContext connectionContext)
        {
            Message m = new Message
            {
                connectionContext = connectionContext,
                message = sb.ToString(),
                output = connectionContext.output
            };
            sb.Clear();
            return m;
        }

        public Task SendServerGroupsAsync(ICollection<Group> groups, IConnectionContext connectionContext, string prepender)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();         
                foreach (var group in groups)
                {
                    sb.Append(prepender);
                    sb.Append(MessageConstants.SERVER + MessageConstants.GROUP);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.groupName);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.type);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.admin);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.description);
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));
                }
            });
        }

        public Task SendUserGroupsAsync(ICollection<Group> groups, IConnectionContext connectionContext, string prepender)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                foreach (var group in groups)
                {
                    sb.Append(prepender);
                    sb.Append(MessageConstants.USER + MessageConstants.GROUP);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.groupName);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.type);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.admin);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(group.description);
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));
                }
            });
        }
        public Task SendFriendsInfoAsync(ICollection<UserWithStatus> friends, IConnectionContext connectionContext, string prepender)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                foreach (var friend in friends)
                {
                    sb.Append(prepender);
                    sb.Append(MessageConstants.FRIEND);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(friend.user);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(friend.status.ToString().ToLower());
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));
                }
            });
        }

        public Task SendFriendshipNotificationsAsync(ICollection<FriendshipNotification> notifications, IConnectionContext connectionContext)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                foreach (var n in notifications)
                {
                    sb.Append(MessageConstants.INIT);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(MessageConstants.FRIENDSHIP + MessageConstants.NOTIFICATION);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.type);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.sender);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.receiver);
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));
                }
            });
        }


        public Task SendGroupNotificationsAsync(ICollection<GroupNotification> notifications, IConnectionContext connectionContext)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                foreach (var n in notifications)
                {
                    sb.Append(MessageConstants.INIT);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(MessageConstants.GROUP + MessageConstants.NOTIFICATION);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.alias);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.groupName);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.type);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.sender);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(n.receiver);
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));
                }
            });
        }

        public Task SendSupportedCommandsAsnyc(ICollection<SupportedCommand> supportedCommands, IConnectionContext connectionContext)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();

                foreach (var c in supportedCommands)
                {
                    sb.Append(MessageConstants.INIT);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(MessageConstants.SERVER + MessageConstants.COMMAND);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(c.commandSignature);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(c.commandDescription);
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));
                }
            });
        }
        public void SendIncorrectCredentials(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message { connectionContext = connectionContext, output = connectionContext.output, message = "incorrect credentials error" });
        }
        public void SendSignOK(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message { connectionContext = connectionContext, output = connectionContext.output, message = MessageConstants.SIGN + MessageConstants.OK + MessageConstants.DELIMITER + StaticData.serverAlias });
        }
        public void SendAlreadyConnected(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message { connectionContext = connectionContext, output = connectionContext.output, message = "such user is already connected" });
        }
        public void SendInitPhaseString(IConnectionContext connectionContext, string user)
        {
            messageWriter.AddMessage(new Message
            {
                connectionContext = connectionContext,
                output = connectionContext.output,
                message = MessageConstants.INIT + MessageConstants.DELIMITER + MessageConstants.BEGIN + MessageConstants.DELIMITER + user
            });
        }
        public void SendInitPhaseOverString(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message
            {
                 connectionContext = connectionContext,
                 message = MessageConstants.INIT + MessageConstants.DELIMITER + MessageConstants.OVER,
                 output = connectionContext.output  
            });
        }

        //GROUPRELATED
        public void SendGroupCreateSuccess(string groupName, string type, string admin, string description, IConnectionContext connectionContext)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.GROUP + MessageConstants.NEW + MessageConstants.OK);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(groupName);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(type);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(admin);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(description);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append("group created successfuly");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));

        }

        public void SendGroupCreateFail(string groupName, IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message
            {
                connectionContext = connectionContext,
                output = connectionContext.output,
                message = MessageConstants.GROUP + MessageConstants.NEW + MessageConstants.ERR + MessageConstants.DELIMITER + "group create fail"
            });
        }

        public Task SendShowGroupUsersOKAsync(ICollection<UserWithStatus> usersInGroup, IConnectionContext connectionContext, string groupName)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(MessageConstants.GROUP);
                sb.Append(MessageConstants.SHOW);
                sb.Append(MessageConstants.OK);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupName);
                messageWriter.AddMessage(buildMessage(sb, connectionContext));

                foreach (var user in usersInGroup)
                {
                    sb.Append(MessageConstants.GROUP);
                    sb.Append(MessageConstants.SHOW);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(groupName);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(user.user);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(user.status.ToString().ToLower());
                    messageWriter.AddMessage(buildMessage(sb, connectionContext));

                }
            });
        }

        public void SendShowGroupUsersERR(IConnectionContext connectionContext, string groupName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.GROUP);
            sb.Append(MessageConstants.SHOW);
            sb.Append(MessageConstants.ERR);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(groupName);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append("group show fail");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        public void SendGroupJoinSuccess(IConnectionContext connectionContext, string groupName)
        {
            StringBuilder sb = buildGroupJoinResult(groupName, MessageConstants.OK, "group join OK");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        public void SendGroupJoinFailBeingDeleted(IConnectionContext connectionContext, string groupName)
        {
            StringBuilder sb = buildGroupJoinResult(groupName, MessageConstants.ERR, "group join fail, being deleted");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        private StringBuilder buildGroupJoinResult(string groupName, string result, string custom)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.GROUP);
            sb.Append(MessageConstants.JOIN);
            sb.Append(result);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(groupName);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(custom);
            return sb;
        }

        public void SendGroupJoinFailNonExisting(IConnectionContext connectionContext, string groupName)
        {
            StringBuilder sb = buildGroupJoinResult(groupName, MessageConstants.ERR, "group join fail, non existing");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        public void SendGroupNotification(IConnectionContext connectionContext, GroupNotificationType groupNotificationType, string groupName, string sender, string receiver, string alias)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(groupNotificationType.ToString());
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(groupName);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(sender);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(receiver);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(alias);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));

        }


        //FRIENDSHIP RELATED
        public void SendFriendshipNotification(IConnectionContext connectionContext, FriendshipNotificationType friendshipNotificationType, string sender, string receiver)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(friendshipNotificationType.ToString());
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(sender);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(receiver);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        //MESSAGES
        public void SendGroupMessage(IConnectionContext connectionContext, string sender, string groupName, string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.MESSAGE + MessageConstants.GROUP);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(groupName);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(sender);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(message);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));

        }

        public void SendFriendMessage(IConnectionContext connectionContext, string sender, string receiver, string message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.MESSAGE + MessageConstants.FRIEND);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(receiver);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(sender);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(message);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        private StringBuilder BuildSearchMessage(string result, string custom)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.SEARCH + result);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(custom);
            return sb;
        }
        //SEARCHES
        public void SendSearchResultOK(IConnectionContext connectionContext)
        {
            StringBuilder sb = BuildSearchMessage(MessageConstants.OK, "search success");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        public void SendSearchResultFail(IConnectionContext connectionContext)
        {
            StringBuilder sb = BuildSearchMessage(MessageConstants.ERR, "search fail");
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        private StringBuilder BuildChangedStatus(string what, string who, string status, string where)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(what);
            sb.Append(MessageConstants.CHANGEDSTATUS);
            sb.Append(MessageConstants.DELIMITER);
            if (where != null)
            {
                sb.Append(where);
                sb.Append(MessageConstants.DELIMITER);
            }
            sb.Append(who);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(status);
            return sb;
        }

        public void SendFriendChangedStatus(string userThatChangedStatus, IConnectionContext connectionContext, UserStatus status)
        {
            StringBuilder sb = BuildChangedStatus(MessageConstants.FRIEND, userThatChangedStatus, status.ToString().ToLower(), null);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }
        public void SendUserInGroupChangedStatus(string userThatChangedStatus, IConnectionContext connectionContext, UserStatus status, string groupName)
        {
            StringBuilder sb = BuildChangedStatus(MessageConstants.USER + MessageConstants.IN + MessageConstants.GROUP, userThatChangedStatus, status.ToString().ToLower(), groupName);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        public void SendServerGroupChangedStatus(string groupThatChangedStatus, IConnectionContext connectionContext, GroupState state, GroupType groupType, string admin, string description)
        {
            StringBuilder sb = BuildChangedStatus(MessageConstants.GROUP + MessageConstants.IN + MessageConstants.SERVER, groupThatChangedStatus, state.ToString().ToLower(), null);
            if (description != null)
            {
                //in this case, the group was just created and i need to send description aswell with admin name...
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupType.ToString());
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(admin);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(description);
            }
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
        }

        public void SendServerHello(IConnectionContext connectionContext, string myAlias)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(MessageConstants.SIGN);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(MessageConstants.SERVER);
            sb.Append(MessageConstants.DELIMITER);
            sb.Append(myAlias);
            messageWriter.AddMessage(buildMessage(sb, connectionContext));
            
        }

        public void SendRefreshOver(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message { connectionContext = connectionContext, output = connectionContext.output, message = MessageConstants.REFRESH + MessageConstants.OVER });
        }

        public void SendDummy(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message { connectionContext = connectionContext, output = connectionContext.output, message = MessageConstants.DUMMY });
        }

        //just for testing purposes...
        public void SendWrongCommandErrorMessage(IConnectionContext connectionContext)
        {
            messageWriter.AddMessage(new Message
            {
                connectionContext = connectionContext,
                message = "wrong command",
                output = connectionContext.output
            });
        }
    }
}
