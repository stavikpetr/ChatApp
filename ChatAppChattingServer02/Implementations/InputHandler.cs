﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Threading;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class InputHandler : IInputHandler
    {
        IDataAccesser dataAccesser;
        IMessageEnqueuer messageEnqueuer;
        INotifier notifier;

        //--------------------------------------------------
        //---------
        //---------   JOIN
        //---------
        //--------------------------------------------------

        //SIGN SERVER serverAlias
        //SIGN ANON
        //SIGN LOGIN username password
        public async Task HandleSignInputAsync(IConnectionContext connectionContext, string[] tokens)
        {
            if (tokens.Length == 2) //user wants to join anonymously
            {
                string name = "Anonymous" + KeyGenerator.keyGenerator.NewKey();
                //i also need to modify table users, i need to insert this anonymous user for his joined session
                await dataAccesser.dataModifier.ModifyAnonymousUser(name, ModificationType.ADD);
                messageEnqueuer.SendSignOK(connectionContext);
                messageEnqueuer.SendInitPhaseString(connectionContext, name);
                var serverGroups = dataAccesser.dataRetriever.GetServerGroupsAsync()
                    .ContinueWith(t1 => messageEnqueuer.SendServerGroupsAsync(t1.Result, connectionContext, MessageConstants.INIT + MessageConstants.DELIMITER));
                User u = new User { userName = name, groups = new LinkedList<Group>(), userStatus = UserStatus.ONLINE };
                dataAccesser.dataModifier.AddNewUser(u);
                dataAccesser.dataModifier.AddNewConnection(name, connectionContext);
                await serverGroups.Result;
                messageEnqueuer.SendInitPhaseOverString(connectionContext);
            }
            else if (tokens.Length == 3) //server joined
            {
                dataAccesser.dataModifier.AddNewConnection(tokens[2], connectionContext);
            }
            else if (tokens.Length == 4) //user is trying to join
            {
                bool isValidUser = await dataAccesser.dataRetriever.IsValidUserAsync(tokens[2], tokens[3]);
                if (isValidUser)
                {
                    //isn't this user already connected?
                    var alreadyConnected = await dataAccesser.dataRetriever.TryGetOnlineUser(tokens[2]);
                    if (alreadyConnected == null)
                    {
                        messageEnqueuer.SendSignOK(connectionContext);
                        //the very first action, i need to wait for its completion => synchronous
                        messageEnqueuer.SendInitPhaseString(connectionContext, tokens[2]);

                        //after init phase, i want this user to receive latest updates about who went online/offline
                        //who sent message in groups he belongs to and so on, so next thing is updating of db and changing status
                        var modifyUserDbStatusTask = dataAccesser.dataModifier.ChangeUserIsOnlineAsync(tokens[2], UserStatus.ONLINE);
                        User u = dataAccesser.dataRetriever.TryGetUser(tokens[2]); //does this server have some info about this user?
                        if (u != null)
                        {
                            dataAccesser.dataModifier.ModifyUserStatus(tokens[2], UserStatus.ONLINE);
                        }
                        else
                        {
                            u = new User { userName = tokens[2], groups = new LinkedList<Group>(), userStatus = UserStatus.ONLINE };
                            dataAccesser.dataModifier.AddNewUser(u);
                        }
                        dataAccesser.dataModifier.AddNewConnection(tokens[2], connectionContext);

                        //awaiting for these two operations should prevent some weird inconsistencies...
                        await modifyUserDbStatusTask;

                        //now lets send all information to the user
                        //úplně první věc je počkání na zafrontění Init phase
                        //change db, change user is online
                        var serverGroups = dataAccesser.dataRetriever.GetServerGroupsAsync()
                            .ContinueWith(t1 => messageEnqueuer.SendServerGroupsAsync(t1.Result, connectionContext, MessageConstants.INIT+MessageConstants.DELIMITER));
                        var friends = dataAccesser.dataRetriever.GetFriendsWithStatusesAsync(tokens[2])
                            .ContinueWith(t1 => messageEnqueuer.SendFriendsInfoAsync(t1.Result, connectionContext, MessageConstants.INIT + MessageConstants.DELIMITER));
                        var userGroups = dataAccesser.dataRetriever.GetUserGroupsAsync(tokens[2])
                            .ContinueWith(t1 => messageEnqueuer.SendUserGroupsAsync(t1.Result, connectionContext, MessageConstants.INIT + MessageConstants.DELIMITER));
                        var friendshipNotifications = dataAccesser.dataRetriever.GetFriendshipNotificationsAsync(tokens[2])
                            .ContinueWith(t1 => messageEnqueuer.SendFriendshipNotificationsAsync(t1.Result, connectionContext));
                        var groupNotifications = dataAccesser.dataRetriever.GetGroupNotificationsAsync(tokens[2]).
                            ContinueWith(t1 => messageEnqueuer.SendGroupNotificationsAsync(t1.Result, connectionContext));
                        var serverCommands = dataAccesser.dataRetriever.GetSupportedCommands()
                            .ContinueWith(t1 => messageEnqueuer.SendSupportedCommandsAsnyc(t1.Result, connectionContext));

                        await Task.WhenAll(new[] { serverGroups.Result, friends.Result, userGroups.Result,
                        friendshipNotifications.Result, groupNotifications.Result, serverCommands.Result });

                        messageEnqueuer.SendInitPhaseOverString(connectionContext);

                        //--------------------------------------------
                        //-------SENDING EVERY NEEDED INFORMATION IS OVER
                        //--------------------------------------------
                        //I don't have to wait for these two tasks, i dont care if they finish executing several moments later
                        notifier.FriendChangedStatusAsync(tokens[2], UserStatus.ONLINE);
                        notifier.GroupUserChangedStatusAllGroupsAsync(tokens[2], UserStatus.ONLINE);
                        return;
                    }
                    else
                    {
                        messageEnqueuer.SendAlreadyConnected(connectionContext);
                    }
                }
                else
                {
                    messageEnqueuer.SendIncorrectCredentials(connectionContext);
                }
            }
        }

        //--------------------------------------------------
        //---------
        //---------   GROUP RELATED
        //---------
        //--------------------------------------------------

        //GROUPNEW|groupname|type|userName|description
        public async Task HandleGroupNewInputAsync(IConnectionContext connectionContext, string[] tokens)
        {
            Group g = new Group
            {
                alias = StaticData.serverAlias,
                groupName = tokens[1],
                admin = tokens[3],
                groupType = (GroupType)Enum.Parse(typeof(GroupType), tokens[2].ToUpper()),
                users = new LinkedList<User>(),
                description = tokens[4]
            };
            if (dataAccesser.dataModifier.TryAddGroup(g, tokens[1]))
            {
                await dataAccesser.dataModifier.ModifyGroupExistsAsync(g, GroupState.CREATED); //adding to db
                await dataAccesser.dataModifier.ModifyIsGroupUserAsync(g.groupName, tokens[3], ModificationType.ADD); //add creator of group as first user
                messageEnqueuer.SendGroupCreateSuccess(tokens[1], tokens[2], tokens[3], tokens[4], connectionContext);
                //if ((GroupType)Enum.Parse(typeof(GroupType), tokens[2].ToUpper()) != GroupType.PRIVATE)
                    notifier.ServerGroupChangedStatusAsync(g.groupName, GroupState.CREATED, (GroupType)Enum.Parse(typeof(GroupType), tokens[2].ToUpper()), tokens[3], tokens[4]); //notify all server users that server group was modified.. created in this case
                //as with few other methods, i don't have to wait for this task, i dont care when it finishes
            }
            else
            {
                 messageEnqueuer.SendGroupCreateFail(tokens[1], connectionContext);
            }
        }

        //GROUPSHOW|groupname
        public async Task HandleGroupShowInputAsync(IConnectionContext connectionContext, string[] tokens)
        {
            var getUsersResult = dataAccesser.dataRetriever.GetGroupUsersToShowAsync(tokens[1]);
            await getUsersResult;
            if (getUsersResult.Result != null)
            {
                //maybe i don't have to wait here?
                await messageEnqueuer.SendShowGroupUsersOKAsync(getUsersResult.Result, connectionContext, tokens[1]);
            }
            else //in a rare case, someone could've deleted the group in the moment user requested to show it
            {
                messageEnqueuer.SendShowGroupUsersERR(connectionContext, tokens[1]);
            }
        }
        //GROUPLEAVE groupName userName
        public async Task HandleUserLeavesGroupAsync(IConnectionContext connectionContext, string[] tokens)
        {
            await dataAccesser.dataModifier.ModifyIsGroupUserAsync(tokens[1], tokens[2], ModificationType.REMOVE);
            notifier.GroupUserChangedStatusAsync(tokens[2], tokens[1], UserStatus.LEFT);
        }
        //GROUPLEAVEADMIN groupName userName
        public async Task HandleAdminLeavesGroupAsync(IConnectionContext connectionContext, string[] tokens)
        {
            await dataAccesser.dataModifier.DeleteGroupAsync(tokens[1], tokens[2]);
            //actually, here, the groupType doesn't matter, the groupType.PRIVATE represents dummy value that will never be used...
            notifier.ServerGroupChangedStatusAsync(tokens[1], GroupState.DELETED, GroupType.PRIVATE, null, null);
            //this is all i need to do, the task for deleting from db was fired in DeleteGroupAsync method
            //now just task for sending info to users and i am done
        }

        //GROUPJOIN|groupName|groupType|userName|admin
        public async Task HandleGroupJoinRequestAsync(IConnectionContext connectionContext, string[] tokens)
        {
            GroupType groupType = (GroupType)Enum.Parse(typeof(GroupType), tokens[2].ToUpper());
            if (groupType == GroupType.PUBLIC)
            {
                bool joinSuccessful = await dataAccesser.dataModifier.ModifyIsGroupUserAsync(tokens[1], tokens[3], ModificationType.ADD);
                if (joinSuccessful)
                {
                    messageEnqueuer.SendGroupJoinSuccess(connectionContext, tokens[1]);
                    notifier.GroupUserChangedStatusAsync(tokens[3], tokens[1], UserStatus.JOINED);
                }
                else //some weird scenarios when the group has been deleted right when the request arrived
                {
                    messageEnqueuer.SendGroupJoinFailBeingDeleted(connectionContext, tokens[1]);
                    //also send info about group changed status to deleted
                    //well, actually, i should not worry about this, info about group deletion has to be send to user in a moment...
                    //messageEnqueuer.SendServerGroupChangedStatus(tokens[1], connectionContext, GroupState.DELETED, null);
                }
            }
            else if (groupType == GroupType.PRIVATE)
            {
                //check if this group with provided admin exists on this server a její typ je private
                if (dataAccesser.dataRetriever.IsValidServerGroup(tokens[1], tokens[4], GroupType.PRIVATE))
                {
                    bool joinSuccessful = await dataAccesser.dataModifier.ModifyIsGroupUserAsync(tokens[1], tokens[3], ModificationType.ADD);
                    if (joinSuccessful)
                    {
                        messageEnqueuer.SendGroupJoinSuccess(connectionContext, tokens[1]);
                        notifier.GroupUserChangedStatusAsync(tokens[3], tokens[1], UserStatus.JOINED);
                    }
                    else //some weird scenarios when the group has been deleted right when the request arrived
                    {
                        messageEnqueuer.SendGroupJoinFailBeingDeleted(connectionContext, tokens[1]);
                        
                    }
                }
                else
                {
                    messageEnqueuer.SendGroupJoinFailNonExisting(connectionContext, tokens[1]);
                }
            }
            
        }

        //GROUPKICKORDER|groupName|kicker|kickeduser|serverAlias
        public async Task HandleGroupKickOrderAsync(IConnectionContext connectionContext, string[] tokens)
        {
            bool result = false;
            while (result != true)
            {
                //at first, this part was done in async, but unfortunatelly, i can'T have multiple connections opened to db
                //while in transactionscope .... one solution was to throw away transaction, second was synchronous, chose synchronous
                    using (var trans = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                    {
                        dataAccesser.dataModifier.ModifyIsGroupUser(tokens[1], tokens[3], ModificationType.REMOVE);
                        dataAccesser.dataModifier.ModifyGroupNotificationExists(GroupNotificationType.GROUPKICKINFO, tokens[1], tokens[2], tokens[3], ModificationType.ADD, tokens[4]);
                        trans.Complete();
                        result = true;
                    }

            }
            await HandleGroupKickInfoAsync(connectionContext, tokens);
            notifier.GroupUserChangedStatusAsync(tokens[3], tokens[1], UserStatus.LEFT);

        }
        //vlastně, pokud awaitu tenhle task, tak chci čekat jen na to, že se naplánuje 
        //GOUPKICKINFO|groupName|kicker|kickedUser|serverAlias
        public async Task HandleGroupKickInfoAsync(IConnectionContext connectionContext, string[] tokens)
        {
            //MOŽNÁ S TASK.RUN BEZ TOHO ABYCH PŘI AWAITU NEČEKAL NA TOHLE CELÝ, PRO TESTOVÁNÍ ZATÍM NECHÁNO TAKTO

            var onlineUser = await dataAccesser.dataRetriever.TryGetOnlineUser(tokens[3]);

            if (onlineUser == null)
            {
                //nothing, the info is already stored in database
            }
            else
            {
                if (onlineUser.alias == StaticData.serverAlias)
                {
                    IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(tokens[3]);
                    if (cc != null)
                        messageEnqueuer.SendGroupNotification(cc, GroupNotificationType.GROUPKICKINFO, tokens[1], tokens[2], tokens[3], tokens[4]);
                }
                else
                {
                    IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(onlineUser.alias);
                    if (cc != null)
                        messageEnqueuer.SendGroupNotification(cc, GroupNotificationType.GROUPKICKINFO, tokens[1], tokens[2], tokens[3], tokens[4]);
                }
            }
            return;

        }
        //  dataAccesser.dataModifier.ModifyGroupNotificationExistsAsync(GroupNotificationType.GROUPKICKINFO, tokens[1], tokens[2], tokens[3], ModificationType.REMOVE, tokens[4]);

        //GROUPKICKINFOUNDERSTOOD|groupName|kicker|kickedUser|serverAlias
        public async Task HandleGroupInfoUnderstood(IConnectionContext connectionContext, string[] tokens, GroupNotificationType groupNotificationType)
        {
            switch (groupNotificationType)
            {
                case GroupNotificationType.GROUPKICKINFOUNDERSTOOD:
                    await dataAccesser.dataModifier.ModifyGroupNotificationExistsAsync(GroupNotificationType.GROUPKICKINFO, tokens[1], tokens[2], tokens[3], ModificationType.REMOVE, tokens[4]);
                    break;
            }
        }

        //--------------------------------------------------
        //---------
        //---------   FRIENDSHIP RELATED
        //---------
        //--------------------------------------------------


        //FRIENDSHIPREQUESTORDER|sender(friendshipRequester)|receiver
        //FRIENDSHIPREFUSEDORDER|userThatRefusedFriendship|friendshipRequester
        //FRIENDSHIPACCEPTORDER|userThatAcceptedFriendship|friendshipRequester
        //FRIENDSHIPEENDEDORDER|userThatEndedFriendship|otherUser
        public async Task HandleFriendshipOrder(IConnectionContext connectionContext, string[] tokens, FriendshipNotificationType friendshipNotification)
        {
            bool operationResult = false;
            while (operationResult != true)
            {
                //transactions with async await dont work very well, i need synchronous variants of methods....
                using (var trans = new TransactionScope())
                {
                    try
                    {
                        switch (friendshipNotification)
                        {
                            case FriendshipNotificationType.FRIENDSHIPREQUESTORDER:
                                dataAccesser.dataModifier.ModifyFriendshipNotificationExists(FriendshipNotificationType.FRIENDSHIPREQUESTINFO, tokens[1], tokens[2], ModificationType.ADD);
                                break;                           
                            case FriendshipNotificationType.FRIENDSHIPREFUSEORDER:
                                dataAccesser.dataModifier.ModifyFriendshipNotificationExists(FriendshipNotificationType.FRIENDSHIPREQUESTINFO, tokens[2], tokens[1], ModificationType.REMOVE);
                                dataAccesser.dataModifier.ModifyFriendshipNotificationExists(FriendshipNotificationType.FRIENDSHIPREFUSEINFO, tokens[1], tokens[2], ModificationType.ADD);
                                break;
                            case FriendshipNotificationType.FRIENDSHIPACCEPTORDER:
                                dataAccesser.dataModifier.ModifyFriendshipNotificationExists(FriendshipNotificationType.FRIENDSHIPREQUESTINFO, tokens[2], tokens[1], ModificationType.REMOVE);
                                dataAccesser.dataModifier.ModifyFriendshipNotificationExists(FriendshipNotificationType.FRIENDSHIPACCEPTINFO, tokens[1], tokens[2], ModificationType.ADD);
                                dataAccesser.dataModifier.ModifyFriendshipRelation(tokens[1], tokens[2], ModificationType.ADD);
                                var friendStatus = dataAccesser.dataRetriever.GetFriendStatus(tokens[1], tokens[2]);
                                messageEnqueuer.SendFriendChangedStatus(tokens[2], connectionContext, friendStatus.status);
                                break;
                            case FriendshipNotificationType.FRIENDSHIPENDORDER:
                                dataAccesser.dataModifier.ModifyFriendshipNotificationExists(FriendshipNotificationType.FRIENDSHIPENDINFO, tokens[1], tokens[2], ModificationType.ADD);
                                dataAccesser.dataModifier.ModifyFriendshipRelation(tokens[1], tokens[2], ModificationType.REMOVE);
                                break;
                        }
                      
                        trans.Complete();
                        operationResult = true;
                    }
                    catch (Exception e)
                    {
                        operationResult = false;
                    }
                }
            }

            FriendshipNotificationType newNotification = FriendshipNotificationType.FRIENDSHIPREQUESTORDER; //dummy value otherwise it wouldn't compile
            if (friendshipNotification == FriendshipNotificationType.FRIENDSHIPACCEPTORDER)
                newNotification = FriendshipNotificationType.FRIENDSHIPACCEPTINFO;
            else if (friendshipNotification == FriendshipNotificationType.FRIENDSHIPENDORDER)
                newNotification = FriendshipNotificationType.FRIENDSHIPENDINFO;
            else if (friendshipNotification == FriendshipNotificationType.FRIENDSHIPREFUSEORDER)
                newNotification = FriendshipNotificationType.FRIENDSHIPREFUSEINFO;
            else if (friendshipNotification == FriendshipNotificationType.FRIENDSHIPREQUESTORDER)
                newNotification = FriendshipNotificationType.FRIENDSHIPREQUESTINFO;

            HandleFriendshipAcceptedInfo(connectionContext, tokens, newNotification);

         

        }

        //přijmutí zprávy od serveru
        //FRIENDSHIPREQUESTINFO|sender(friendshipRequester)|receiver 
        //FRIENDSHIPACCEPTEDINFO|userThataccepted|friendshiprequester
        //FRIENDSHIPREFUSEDINFO|userthatRefused|friendshipRequester
        //FRIENDSHIPENDEDINFO|userthatEnded|friendToRemove
        public async Task HandleFriendshipAcceptedInfo(IConnectionContext connectionContext, string[] tokens, FriendshipNotificationType friendshipNotificationType)
        {

            var onlineUser = await dataAccesser.dataRetriever.TryGetOnlineUser(tokens[2]);
            if (onlineUser == null)
            {
                //nothing, the info is already stored in database
                //there is one million chance for super weird
            }
            else
            {
                if (onlineUser.alias == StaticData.serverAlias)
                {
                    IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(tokens[2]);
                    if (cc != null)
                    {
                        
                            messageEnqueuer.SendFriendshipNotification(cc, friendshipNotificationType, tokens[1], tokens[2]);
                            //send him info about the status of accepter
                            if (friendshipNotificationType == FriendshipNotificationType.FRIENDSHIPACCEPTINFO)
                            {
                                var friendStatus = await dataAccesser.dataRetriever.GetFriendStatusAsync(tokens[2], tokens[1]);
                                messageEnqueuer.SendFriendChangedStatus(tokens[1], cc, friendStatus.status);
                            }
                   
                    }
                }
                else
                {
                    IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(onlineUser.alias);
                    if (cc != null)
                        messageEnqueuer.SendFriendshipNotification(cc, friendshipNotificationType, tokens[1], tokens[2]);
                }
            }

            return;
        }

        //přijmutí zprávy od klienta
        //FRIENDSHIPACCEPTEDINFOUNDERSTOOD|userThatPerformedAccept|userThatUnderstands
        //FRIENDSHIPREFUSEDINFOUNDERSTOOD|userThatPerformedRefuse|userThatUnderstands
        //FRIENDSHIENDEDINFOUNDERSTOOD|userThatPerformedEnd|userThatUnderstands
        public async Task HandleFriendshipInfoUnderstood(IConnectionContext connectionContext, string[] tokens, FriendshipNotificationType friendshipNotificationType)
        {
            switch (friendshipNotificationType)
            {
                case FriendshipNotificationType.FRIENDSHIPACCEPTINFOUNDERSTOOD:
                    await dataAccesser.dataModifier.ModifyFriendshipNotificationExistsAsync(FriendshipNotificationType.FRIENDSHIPACCEPTINFO, tokens[1], tokens[2], ModificationType.REMOVE);
                    break;
                case FriendshipNotificationType.FRIENDSHIPENDINFOUNDERSTOOD:
                    await dataAccesser.dataModifier.ModifyFriendshipNotificationExistsAsync(FriendshipNotificationType.FRIENDSHIPENDINFO, tokens[1], tokens[2], ModificationType.REMOVE);
                    break;
                case FriendshipNotificationType.FRIENDSHIPREFUSEINFOUNDERSTOOD:
                    await dataAccesser.dataModifier.ModifyFriendshipNotificationExistsAsync(FriendshipNotificationType.FRIENDSHIPREFUSEINFO, tokens[1], tokens[2], ModificationType.REMOVE);
                    break;
            }
        }

        //--------------------------------------------------
        //---------
        //---------   MESSAGES
        //---------
        //--------------------------------------------------

        //MESSAGEGROUP|groupName|sender|message
        public async Task HandleGroupMessage(IConnectionContext connectionContext, string[] tokens)
        {
            Task.Factory.StartNew(async() =>
            {
                var groupUsers =await dataAccesser.dataRetriever.GetUsersInGroupAsync(tokens[1]);
                foreach (var user in groupUsers)
                {
                    if (user.userName == tokens[2])
                        continue;
                    IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(user.userName);
                    if (cc != null)
                        messageEnqueuer.SendGroupMessage(cc, tokens[2], tokens[1], tokens[3]);
                }
                
            });
        }
        //MESSAGEFRIEND|receiver|sender|message
        public async Task HandleFriendMessage(IConnectionContext connectionContext, string[] tokens)
        {
            Task.Factory.StartNew(async () =>
            {
                var onlineFriend = await dataAccesser.dataRetriever.TryGetOnlineUser(tokens[1]);
                if (onlineFriend == null)
                {
                    //well, friend went offline and sender saw him as online, soon, he will receive notification that he went offline...
                    //messageEnqueuer.SendFriendMessageOperationResult(connectionContext, "sorry, friend is offline");
                }
                else
                {
                    if (onlineFriend.alias == StaticData.serverAlias)
                    {
                        IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(tokens[1]);
                        if (cc != null)
                            messageEnqueuer.SendFriendMessage(cc, tokens[2], tokens[1], tokens[3]);
                    }
                    else
                    {
                        IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(onlineFriend.alias);
                        if (cc != null)
                            messageEnqueuer.SendFriendMessage(cc, tokens[2], tokens[1], tokens[3]);
                    }
                }
            });
        }

        //--------------------------------------------------
        //---------
        //---------   SEARCHES
        //---------
        //--------------------------------------------------

        //SEARCHBYUSERNAME|searchedValue
        public async Task HandleSearchByUsername(IConnectionContext connectionContext, string[] tokens)
        {
            Task.Factory.StartNew(async() =>
            {
                if (await dataAccesser.dataRetriever.DoesUserExistsByName(tokens[1]))
                    messageEnqueuer.SendSearchResultOK(connectionContext);
                else
                    messageEnqueuer.SendSearchResultFail(connectionContext);
            });
        }

        //not suppported yet, in my app, multiple users can have same mail - that wouldn't be in final version
        //SEARCHBYEMAIL|searchedValue
        public async Task HandleSearchByEMAIL(IConnectionContext connectionContext, string[] tokens)
        {
            Task.Factory.StartNew(async() =>
            {
                if (await dataAccesser.dataRetriever.DoesUserExistsByName(tokens[1]))
                    messageEnqueuer.SendSearchResultOK(connectionContext);
                else
                    messageEnqueuer.SendSearchResultFail(connectionContext);
            });
        }

        //DISCONNECT|userName
        //runs already in separate thread, i dont need to await anything
        public async Task HandleDisconnect(IConnectionContext connectionContext, string userName, bool isFatal)
        {
            if (userName.StartsWith("Anonymous"))
            {
                var userGroups = await dataAccesser.dataRetriever.GetUserGroupsAsync(userName);
                foreach (var group in userGroups)
                {
                    dataAccesser.dataModifier.RemoveGroupUserLocked(group, userName);
                }
                
                await dataAccesser.dataModifier.ModifyAnonymousUser(userName, ModificationType.REMOVE);

                if (!isFatal)
                {
                    connectionContext.oneAwaitsEnd = true;
                    messageEnqueuer.SendDummy(connectionContext);
                    while (connectionContext.ended != true)
                    {
                        //nothing, let's wait for end, it has to end really soon
                    }
                    
                }
                await notifier.GroupUserChangedStatusAllGroupsAsync(userName, UserStatus.LEFT);
                dataAccesser.dataModifier.RemoveUser(userName);
                //remove him from users
                //remove him from every single group he was in
            }
            else
            {
                dataAccesser.dataModifier.ModifyUserStatus(userName, UserStatus.OFFLINE);
                await dataAccesser.dataModifier.ChangeUserIsOnlineAsync(userName, UserStatus.OFFLINE); //db operation

                if (!isFatal)
                {
                    connectionContext.oneAwaitsEnd = true;
                    messageEnqueuer.SendDummy(connectionContext);

                    while (connectionContext.ended != true)
                    {
                        //nothing, let's wait for end, it has to end really soon
                    }
                }
                notifier.FriendChangedStatusAsync(userName, UserStatus.OFFLINE);
                notifier.GroupUserChangedStatusAllGroupsAsync(userName, UserStatus.OFFLINE);
            }
        }
        //FRIENDCHANGEDSTATUS|friendName|STATUS
        public async Task HandleFriendChangedStatusAsync(IConnectionContext connectionContext, string[] tokens)
        {
            var status = (UserStatus)Enum.Parse(typeof(UserStatus), tokens[2].ToUpper());
            var onlineFriends = await dataAccesser.dataRetriever.GetOnlineFriendsWithServersAsync(tokens[1]);
            foreach (var friend in onlineFriends)
            {
                if (friend.alias == StaticData.serverAlias)
                {
                    await notifier.FriendChangedStatusSingleFriendAsync(tokens[1], friend.login, status);
                }
            }
        }

        public async Task HandleUnexpectedEndAsync(IConnectionContext connectionContext)
        {
            string userName = dataAccesser.dataRetriever.GetUserNameFromConnectionContext(connectionContext);
            await HandleDisconnect(connectionContext, userName, true);
        }

      

        public async Task WrongCommand(IConnectionContext connectionContext)
        {
            messageEnqueuer.SendWrongCommandErrorMessage(connectionContext);
        }


        public InputHandler(IDataAccesser dataAccesser, IMessageEnqueuer messageEnqueuer)
        {
            this.dataAccesser = dataAccesser;
            this.messageEnqueuer = messageEnqueuer;
            notifier = new Notifier(messageEnqueuer, dataAccesser);
        }
    }
}
