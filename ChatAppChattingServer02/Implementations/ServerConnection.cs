﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace ChatAppChattingServer02
{
    class ServerConnection : IConnectionContext
    {
        public StreamReader input { get; private set; }

        public StreamWriter output { get; private set; }

        public Socket socket { get; private set; }

        public bool oneAwaitsEnd { get; set; } = false;

        public bool ended { get; set; } = false;

        public ServerConnection(StreamReader input, StreamWriter output, Socket socket)
        {
            this.input = input;
            this.output = output;
            this.socket = socket;
        }
    }
}
