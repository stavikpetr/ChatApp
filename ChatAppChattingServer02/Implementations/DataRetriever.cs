﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class DataRetriever : IDataRetriever
    {
        private IRepository repository { get; set; }
        private SharedData sharedData { get; set; }

        //--------------------------------------------------
        //---------
        //---------   WORKING WITH DB
        //---------
        //--------------------------------------------------
        public Task<ICollection<Server>> GetServersAsync()
        {
            return Task.Factory.StartNew<ICollection<Server>>(() =>
            {
                return repository.GetServers();
            });
        }

        public Task<ICollection<Group>> GetGroupsAsync(string alias)
        {
            return Task.Factory.StartNew<ICollection<Group>>(() =>
            {
                return repository.GetGroups(alias);
            });
        }

        public Task<ICollection<GroupUser>> GetGroupUsersAsync(string alias)
        {
            return Task.Factory.StartNew<ICollection<GroupUser>>(() =>
            {
                return repository.GetGroupUsers(alias);
            });
        }

        public Task<ICollection<UserWithStatus>> GetFriendsWithStatusesAsync(string user)
        {
            return Task.Factory.StartNew<ICollection<UserWithStatus>>(() =>
            {
                return repository.GetFriendsWithStatuses(user);
            });
        }

        public Task<ICollection<FriendshipNotification>> GetFriendshipNotificationsAsync(string receiver)
        {
            return Task.Factory.StartNew<ICollection<FriendshipNotification>>(() =>
            {
                return repository.GetFriendshipNotifications(receiver);
            });
        }

        public Task<ICollection<GroupNotification>> GetGroupNotificationsAsync(string receiver)
        {
            return Task.Factory.StartNew<ICollection<GroupNotification>>(() =>
            {
                return repository.GetGroupNotifications(receiver);
            });
        }

        public Task<ICollection<OnlineUser>> GetOnlineFriendsWithServersAsync(string user)
        {
            return Task.Factory.StartNew<ICollection<OnlineUser>>(() =>
            {
                return repository.GetOnlineFriendsWithServers(user);
            });
        }


        public Task<ICollection<SupportedCommand>> GetSupportedCommands()
        {
            return Task.Factory.StartNew(() =>
            {
                return repository.GetSupportedCommands();
            });
        }

        public Task<UserWithStatus> GetFriendStatusAsync(string user, string friend)
        {
            return Task.Factory.StartNew(() =>
            {
                return repository.GetFriendStatus(user, friend);
            });
        }

        public UserWithStatus GetFriendStatus(string user, string friend)
        {
            return repository.GetFriendStatus(user, friend);
        }

        public Task<bool> IsValidUserAsync(string login, string password)
        {
            return Task.Factory.StartNew(() =>
            {
                return repository.IsValidUser(login, password);
            });
        }

        public Task<OnlineUser> TryGetOnlineUser(string userName)
        {
            return Task.Factory.StartNew<OnlineUser>(() =>
            {
                return repository.TryGetOnlineUser(userName);
            });
        }

        public Task<string> DoesUserExistsByEmail(string email)
        {
            return Task.Factory.StartNew<string>(() =>
            {
                return repository.DoesUserExistsByEmail(email);
            });
        }

        public Task<bool> DoesUserExistsByName(string userName)
        {
            return Task.Factory.StartNew<bool>(() =>
            {
                return repository.DoesUserExistsByUsername(userName);
            });
        }

        public string GetUserNameFromConnectionContext(IConnectionContext connectionContext)
        {
            sharedData.rwActiveConnectionsLock.EnterReadLock();
            foreach (var con in sharedData.activeConnections)
            {
                if (con.Value == connectionContext)
                {
                    sharedData.rwActiveConnectionsLock.ExitReadLock();
                    return con.Key;
                }
            }
            sharedData.rwActiveConnectionsLock.ExitReadLock();
            return null;
            
        }

        //--------------------------------------------------
        //---------
        //---------   WORKING WITH SHARED DATA
        //---------
        //--------------------------------------------------

        public Task<ICollection<Group>> GetServerGroupsAsync()
        {
            return Task.Factory.StartNew<ICollection<Group>>(() =>
            {
                LinkedList<Group> groups = new LinkedList<Group>();
                sharedData.rwGroupsLock.EnterReadLock();
                foreach (var group in sharedData.groups)
                {
                    //if(group.Value.groupType != GroupType.PRIVATE)
                        groups.AddLast(group.Value);
                }
                sharedData.rwGroupsLock.ExitReadLock();
                return groups;
            });
        }

        public Task<ICollection<Group>> GetUserGroupsAsync(string user)
        {
            return Task.Factory.StartNew<ICollection<Group>>(() =>
            {
                LinkedList<Group> userGroups = new LinkedList<Group>();    
                sharedData.rwGroupsLock.EnterReadLock();           //always in this order
                sharedData.rwUsersLock.EnterReadLock();
                User u = sharedData.users[user];
                foreach (var group in u.groups)
                {
                    userGroups.AddLast(group);
                }
                sharedData.rwUsersLock.ExitReadLock();
                sharedData.rwGroupsLock.ExitReadLock();
                return userGroups;
            });
        }


        public Task<ICollection<UserWithStatus>> GetGroupUsersToShowAsync(string groupName)
        {
            return Task.Factory.StartNew<ICollection<UserWithStatus>>(() =>
            {
                LinkedList<UserWithStatus> groupUsers = new LinkedList<UserWithStatus>();
                sharedData.rwGroupsLock.EnterReadLock();
                sharedData.rwUsersLock.EnterReadLock();
                if (sharedData.groups.ContainsKey(groupName))
                {
                    foreach (var user in sharedData.groups[groupName].users)
                    {
                        groupUsers.AddLast(new UserWithStatus(user.userStatus, user.userName));
                    }
                }
                else
                {
                    sharedData.rwUsersLock.ExitReadLock();
                    sharedData.rwGroupsLock.ExitReadLock();
                    return null;
                }
                sharedData.rwUsersLock.ExitReadLock();
                sharedData.rwGroupsLock.ExitReadLock();
                return groupUsers;
            });
        }


        public Task<ICollection<User>> GetUsersInGroupAsync(string group)
        {
            return Task.Factory.StartNew<ICollection<User>>(() =>
            {
                LinkedList<User> users = new LinkedList<User>();
                sharedData.rwGroupsLock.EnterReadLock();
                sharedData.rwUsersLock.EnterReadLock();
                if (sharedData.groups.ContainsKey(group))
                {
                    var users2 = sharedData.groups[group].users;
                    foreach (var user in users2)
                    {
                        if (user.userStatus == UserStatus.ONLINE)
                            users.AddLast(user);
                    }
                }
                sharedData.rwUsersLock.ExitReadLock();
                sharedData.rwGroupsLock.ExitReadLock();
                return users;
            });
        }

        public Task<ICollection<string>> GetAllOnlineServerUsersAsync()
        {
            return Task.Factory.StartNew<ICollection<string>>(() =>
            {
                LinkedList<string> onlineUsers = new LinkedList<string>();
                sharedData.rwUsersLock.EnterReadLock();
                foreach (var userItem in sharedData.users)
                {
                    if (userItem.Value.userStatus == UserStatus.ONLINE)
                        onlineUsers.AddLast(userItem.Value.userName);
                }
                sharedData.rwUsersLock.ExitReadLock();
                return onlineUsers;
            });
        }



        public bool IsValidServerGroup(string groupName, string admin, GroupType groupType)
        {
            sharedData.rwGroupsLock.EnterReadLock();
            if (sharedData.groups.ContainsKey(groupName))
            {
                Group g = sharedData.groups[groupName];
                if (g.admin == admin && g.groupType == groupType)
                {
                    sharedData.rwGroupsLock.ExitReadLock();
                    return true;
                }
                else
                {
                    sharedData.rwGroupsLock.ExitReadLock();
                    return false;
                }
            }
            else
            {
                sharedData.rwGroupsLock.ExitReadLock();
                return false;
            }
        }

        public IConnectionContext TryRetrieveConnectionContext(string of)
        {
            sharedData.rwActiveConnectionsLock.EnterReadLock();
            IConnectionContext cs;
            sharedData.activeConnections.TryGetValue(of, out cs);
            sharedData.rwActiveConnectionsLock.ExitReadLock();
            return cs;
        }
       
        public User TryGetUser(string login)
        {
            User u;
            sharedData.rwUsersLock.EnterReadLock();
            sharedData.users.TryGetValue(login, out u);
            sharedData.rwUsersLock.ExitReadLock();
            return u;
        }

        public ICollection<IConnectionContext> GetConnections()
        {
            LinkedList<IConnectionContext> connections = new LinkedList<IConnectionContext>();
            foreach (var connection in sharedData.activeConnections)
            {
                connections.AddLast(connection.Value);
            }
            return connections;
        }

        public DataRetriever(IRepository repository, SharedData sharedData)
        {
            this.repository = repository;
            this.sharedData = sharedData;
        }
    }
}
