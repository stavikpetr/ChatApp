﻿#define LOG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ChatAppCommon;

using System.Collections.Concurrent;

namespace ChatAppChattingServer02
{
    class MessageWriter : IMessageWriter
    {
        private BlockingCollection<Message> messageQueue;

        public void WriteMessages()
        {
            while (true)
            {
                var message = messageQueue.Take();
                try
                {
                    var x =message.output.WriteLineAsync(message.message).
                        ContinueWith(_ => message.output.FlushAsync());
                    x.Result.Wait();
                    if (message.connectionContext.oneAwaitsEnd)
                    {
                        if (message.message == MessageConstants.DUMMY) //I've sent dummy, no more messages for this user, streams can be closed
                            message.connectionContext.ended = true;
                    }
                }
                catch (IOException e)
                {
#if LOG
                    Logger.logger.Log("message Writer caught IOException");
#endif
                    TaskManager.unexpectedEndingHandler.Invoke(message.connectionContext);
                }
                catch (Exception e)
                {
#if LOG
                    Logger.logger.Log($"caught general execption, messageWriter: {e.Message}");
#endif
                    TaskManager.unexpectedEndingHandler(message.connectionContext);
                }
            }
        }

        public void AddMessage(Message m)
        {
            messageQueue.Add(m);
        }

        public MessageWriter()
        {
            messageQueue = new BlockingCollection<Message>();
        }
    }
}
