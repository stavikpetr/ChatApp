﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class Notifier: INotifier
    {
        IMessageEnqueuer messageEnqueuer;
        IDataAccesser dataAccesser;

        //--------------------------------------------------
        //---------
        //---------   NOTIFYING OTHER USERS
        //---------
        //--------------------------------------------------

        public Task FriendChangedStatusSingleFriendAsync(string friend, string whoToNotify, UserStatus status)
        {
            return Task.Factory.StartNew(() =>
            {
                IConnectionContext cc = dataAccesser.dataRetriever.TryRetrieveConnectionContext(whoToNotify);
                if (cc != null)
                    messageEnqueuer.SendFriendChangedStatus(friend, cc, status);
            });
        }

        public async Task FriendChangedStatusAsync(string user, UserStatus status)
        {
            var onlineFriendsWithServers = await dataAccesser.dataRetriever.GetOnlineFriendsWithServersAsync(user);
            HashSet<string> serversNotified = new HashSet<string>();
            foreach (var onlineUser in onlineFriendsWithServers)
            {
                //the user is on this server and has to be connected
                if (onlineUser.alias == StaticData.serverAlias)
                {
                    //try retrieve connectionContext
                    IConnectionContext connectionContext = dataAccesser.dataRetriever.TryRetrieveConnectionContext(onlineUser.login);
                    if (connectionContext != null)
                        messageEnqueuer.SendFriendChangedStatus(user, connectionContext, status);
                }
                else
                {
                    if (!serversNotified.Contains(onlineUser.alias))
                    {
                        serversNotified.Add(onlineUser.alias);
                        IConnectionContext connectionContext = dataAccesser.dataRetriever.TryRetrieveConnectionContext(onlineUser.alias);
                        if (connectionContext != null)
                            messageEnqueuer.SendFriendChangedStatus(user, connectionContext, status);
                    }
                }
            }

        }

        public async Task GroupUserChangedStatusAllGroupsAsync(string user, UserStatus status)
        {
            var groups = await dataAccesser.dataRetriever.GetUserGroupsAsync(user);
            foreach (var group in groups)
            {
                await GroupUserChangedStatusAsync(user, group.groupName, status);
            }
        }

        public async Task GroupUserChangedStatusAsync(string user, string groupName, UserStatus status)
        {
            var users = await dataAccesser.dataRetriever.GetUsersInGroupAsync(groupName);
            foreach (var u in users)
            {
                IConnectionContext connectionContext = dataAccesser.dataRetriever.TryRetrieveConnectionContext(u.userName);
                if (connectionContext != null)
                    messageEnqueuer.SendUserInGroupChangedStatus(user, connectionContext, status, groupName);
            }

        }

        public async Task ServerGroupChangedStatusAsync(string groupName, GroupState groupState, GroupType groupType, string admin, string description)
        {
            var users = await dataAccesser.dataRetriever.GetAllOnlineServerUsersAsync();
            foreach (var u in users)
            {
                IConnectionContext connectionContext = dataAccesser.dataRetriever.TryRetrieveConnectionContext(u);
                if (connectionContext != null)
                    messageEnqueuer.SendServerGroupChangedStatus(groupName, connectionContext, groupState, groupType, admin, description);
            }
        }

        public Notifier(IMessageEnqueuer messageEnqueuer, IDataAccesser dataAccesser)
        {
            this.messageEnqueuer = messageEnqueuer;
            this.dataAccesser = dataAccesser;
        }

    }
}
