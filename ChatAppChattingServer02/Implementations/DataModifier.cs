﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Transactions;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class DataModifier:IDataModifier
    {
        private IRepository repository { get; set; }
        private SharedData sharedData { get; set; }

        //-----------------------------------------------------
        //----------------
        //----------------  SHARED DATA MODIFIING
        //----------------
        //-----------------------------------------------------

        public void AddNewConnection(string key, IConnectionContext connectionContext)
        {
            sharedData.rwActiveConnectionsLock.EnterWriteLock();
            sharedData.activeConnections.Add(key, connectionContext);
            sharedData.rwActiveConnectionsLock.ExitWriteLock();
        }

        public Task AddServersAsync(ICollection<Server> servers)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var server in servers)
                {
                    AddNewServer(server);
                }
            });
        }

        public Task AddGroupsAsync(ICollection<Group> groups)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var group in groups)
                {
                    AddNewGroup(group);
                }
            });
        }

        public Task AddGroupUsersAsync(ICollection<GroupUser> groupUsersQuery)
        {
            return Task.Factory.StartNew(() =>
            {

                foreach (var groupUser in groupUsersQuery)
                {
                    User u;
                    sharedData.rwUsersLock.EnterUpgradeableReadLock();
                    if (!sharedData.users.TryGetValue(groupUser.login, out u))
                    {
                        u = new User { userName = groupUser.login, userStatus = UserStatus.OFFLINE, groups = new LinkedList<Group>() };
                        AddNewUser(u);
                        sharedData.rwUsersLock.ExitUpgradeableReadLock();
                    }
                    else
                        sharedData.rwUsersLock.ExitUpgradeableReadLock();
                    Group g;

                    //the deadlock should never occur here
                    sharedData.rwGroupsLock.EnterUpgradeableReadLock();
                    sharedData.rwUsersLock.EnterUpgradeableReadLock();
                    g = sharedData.groups[groupUser.groupName];
                    u = sharedData.users[groupUser.login];
                    AddNewGroupUser(g, u);
                    sharedData.rwUsersLock.ExitUpgradeableReadLock();
                    sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                }

            });
        }

        public void AddNewUser(User user)
        {
            sharedData.rwUsersLock.EnterWriteLock();
            sharedData.users.Add(user.userName, user);
            sharedData.rwUsersLock.ExitWriteLock();
        }

        public void AddNewGroupUser(Group group, User user)
        {
            sharedData.rwGroupsLock.EnterWriteLock();
            group.users.Add(user);
            sharedData.rwGroupsLock.ExitWriteLock();
            sharedData.rwUsersLock.EnterWriteLock();
            user.groups.Add(group);
            sharedData.rwUsersLock.ExitWriteLock();
        }

        //called during disconnect of user
        public void RemoveUser(string userName)
        {
            sharedData.rwUsersLock.EnterWriteLock();
            sharedData.users.Remove(userName);
            sharedData.rwUsersLock.ExitWriteLock();
            sharedData.rwActiveConnectionsLock.EnterWriteLock();
            sharedData.activeConnections.Remove(userName);
            sharedData.rwActiveConnectionsLock.ExitWriteLock();
        }

        public void RemoveGroupUserLocked(Group g, string userName)
        {
            sharedData.rwGroupsLock.EnterWriteLock();
            sharedData.rwUsersLock.EnterWriteLock();
            User u = sharedData.users[userName];
            g.users.Remove(u);
            sharedData.rwUsersLock.ExitWriteLock();
            sharedData.rwGroupsLock.ExitWriteLock();
        }

        private void AddNewGroupUserUnlocked(Group group, User user)
        {
            group.users.Add(user);
            user.groups.Add(group);
        }

        private void RemoveGroupUserUnlocked(Group group, User user)
        {
            group.users.Remove(user);
            user.groups.Remove(group);
        }       

        public void AddNewGroup(Group g)
        {
            sharedData.rwGroupsLock.EnterWriteLock();
            sharedData.groups.Add(g.groupName, g);
            sharedData.rwGroupsLock.ExitWriteLock();
        }

        public void AddNewServer(Server s)
        {
            sharedData.rwServersLock.EnterWriteLock();
            sharedData.servers.Add(s);
            sharedData.rwServersLock.ExitWriteLock();
        }

        public bool TryAddGroup(Group g, string groupName)
        {
            sharedData.rwGroupsLock.EnterUpgradeableReadLock();
            if (sharedData.groups.ContainsKey(groupName))
            {
                sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                return false;
            }
            else
            {
                sharedData.rwGroupsLock.EnterWriteLock();
                sharedData.groups.Add(groupName, g);
                sharedData.rwGroupsLock.ExitWriteLock();
                sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                return true;
            }
        }  

        public void ModifyUserStatus(string login, UserStatus userStatus)
        {
            sharedData.rwUsersLock.EnterUpgradeableReadLock();
            User u = sharedData.users[login];
            sharedData.rwUsersLock.EnterWriteLock();
            u.userStatus = userStatus;
            if (userStatus == UserStatus.OFFLINE)
            {
                sharedData.rwActiveConnectionsLock.EnterWriteLock();
                sharedData.activeConnections.Remove(login);
                sharedData.rwActiveConnectionsLock.ExitWriteLock();
            }
            sharedData.rwUsersLock.ExitWriteLock();
            sharedData.rwUsersLock.ExitUpgradeableReadLock();
            
        }

        //-----------------------------------------------------
        //----------------
        //---------------- DB AND SHARED DATA MODIFIING
        //----------------
        //-----------------------------------------------------

        public Task DeleteGroupAsync(string groupName, string admin)
        {
            sharedData.rwGroupsLock.EnterWriteLock();
            Group g = sharedData.groups[groupName];
            sharedData.groups.Remove(groupName);
            sharedData.rwGroupsLock.ExitWriteLock();
            return Task.Factory.StartNew(() =>
            {
                Task.Factory.StartNew(() =>
                {
                    bool result = false;
                    while (result != true)
                    {
                        using (var trans = new TransactionScope())
                        {
                            repository.TableGroupUsersRemoveMultipleRows(g.users, g);
                            repository.TableGroupNotificationsRemoveMultipleRows(groupName);
                            repository.TableGroupsRemoveRow(g);
                            trans.Complete();
                        }
                        
                        result = true;
                    }
                });

                foreach (var user in g.users)
                {
                    user.groups.Remove(g);
                }
            });
        }


        public Task<bool> ModifyIsGroupUserAsync(string groupName, string userName, ModificationType modificationType)
        {
            return Task.Factory.StartNew<bool>(() =>
            {
                sharedData.rwGroupsLock.EnterUpgradeableReadLock();
                sharedData.rwUsersLock.EnterUpgradeableReadLock();
                User u = sharedData.users[userName];
                if (!sharedData.groups.ContainsKey(groupName))//this presents some weird scenarios where someone deleted the group as the user was leaving and so on...
                {
                    sharedData.rwUsersLock.ExitUpgradeableReadLock();
                    sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                    //pokud se tak skutečně stalo, tak určitě nemusím řešit odstranění connection z db
                    //a nemusím řešit ani odstranění z listů userů, to si prostě pořeší ten co maže skupinu...
                    return false;
                }
                else
                {
                    Group g = sharedData.groups[groupName];
                    GroupUser gu = new GroupUser { alias = StaticData.serverAlias, groupName = g.groupName, login = userName };
                    //holding the lock for the whole time (even for db operation)
                    //prevents some weird inconsistencies...
                    sharedData.rwGroupsLock.EnterWriteLock();
                    sharedData.rwUsersLock.EnterWriteLock();
                    if (modificationType == ModificationType.ADD)
                    {
                        AddNewGroupUserUnlocked(g, u);
                        repository.TableGroupUsersAddRow(gu);
                    }
                    else if (modificationType == ModificationType.REMOVE)
                    {
                        RemoveGroupUserUnlocked(g, u);
                        repository.TableGroupUsersRemoveRow(gu);
                    }
                    sharedData.rwUsersLock.ExitWriteLock();
                    sharedData.rwGroupsLock.ExitWriteLock();
                    sharedData.rwUsersLock.ExitUpgradeableReadLock();
                    sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                    return true;
                }

            });
        }

        public bool ModifyIsGroupUser(string groupName, string userName, ModificationType modificationType)
        {
            sharedData.rwGroupsLock.EnterUpgradeableReadLock();
            sharedData.rwUsersLock.EnterUpgradeableReadLock();
            User u = sharedData.users[userName];
            if (!sharedData.groups.ContainsKey(groupName))//this presents some weird scenarios where someone deleted the group as the user was leaving and so on...
            {
                sharedData.rwUsersLock.ExitUpgradeableReadLock();
                sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                //pokud se tak skutečně stalo, tak určitě nemusím řešit odstranění connection z db
                //a nemusím řešit ani odstranění z listů userů, to si prostě pořeší ten co maže skupinu...
                return false;
            }
            else
            {
                Group g = sharedData.groups[groupName];
                GroupUser gu = new GroupUser { alias = StaticData.serverAlias, groupName = g.groupName, login = userName };
                //holding the lock for the whole time (even for db operation)
                //prevents some weird inconsistencies...
                sharedData.rwGroupsLock.EnterWriteLock();
                sharedData.rwUsersLock.EnterWriteLock();
                if (modificationType == ModificationType.ADD)
                {
                    AddNewGroupUserUnlocked(g, u);
                    repository.TableGroupUsersAddRow(gu);
                }
                else if (modificationType == ModificationType.REMOVE)
                {
                    RemoveGroupUserUnlocked(g, u);
                    repository.TableGroupUsersRemoveRow(gu);
                }
                sharedData.rwUsersLock.ExitWriteLock();
                sharedData.rwGroupsLock.ExitWriteLock();
                sharedData.rwUsersLock.ExitUpgradeableReadLock();
                sharedData.rwGroupsLock.ExitUpgradeableReadLock();
                return true;
            }
        }

        //-----------------------------------------------------
        //----------------
        //---------------- DB MODIFYING
        //----------------
        //-----------------------------------------------------

        public Task ModifyGroupExistsAsync(Group g, GroupState groupState)
        {
            return Task.Factory.StartNew(() =>
            {
                if (groupState == GroupState.CREATED)
                {
                    //group was already added in TryAddGroupMethod, now i will just modify db
                    repository.TableGroupsAddRow(g);
                }
                else if (groupState == GroupState.DELETED)
                {
                    //i have specific version for delete...
                }
            });
        }

        public void ModifyGroupNotificationExists(GroupNotificationType groupNotificationType, string groupName, string sender, string receiver, ModificationType modificationType, string serverAlias)
        {
            switch (modificationType)
            {
                case ModificationType.ADD:
                    repository.TableGroupNotificationsAddRow(new GroupNotification
                    {
                        alias = serverAlias,
                        groupName = groupName,
                        sender = sender,
                        receiver = receiver,
                        type = groupNotificationType.ToString().ToLower()
                    });
                    break;
                case ModificationType.REMOVE:
                    repository.TableGroupNotificationsRemoveRow(new GroupNotification
                    {
                        alias = serverAlias,
                        groupName = groupName,
                        sender = sender,
                        receiver = receiver,
                        type = groupNotificationType.ToString().ToLower()
                    });
                    break;
            }

        }


        public Task ModifyGroupNotificationExistsAsync(GroupNotificationType groupNotificationType, string groupName, string sender, string receiver, ModificationType modificationType, string serverAlias)
        {
            return Task.Factory.StartNew(() =>
            {
                switch (modificationType)
                {
                    case ModificationType.ADD:
                        repository.TableGroupNotificationsAddRow(new GroupNotification
                        {
                            alias = serverAlias,
                            groupName = groupName,
                            sender = sender,
                            receiver = receiver,
                            type = groupNotificationType.ToString().ToLower()
                        });
                        break;
                    case ModificationType.REMOVE:
                        repository.TableGroupNotificationsRemoveRow(new GroupNotification
                        {
                            alias = serverAlias,
                            groupName = groupName,
                            sender = sender,
                            receiver = receiver,
                            type = groupNotificationType.ToString().ToLower()
                        });
                        break;
                }
            });
        }
        public Task ModifyFriendshipNotificationExistsAsync(FriendshipNotificationType friendshipNotification, string sender, string receiver, ModificationType modificationType)
        {
            return Task.Factory.StartNew(() =>
            {
                switch (modificationType)
                {
                    case ModificationType.ADD:
                        repository.TableFriendshipNotificationsAddRow(new FriendshipNotification { type = friendshipNotification.ToString().ToLower(), sender = sender, receiver = receiver });
                        break;
                    case ModificationType.REMOVE:
                        repository.TableFriendshipNotificationsRemoveRow(new FriendshipNotification { type = friendshipNotification.ToString().ToLower(), sender = sender, receiver = receiver });
                        break;
                }
            });
        }

        public void ModifyFriendshipNotificationExists(FriendshipNotificationType friendshipNotification, string sender, string receiver, ModificationType modificationType)
        {

            switch (modificationType)
            {
                case ModificationType.ADD:
                    repository.TableFriendshipNotificationsAddRow(new FriendshipNotification { type = friendshipNotification.ToString().ToLower(), sender = sender, receiver = receiver });
                    break;
                case ModificationType.REMOVE:
                    repository.TableFriendshipNotificationsRemoveRow(new FriendshipNotification { type = friendshipNotification.ToString().ToLower(), sender = sender, receiver = receiver });
                    break;
            }
        }

        public Task ModifyFriendshipRelationAsync(string friend1, string friend2, ModificationType modificationType)
        {
            return Task.Factory.StartNew(() =>
            {
                if (modificationType == ModificationType.ADD)
                {
                    repository.TableFriendsAddRow(new Friend { user1 = friend1, user2 = friend2 }, new Friend { user1 = friend2, user2 = friend1 });
                }
                else if (modificationType == ModificationType.REMOVE)
                {
                    repository.TableFriendsRemoveRow(new Friend { user1 = friend1, user2 = friend2 }, new Friend { user1 = friend2, user2 = friend1 });
                }
            });
        }


        public void ModifyFriendshipRelation(string friend1, string friend2, ModificationType modificationType)
        {

            if (modificationType == ModificationType.ADD)
            {
                repository.TableFriendsAddRow(new Friend { user1 = friend1, user2 = friend2 }, new Friend { user1 = friend2, user2 = friend1 });
            }
            else if (modificationType == ModificationType.REMOVE)
            {
                repository.TableFriendsRemoveRow(new Friend { user1 = friend1, user2 = friend2 }, new Friend { user1 = friend2, user2 = friend1 });
            }
        }

        //jedna operace s databází - jen odebere nebo vloží řádek, kterej říká jestli je user online na nějakém serveru
        public Task ChangeUserIsOnlineAsync(string user, UserStatus status)
        {
            return Task.Factory.StartNew(() =>
            {
                if (status == UserStatus.ONLINE)
                { 
                    repository.TableOnlineUsersAddRow(user);
                }
                else if (status == UserStatus.OFFLINE)
                {
                    repository.TableOnlineUsersRemoveRow(user);
                }
            });
        }

        public Task ModifyAnonymousUser(string userName, ModificationType modificationType)
        {
            return Task.Factory.StartNew(() =>
            {
                if (modificationType == ModificationType.ADD)
                    repository.TableUsersAddRow(userName);
                else if (modificationType == ModificationType.REMOVE)
                    repository.TableUsersRemoveRow(userName);
            });
        }

       

        public DataModifier(IRepository repository, SharedData sharedData)
        {
            this.repository = repository;
            this.sharedData = sharedData;
        }
    }
}
