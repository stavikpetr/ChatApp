﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppChattingServer02
{
    class DataAccesser : IDataAccesser
    {
        public IDataModifier dataModifier { get; set; }
        public IDataRetriever dataRetriever { get; set; }

        public DataAccesser(IDataModifier dataModifier, IDataRetriever dataRetriever)
        {
            this.dataModifier = dataModifier;
            this.dataRetriever = dataRetriever;
        }
    }
}
