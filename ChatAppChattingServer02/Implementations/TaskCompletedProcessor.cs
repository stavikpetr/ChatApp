﻿#define LOG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;


namespace ChatAppChattingServer02
{
    public enum MessageType { OK, FATAL, NEWCONNECTION, END };
    class TaskCompletedProcessor : ITaskCompletedProcessor
    {
        ITaskManager taskManager;
        BlockingCollection<Tuple<MessageType, IConnectionContext>> taskQueue;

        public void StartProcessing()
        {
            taskManager.NewListeningTaskAsync();
#if LOG
            Logger.logger.Log("task completed processor is starting to process");
#endif
            while (true)
            {
                var completedTask = taskQueue.Take();

                switch (completedTask.Item1)
                {
                    case MessageType.OK:
#if LOG
                        Logger.logger.Log("client was served OK");
#endif
                        taskManager.NewClientServingTaskAsync(completedTask.Item2);
                        break;
                    case MessageType.NEWCONNECTION:
#if LOG
                        Logger.logger.Log("new connection");
#endif
                        taskManager.NewListeningTaskAsync();
                        taskManager.NewClientServingTaskAsync(completedTask.Item2);
                        break;
                    case MessageType.END:
#if LOG
                        Logger.logger.Log("cliend ended OK");
#endif
                        completedTask.Item2.input.Close();
                        completedTask.Item2.output.Close();
                        completedTask.Item2.socket.Close();
                        break;
                    case MessageType.FATAL:
#if LOG
                        Logger.logger.Log("cliend ended in fatal");
#endif
                        completedTask.Item2.input.Close();
                        completedTask.Item2.output.Close();
                        completedTask.Item2.socket.Close();
                        break;
                }
            }
        }

        public TaskCompletedProcessor(BlockingCollection<Tuple<MessageType, IConnectionContext>> taskQueue, ITaskManager taskManager)
        {
            this.taskQueue = taskQueue;
            this.taskManager = taskManager;
        }
    }
}
