﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppChattingServer02
{
    class ClientConnection : IConnectionContext
    {
        public StreamReader input { get; private set; }

        public StreamWriter output { get; private set; }

        public Socket socket { get; private set; }

        public bool oneAwaitsEnd { get; set; } = false;

        public bool ended { get; set; } = false;

        public ClientConnection(StreamReader input, StreamWriter output, Socket socket)
        {
            this.input = input;
            this.output = output;
            this.socket = socket;
        }
    }
}
