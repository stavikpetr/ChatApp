﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.IO;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class TaskManager:ITaskManager
    {
        BlockingCollection<Tuple<MessageType, IConnectionContext>> taskQueue;
        IDataAccesser dataAccesser;
        Connecter connecter;
        Socket listener;
        public static UnexpectedEndingHandler unexpectedEndingHandler;
        IInputHandler inputHandler;
        

        public async Task NewListeningTaskAsync()
        {
            var newConnection = await connecter.NewAcceptingClientTask(listener);
            taskQueue.Add(newConnection);
        }

        public async Task NewClientServingTaskAsync(IConnectionContext connectionContext)
        {
            try
            {
                string input = await connectionContext.input.ReadLineAsync();
                Console.WriteLine($"Got message: {input}");
                string[] tokens = input.Split('|');
                if (input.StartsWith(MessageConstants.SIGN))
                {
                    await inputHandler.HandleSignInputAsync(connectionContext, tokens);
                }
                else if (input.StartsWith(MessageConstants.GROUP)) //GROUPSHOW GROUPLEAVE GROUPLEAVEADMIN GROUPNEW
                {
                    if (tokens[0] == MessageConstants.GROUP + MessageConstants.NEW)
                    {
                        await inputHandler.HandleGroupNewInputAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUP + MessageConstants.SHOW)
                    {
                        await inputHandler.HandleGroupShowInputAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUP + MessageConstants.LEAVE)
                    {
                        await inputHandler.HandleUserLeavesGroupAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUP + MessageConstants.LEAVE + MessageConstants.ADMIN)
                    {
                        await inputHandler.HandleAdminLeavesGroupAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUP + MessageConstants.JOIN)
                    {
                        await inputHandler.HandleGroupJoinRequestAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUPKICKORDER)
                    {
                        await inputHandler.HandleGroupKickOrderAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUPKICKINFO)
                    {
                        await inputHandler.HandleGroupKickInfoAsync(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.GROUPKICKINFOUNDERSTOOD)
                    {
                        await inputHandler.HandleGroupInfoUnderstood(connectionContext, tokens, GroupNotificationType.GROUPKICKINFOUNDERSTOOD);
                    }
                }
                else if (input.StartsWith(MessageConstants.FRIENDSHIP))
                {
                    var type = (FriendshipNotificationType)Enum.Parse(typeof(FriendshipNotificationType), tokens[0].ToUpper());
                    if (tokens[0].EndsWith(MessageConstants.ORDER))
                    {
                        await inputHandler.HandleFriendshipOrder(connectionContext, tokens, type);
                    }
                    else if (tokens[0].EndsWith(MessageConstants.INFO))
                    {
                        await inputHandler.HandleFriendshipAcceptedInfo(connectionContext, tokens, type);
                    }
                    else if (tokens[0].EndsWith(MessageConstants.UNDERSTOOD))
                    {
                        await inputHandler.HandleFriendshipInfoUnderstood(connectionContext, tokens, type);
                    }
                }
                else if (input.StartsWith(MessageConstants.MESSAGE))
                {
                    if (tokens[0] == MessageConstants.MESSAGE + MessageConstants.FRIEND)
                    {
                        await inputHandler.HandleFriendMessage(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.MESSAGE + MessageConstants.GROUP)
                    {
                        await inputHandler.HandleGroupMessage(connectionContext, tokens);
                    }
                }
                else if (input.StartsWith(MessageConstants.SEARCH))
                {
                    //search by email is turned off, this message can't be received
                    if (tokens[0] == MessageConstants.SEARCHBYEMAIL)
                    {
                        await inputHandler.HandleSearchByEMAIL(connectionContext, tokens);
                    }
                    else if (tokens[0] == MessageConstants.SEARCHBYUSERNAME)
                    {
                        await inputHandler.HandleSearchByUsername(connectionContext, tokens);
                    }
                }
                else if (input.StartsWith(MessageConstants.DISCONNECT))
                {
                    if (tokens[0] == MessageConstants.DISCONNECT)
                    {
                        await inputHandler.HandleDisconnect(connectionContext, tokens[1], false);
                    }
                }
                else if (input.StartsWith(MessageConstants.FRIEND + MessageConstants.CHANGEDSTATUS))
                {
                    await inputHandler.HandleFriendChangedStatusAsync(connectionContext, tokens);
                }
                else
                {
                    await inputHandler.WrongCommand(connectionContext);
                }

                if (tokens[0] != MessageConstants.DISCONNECT)
                    taskQueue.Add(new Tuple<MessageType, IConnectionContext>(MessageType.OK, connectionContext));
                else
                    taskQueue.Add(new Tuple<MessageType, IConnectionContext>(MessageType.END, connectionContext));

            }
            catch (IOException e)
            {

                Logger.logger.Log($"caught {e.GetType()}, message: {e.Message}, shutting client down");
                await unexpectedEndingHandler.Invoke(connectionContext);
                taskQueue.Add(new Tuple<MessageType, IConnectionContext>(MessageType.FATAL, connectionContext));
            }
            catch (Exception e)
            {
                Logger.logger.Log($"caught general exception in TaskManager: {e.Message}");
                await unexpectedEndingHandler.Invoke(connectionContext);
                taskQueue.Add(new Tuple<MessageType, IConnectionContext>(MessageType.FATAL, connectionContext));
            }

        }

        public TaskManager(BlockingCollection<Tuple<MessageType, IConnectionContext>> taskQueue, IDataAccesser dataAccesser, Socket listener, Connecter connecter, IMessageEnqueuer messageEnqueuer)
        {
            this.connecter = connecter;
            this.listener = listener;
            this.dataAccesser = dataAccesser;
            this.taskQueue = taskQueue;
            inputHandler = new InputHandler(dataAccesser, messageEnqueuer);
            unexpectedEndingHandler = new UnexpectedEndingHandler(inputHandler.HandleUnexpectedEndAsync);
        }
    }
}
