﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Transactions;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    class Repository : IRepository
    {
        //represents compiled query for left outer join used for returning friend server tuples.
       //--------------------------------------------------
        //---------
        //---------   MAINLY INIT PHASE
        //---------
        //--------------------------------------------------

        public ICollection<Server> GetServers()
        {
            LinkedList<Server> servers = new LinkedList<Server>();
            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = from s in context.Tservers select s;
                foreach (var server in query)
                {
                    servers.AddLast(server);
                }
            }
            return servers;
        }

        public ICollection<Group> GetGroups(string alias)
        {
            LinkedList<Group> groups = new LinkedList<Group>();
            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = context.Tgroups.Where(g => g.alias == alias).Select(g => g);
                foreach (var g in query)
                {
                    groups.AddLast(g);
                }
            }
            Console.WriteLine("completed");
            return groups;
        }

        public ICollection<GroupUser> GetGroupUsers(string alias)
        {
            LinkedList<GroupUser> groupUsers = new LinkedList<GroupUser>();
            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = from gu in context.TgroupUsers where gu.alias == alias select gu;
                foreach (var gu in query)
                {
                    groupUsers.AddLast(gu);
                }
            }
            return groupUsers;
        }


        public ICollection<SupportedCommand> GetSupportedCommands()
        {
            LinkedList<SupportedCommand> supportedCommands = new LinkedList<SupportedCommand>();
            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = from cmd in context.TsupportedCommands select cmd;
                foreach (var cmd in query)
                {
                    supportedCommands.AddLast(cmd);
                }
            }
            return supportedCommands;
        }

        //--------------------------------------------------
        //---------
        //---------   MAIN FUNCTIONS
        //---------
        //--------------------------------------------------

        public ICollection<UserWithStatus> GetFriendsWithStatuses(string user)
        {
            LinkedList<UserWithStatus> friends = new LinkedList<UserWithStatus>();

            using (var context = new MyContext(StaticData.connectionString))
            {
                var q = from f in context.Tfriends
                        where f.user1 == user
                        from o in context.TonlineUsers.Where(o => f.user2 == o.login).DefaultIfEmpty()
                        select new UserWithStatus(o.alias == null ? UserStatus.OFFLINE : UserStatus.ONLINE, f.user2);
                foreach (var friend in q)
                {
                    friends.AddLast(friend);
                }
            }

            return friends;
        }

        public ICollection<FriendshipNotification> GetFriendshipNotifications(string receiver)
        {
            LinkedList<FriendshipNotification> notifications = new LinkedList<FriendshipNotification>();

            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = from n in context.TfriendshipNotifications where n.receiver == receiver select n;
                foreach (var notification in query)
                {
                    notifications.AddLast(notification);
                }
            }

            return notifications;
        }

        public ICollection<GroupNotification> GetGroupNotifications(string receiver)
        {
            LinkedList<GroupNotification> notifications = new LinkedList<GroupNotification>();

            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = from n in context.TgroupNotifications where n.receiver == receiver select n;
                foreach (var notification in query)
                {
                    notifications.AddLast(notification);
                }
            }

            return notifications;
        }

        public ICollection<OnlineUser> GetOnlineFriendsWithServers(string user)
        {
            LinkedList<OnlineUser> onlineUsers = new LinkedList<OnlineUser>();
            using (var context = new MyContext(StaticData.connectionString))
            {
                var query = from f in context.Tfriends where f.user1 == user from o in context.TonlineUsers where f.user2 == o.login select o;
                foreach (var ou in query)
                {
                    onlineUsers.AddLast(ou);
                }
            }
            return onlineUsers;
        }

        //--------------------------------------------------
        //---------
        //---------   HELPERS
        //---------
        //--------------------------------------------------

        public UserWithStatus GetFriendStatus(string user, string friend)
        {
            UserWithStatus friendStatus = null;

            using (var context = new MyContext(StaticData.connectionString))
            {
                var q = from f in context.Tfriends
                               where f.user1 == user
                               from o in context.TonlineUsers.Where(o => f.user2 == o.login).DefaultIfEmpty()
                               select new UserWithStatus(o.alias == null ? UserStatus.OFFLINE : UserStatus.ONLINE, f.user2);
                foreach (var f in q)
                {

                    if (f.user == friend)
                    {
                        friendStatus = f;
                        break;
                    }
                }
            }
            return friendStatus;
        }

        public bool IsValidUser(string login, string password)
        {
            bool isValid;
            using (var context = new MyContext(StaticData.connectionString))
            {
                isValid = context.Tusers.Any(u => u.userName == login && u.password == password && u.isValidated == true);
            }
            return isValid;
        }

        public string DoesUserExistsByEmail(string email)
        {
            User user;
            using (var context = new MyContext(StaticData.connectionString))
            {
                user = context.Tusers.SingleOrDefault(u => u.email == email);
            }
            if (user == null)
                return null;
            return user.userName;
        }

        public bool DoesUserExistsByUsername(string userName)
        {
            bool exists = false;
            using (var context = new MyContext(StaticData.connectionString))
            {
                exists = context.Tusers.Any(u => u.userName == userName);
            }
            return exists;
        }

        public OnlineUser TryGetOnlineUser(string userName)
        {
            OnlineUser onlineUser;
            using (var context = new MyContext(StaticData.connectionString))
            {
                onlineUser = context.TonlineUsers.FirstOrDefault(oU => oU.login == userName);
            }
            return onlineUser;
        }

        //--------------------------------------------------
        //---------
        //---------   DB MODIFYING
        //---------
        //--------------------------------------------------

        public void TableGroupUsersRemoveRow(GroupUser gu)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                var toRemove = context.TgroupUsers.FirstOrDefault(gU => gU.alias == gu.alias && gU.groupName == gu.groupName && gU.login == gu.login);
                context.TgroupUsers.DeleteOnSubmit(toRemove);
                context.SubmitChanges();
            }
        }

        public void TableGroupUsersAddRow(GroupUser gu)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                context.TgroupUsers.InsertOnSubmit(gu);
                context.SubmitChanges();
            }
        }

        public void TableOnlineUsersRemoveRow(string user)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                var toRemove = context.TonlineUsers.FirstOrDefault(u => u.login == user && u.alias == StaticData.serverAlias);
                context.TonlineUsers.DeleteOnSubmit(toRemove);
                context.SubmitChanges();
            }
        }

        public void TableOnlineUsersAddRow(string user)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                OnlineUser u = new OnlineUser { alias = StaticData.serverAlias, login = user };
                context.TonlineUsers.InsertOnSubmit(u);
                context.SubmitChanges();
            }
        }

        public void TableGroupsAddRow(Group g)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                context.Tgroups.InsertOnSubmit(g);
                context.SubmitChanges();
            }
        }

        public void TableGroupsRemoveRow(Group g)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                var toRemove = context.Tgroups.FirstOrDefault(g_ => g_.admin == g.admin && g_.alias == g.alias
                && g_.description == g.description && g_.groupName == g.groupName && g_.type == g.type);
                context.Tgroups.DeleteOnSubmit(toRemove);
                context.SubmitChanges();

            }
        }

        public void TableGroupUsersRemoveMultipleRows(ICollection<User> users, Group g)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                var q = from gu in context.TgroupUsers where gu.groupName == g.groupName && gu.alias == g.alias select gu;
                foreach (var user in q)
                {
                    context.TgroupUsers.DeleteOnSubmit(user);

                }
                context.SubmitChanges();
            }
        }


        public void TableGroupNotificationsAddRow(GroupNotification g)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                bool alreadyExists = context.TgroupNotifications.Any(gn => (gn.type == GroupNotificationType.GROUPKICKINFO.ToString().ToLower() &&
                gn.sender == g.sender && gn.receiver == g.receiver && g.alias == gn.alias && gn.groupName == g.groupName));
                if (!alreadyExists)
                {
                    context.TgroupNotifications.InsertOnSubmit(g);
                    context.SubmitChanges();
                }
            }
        }

        public void TableGroupNotificationsRemoveRow(GroupNotification g)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                bool exists = context.TgroupNotifications.Any(gn => (gn.type == GroupNotificationType.GROUPKICKINFO.ToString().ToLower() &&
                gn.sender == g.sender && gn.alias == g.alias && g.groupName == gn.groupName && g.receiver == gn.receiver));
                if (exists)
                {
                    var toRemove = context.TgroupNotifications.First(gN => gN.alias == g.alias && gN.groupName == g.groupName
                    && gN.receiver == g.receiver && gN.sender == g.sender && gN.type == g.type);
                    context.TgroupNotifications.DeleteOnSubmit(toRemove);
                    context.SubmitChanges();
                }
            }
        }

        public void TableGroupNotificationsRemoveMultipleRows(string groupName)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                var q = from gn in context.TgroupNotifications where gn.groupName == groupName && gn.alias == StaticData.serverAlias select gn;
                foreach (var groupNotification in q)
                {
                    context.TgroupNotifications.DeleteOnSubmit(groupNotification);
                }
                context.SubmitChanges();
            }
        }


        public void TableFriendshipNotificationsAddRow(FriendshipNotification f)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                //preventing some super weird and rare scenarios...
                //bool alreadyExists = context.TfriendshipNotifications.Any(fn => ((fn.type == FriendshipNotificationType.FRIENDSHIPREQUESTINFO.ToString().ToLower() && ((fn.sender == f.sender && fn.receiver == f.receiver) || (fn.sender == f.receiver && fn.receiver == f.sender)))
                //|| (fn.type == FriendshipNotificationType.FRIENDSHIPACCEPTINFO.ToString().ToLower() && ((fn.sender == f.sender && fn.receiver == f.receiver) || (fn.sender == f.receiver && fn.receiver == f.sender)))
                //|| (fn.type == FriendshipNotificationType.FRIENDSHIPREFUSEINFO.ToString().ToLower() && ((fn.sender == f.sender && fn.receiver == f.receiver) || (fn.sender == f.receiver && fn.receiver == f.sender))
                //)));
                bool alreadyExists = context.TfriendshipNotifications.Any(fn => (fn.type == f.type && fn.sender == f.sender && fn.receiver == f.receiver ));
                if (!alreadyExists)
                {
                    context.TfriendshipNotifications.InsertOnSubmit(f);
                    context.SubmitChanges();
                }

            }
        }
        public void TableFriendshipNotificationsRemoveRow(FriendshipNotification f)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                //preventing some super weird and rare scenarios...
                var toRemove = context.TfriendshipNotifications.FirstOrDefault(fN => fN.sender == f.sender &&
                fN.receiver == f.receiver && fN.type == f.type);

                if (toRemove != null)
                {
                    context.TfriendshipNotifications.DeleteOnSubmit(toRemove);
                    context.SubmitChanges();
                }
            }
        }

        public void TableFriendsAddRow(Friend f1, Friend f2)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                context.Tfriends.InsertOnSubmit(f1);
                context.Tfriends.InsertOnSubmit(f2);
                context.SubmitChanges();
            }
        }

        public void TableFriendsRemoveRow(Friend f1, Friend f2)
        {
            using (var context = new MyContext(StaticData.connectionString))
            {
                var toRemove1 = context.Tfriends.First(f_ => f_.user1 == f1.user1 && f_.user2 == f1.user2);
                var toRemove2 = context.Tfriends.First(f_ => f_.user1 == f2.user1 && f_.user2 == f2.user2);
                context.Tfriends.DeleteOnSubmit(toRemove1);
                context.Tfriends.DeleteOnSubmit(toRemove2);
                context.SubmitChanges();
            }
        }

        public void TableUsersAddRow(string userName)
        {
            var key = userName.Remove(0, 9); //lets remove the ANONYMOUS...
            using (var context = new MyContext(StaticData.connectionString))
            {
                User u = new User { email = key, validationKey = key, userName = userName, isValidated = true, password = key };
                OnlineUser ou = new OnlineUser { alias = StaticData.serverAlias, login = userName };
                context.Tusers.InsertOnSubmit(u);
                context.TonlineUsers.InsertOnSubmit(ou);
                context.SubmitChanges();
            }
        }

        public void TableUsersRemoveRow(string userName)
        {
            var key = userName.Remove(0, 9);
            using (var context = new MyContext(StaticData.connectionString))
            {
                //also remove all groupNOtifications
                //also remove from onlineUsers..
                var onlineUserToRemove = context.TonlineUsers.First(u => u.login == userName);
                context.TonlineUsers.DeleteOnSubmit(onlineUserToRemove);

                var notificationsToRemove = context.TgroupNotifications.Where(gn => gn.receiver == userName);
                context.TgroupNotifications.DeleteAllOnSubmit(notificationsToRemove);

                //and also, all info about group users...
                var groupUsersToRemove = context.TgroupUsers.Where(gu => gu.login == userName);
                context.TgroupUsers.DeleteAllOnSubmit(groupUsersToRemove);

                var toRemove = context.Tusers.First(u => u.userName == userName);
                context.Tusers.DeleteOnSubmit(toRemove);
                context.SubmitChanges();
            }
        }
    }
}
