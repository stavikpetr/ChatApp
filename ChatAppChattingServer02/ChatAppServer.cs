﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Concurrent;
using System.Data.SqlClient;

namespace ChatAppChattingServer02
{
    delegate Task UnexpectedEndingHandler(IConnectionContext cc);
    class ChatAppServer
    {
        private IPAddress myAddress;
        private int myPort;
        private string serverAlias;
        
        public void StartWorking()
        {
            StaticData.port = myPort;
            StaticData.ipAddress = myAddress;
            StaticData.serverAlias = serverAlias;
            StaticData.connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;Initial Catalog=ChatAppTesting;Integrated Security=True;Pooling=True";

            //lets Test DbConnection
            using (var testConnection= new SqlConnection(StaticData.connectionString))
            {
                try
                {
                    testConnection.Open();                    
                }
                catch (SqlException)
                {
                    Console.WriteLine("can't join to db, shutting down");
                    return;
                }
            }

            SharedData sharedData = new SharedData();
            IRepository repository = new Repository();
            IDataAccesser dataAccesser = new DataAccesser(new DataModifier(repository, sharedData), new DataRetriever(repository, sharedData));
            IMessageWriter messageWriter = new MessageWriter();
            IMessageEnqueuer messageEnqueuer = new MessageEnqueuer(messageWriter);


            Initializer init = new Initializer();
            Connecter connecter = new Connecter();
            init.InitializeAsync(dataAccesser, connecter, messageEnqueuer).Wait();

            IPEndPoint localEP = new IPEndPoint(myAddress, myPort);
            Socket listener = new Socket(SocketType.Stream, ProtocolType.Tcp);
            listener.DualMode = true;
            listener.Bind(localEP);
            

            BlockingCollection<Tuple<MessageType, IConnectionContext>> taskQueue = new BlockingCollection<Tuple<MessageType, IConnectionContext>>();
            ITaskManager taskManager = new TaskManager(taskQueue, dataAccesser, listener, connecter, messageEnqueuer);

            ITaskCompletedProcessor taskCompletedProcessor = new TaskCompletedProcessor(taskQueue, taskManager);

            //hanging for readline on other servers which i connected to
            foreach (var connection in dataAccesser.dataRetriever.GetConnections())
            {
                taskManager.NewClientServingTaskAsync(connection);
            }

            listener.Listen(0);
            Console.WriteLine("initialization completed, starting the main loop");

            //-----------------------------------------
            //-------------
            //-------------INIT PART IS OVER, START THE MAIN LOOPS
            //-------------
            //-----------------------------------------
            Thread t = new Thread(() =>
            {
                messageWriter.WriteMessages();
            });
            t.Start();

            taskCompletedProcessor.StartProcessing();
        }

        public ChatAppServer(IPAddress ipAddress, int port, string serverAlias)
        {
            this.myAddress = ipAddress;
            this.myPort = port;
            this.serverAlias = serverAlias;
        }
    }
}
