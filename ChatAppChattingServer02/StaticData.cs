﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace ChatAppChattingServer02
{
    public static class StaticData
    {
        public static int port { get; set; }
        public static IPAddress ipAddress { get; set; }
        public static string connectionString { get; set; }
        public static string serverAlias { get; set; }
    }
}
