﻿using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Net;
using System;
using System.Collections.Generic;
using System.Threading;
using ChatAppCommon;
namespace ChatAppChattingServer02
{
    public class MyContext : DataContext
    {
        public Table<Server> Tservers { get; set; }
        public Table<User> Tusers { get; set; }
        public Table<Group> Tgroups { get; set; }
        public Table<GroupUser> TgroupUsers { get; set; }
        public Table<OnlineUser> TonlineUsers { get; set; }
        public Table<Friend> Tfriends { get; set; }
        public Table<FriendshipNotification> TfriendshipNotifications { get; set; }
        public Table<GroupNotification> TgroupNotifications { get; set; }
        public Table<SupportedCommand> TsupportedCommands { get; set; }

        public MyContext(string connectionString) : base(connectionString)
        {
            Tservers = this.GetTable<Server>();
            Tusers = this.GetTable<User>();
            Tgroups = this.GetTable<Group>();
            TgroupUsers = this.GetTable<GroupUser>();
            TonlineUsers = this.GetTable<OnlineUser>();
            Tfriends = this.GetTable<Friend>();
            TfriendshipNotifications = this.GetTable<FriendshipNotification>();
            TgroupNotifications = this.GetTable<GroupNotification>();
            TsupportedCommands = this.GetTable<SupportedCommand>();
        }
    }

    [Table(Name = "Servers")]
    public class Server
    {
        [Column(IsPrimaryKey = true)]
        public string alias { get; set; }
        [Column(Name = "address")]
        public string address { get { return IpAddress.ToString(); } set { IpAddress = IPAddress.Parse(value); } }
        [Column]
        public int port { get; set; }

        public IPAddress IpAddress { get; set; }
    }


    [Table(Name = "groups")]
    public class Group
    {
        [Column(IsPrimaryKey = true)]
        public string alias { get; set; }
        [Column(IsPrimaryKey = true)]
        public string groupName { get; set; }
        [Column(Name = "description")]
        public string description { get; set; }
        [Column(Name = "type")]
        public string type { get { return groupType.ToString().ToLower(); } set { groupType = (GroupType)Enum.Parse(typeof(GroupType), value.ToUpper()); } }
        [Column(Name = "admin")]
        public string admin { get; set; }

        public GroupType groupType { get; set; }

        public ICollection<User> users { get; set; }
        public Group()
        {
            users = new LinkedList<User>();
        }
    }


    [Table(Name = "users")]
    public class User
    {
        public UserStatus userStatus { get; set; }
        [Column(Name = "login", IsPrimaryKey = true)]
        public string userName { get; set; }

        [Column(Name = "password")]
        public string password { get; set; }

        [Column(Name = "email")]
        public string email { get; set; }

        [Column(Name = "validationKey")]
        public string validationKey { get; set; }

        [Column(Name ="isValidated")]
        public bool isValidated { get; set; }

        public ICollection<Group> groups { get; set; }
    }

    [Table(Name = "groupNotifications")]
    public class GroupNotification
    {
        [Column(IsPrimaryKey = true)]
        public string sender { get; set; }
        [Column(IsPrimaryKey = true)]
        public string receiver { get; set; }
        [Column(IsPrimaryKey = true)]
        public string type { get { return groupNotification.ToString().ToLower(); } set { groupNotification = (GroupNotificationType)Enum.Parse(typeof(GroupNotificationType), value.ToUpper()); } }
        [Column(IsPrimaryKey = true)]
        public string alias { get; set; }
        [Column(IsPrimaryKey = true)]
        public string groupName { get; set; }


        public GroupNotificationType groupNotification { get; set; }
    }

    [Table(Name = "friendshipNotifications")]
    public class FriendshipNotification
    {
        [Column(IsPrimaryKey = true)]
        public string sender { get; set; }
        [Column(IsPrimaryKey = true)]
        public string receiver { get; set; }
        [Column(IsPrimaryKey = true, Name = "Type")]
        public string type { get { return friendshipNotification.ToString().ToLower(); } set { friendshipNotification = (FriendshipNotificationType)Enum.Parse(typeof(FriendshipNotificationType), value.ToUpper()); } }
       
        public FriendshipNotificationType friendshipNotification { get; set; }
    }

    public class UserWithStatus
    {
        public UserStatus status { get; set; }
        public string user { get; set; }
        public UserWithStatus(UserStatus status, string user)
        {
            this.status = status;
            this.user = user;
        }
    }

    [Table(Name = "SupportedCommands")]
    public class SupportedCommand
    {
        [Column(IsPrimaryKey = true)]
        public int id { get; set; }
        [Column(Name = "commandSignature")]
        public string commandSignature { get; set; }
        [Column(Name = "commandDescription")]
        public string commandDescription { get; set; }
    }

    [Table(Name ="OnlineUsers")]
    public class OnlineUser
    {
        [Column(IsPrimaryKey = true)]
        public string login { get; set; }
        [Column(IsPrimaryKey = true)]
        public string alias { get; set; }
    }

    [Table(Name = "GroupUsers")]
    public class GroupUser
    {
        [Column(IsPrimaryKey = true)]
        public string alias { get; set; }

        [Column(IsPrimaryKey = true)]
        public string groupName { get; set; }

        [Column(IsPrimaryKey = true)]
        public string login { get; set; }
    }

    [Table(Name = "Friends")]
    public class Friend
    {
        [Column(IsPrimaryKey = true)]
        public string user1;

        [Column(IsPrimaryKey = true)]
        public string user2;
    }


}