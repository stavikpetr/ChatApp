﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ChatAppChattingServer02
{
    class SharedData
    {
        public ICollection<Server> servers { get; set; }
        public Dictionary<string, IConnectionContext> activeConnections { get; set; }

        public Dictionary<string, Group> groups { get; set; }
        public Dictionary<string, User> users { get; set; }

        public ReaderWriterLockSlim rwActiveConnectionsLock { get; set; }
        public ReaderWriterLockSlim rwServersLock { get; set; }
        public ReaderWriterLockSlim rwGroupsLock { get; set; }
        public ReaderWriterLockSlim rwUsersLock { get; set; }

        public SharedData()
        {
            rwActiveConnectionsLock = new ReaderWriterLockSlim();
            rwServersLock = new ReaderWriterLockSlim();
            rwGroupsLock = new ReaderWriterLockSlim();
            rwUsersLock = new ReaderWriterLockSlim();

            activeConnections = new Dictionary<string, IConnectionContext>();
            servers = new LinkedList<Server>();
            groups = new Dictionary<string, Group>();
            users = new Dictionary<string, User>();
        }
    }
}
