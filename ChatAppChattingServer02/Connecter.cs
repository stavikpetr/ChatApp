﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ChatAppChattingServer02
{
    class Connecter
    {
        public async Task ConnectToKnownServersAsync(ICollection<Server> servers, IDataModifier dataModifier, IMessageEnqueuer messageEnqueuer)
        {
            foreach (var server in servers)
            {
                if ((server.IpAddress != StaticData.ipAddress) && (server.port != StaticData.port))
                {
                    Socket s = await ConnectAsync(server.IpAddress, server.port);
                    if (s != null)
                    {
                        NetworkStream ns = new NetworkStream(s);
                        ServerConnection sc = new ServerConnection(new StreamReader(ns), new StreamWriter(ns), s);
                        dataModifier.AddNewConnection(server.alias, sc);
                        messageEnqueuer.SendServerHello(sc, StaticData.serverAlias);
                    }
                }
            }
        }

        private Task<Socket> ConnectAsync(IPAddress IpAddressTo, int portNumber)
        {
            return Task.Factory.StartNew<Socket>(() =>
            {
                IPEndPoint EP = new IPEndPoint(IpAddressTo, portNumber);
                Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);

                IAsyncResult result = socket.BeginConnect(EP.Address, EP.Port, null, null);

                //let's wait for 5 seconds
                result.AsyncWaitHandle.WaitOne(5000, true);
                if (!socket.Connected)
                {
                    socket.Close();
                    socket = null;
                }

                return socket;
            });
        }

        public async Task<Tuple<MessageType, IConnectionContext>> NewAcceptingClientTask(Socket listener)
        {
            Socket s = await AsyncAccept(listener);
            NetworkStream ns = new NetworkStream(s);
            IConnectionContext cs;
            //now i can't possibly know if it is server or client connection, i will resolve that
            //in taskManager based on clients/servers first message
            cs = new Connection(new StreamReader(ns), new StreamWriter(ns), s);     
            return new Tuple<MessageType, IConnectionContext>(MessageType.NEWCONNECTION, cs);
        }

        private Task<Socket> AsyncAccept(Socket listener)
        {
            TaskCompletionSource<Socket> tcs = new TaskCompletionSource<Socket>();
            SocketAsyncEventArgs args = new SocketAsyncEventArgs();

            args.Completed += new EventHandler<SocketAsyncEventArgs>((_, e) =>
            {
                tcs.SetResult(args.AcceptSocket);
            });

            bool willRaiseEvent = listener.AcceptAsync(args);
            if (!willRaiseEvent)
            {
                tcs.SetResult(args.AcceptSocket);
            }

            return tcs.Task;
        }
    }
}
