﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ChatAppChattingServer02
{
    class Message
    {
        public StreamWriter output { get; set; }
        public string message { get; set; }
        public IConnectionContext connectionContext { get; set; }
    }
}
