﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ChatAppChattingServer02
{
    class Initializer
    {
        public async Task InitializeAsync(IDataAccesser dataAccesser, Connecter connecter, IMessageEnqueuer messageEnqueuer)
        {
            var getGroups = dataAccesser.dataRetriever.GetGroupsAsync(StaticData.serverAlias).
                ContinueWith(t1 => dataAccesser.dataModifier.AddGroupsAsync(t1.Result));

            var getServers = dataAccesser.dataRetriever.GetServersAsync();
            var connectToServers = connecter.ConnectToKnownServersAsync(await getServers, dataAccesser.dataModifier, messageEnqueuer); 
            var addServers = dataAccesser.dataModifier.AddServersAsync(await getServers); 

            var getGroupUsersAsync = dataAccesser.dataRetriever.GetGroupUsersAsync(StaticData.serverAlias);
            await getGroups.Result;
            var addGroupUsers = dataAccesser.dataModifier.AddGroupUsersAsync(await getGroupUsersAsync);
            await Task.WhenAll(new Task[] { connectToServers, addServers, addGroupUsers});
        }

    }
}
