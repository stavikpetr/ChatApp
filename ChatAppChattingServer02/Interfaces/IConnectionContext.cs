﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace ChatAppChattingServer02
{
    interface IConnectionContext
    {
        StreamReader input { get; }
        StreamWriter output { get; }
        Socket socket { get; }

        /*these two flags are solely for disconnect from user, in rare situations, i was closing
         * the output stream and writing dummy message to it, so, that of course raised exception, 
         * in current architecture, the only reasonable solution are these two flags */
        bool oneAwaitsEnd { get; set; }
        bool ended { get; set; }
    }
}
