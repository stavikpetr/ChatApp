﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppChattingServer02
{
    interface IRepository
    {
        //----------
        //MAINLY INIT PHASE
        //----------
        ICollection<Server> GetServers();
        ICollection<Group> GetGroups(string alias);
        ICollection<GroupUser> GetGroupUsers(string alias);

        //----------
        //MAIN FUNCTIONS
        //----------
        ICollection<SupportedCommand> GetSupportedCommands();
        ICollection<FriendshipNotification> GetFriendshipNotifications(string receiver);
        ICollection<GroupNotification> GetGroupNotifications(string receiver);
        ICollection<UserWithStatus> GetFriendsWithStatuses(string user);
        ICollection<OnlineUser> GetOnlineFriendsWithServers(string user);


        //----------
        //HELPERS
        //----------
        UserWithStatus GetFriendStatus(string user, string friend);
        bool IsValidUser(string login, string password);
        OnlineUser TryGetOnlineUser(string userName);
        string DoesUserExistsByEmail(string email);
        bool DoesUserExistsByUsername(string userName);

        //----------
        //DB MODIFYING
        //----------

        void TableOnlineUsersRemoveRow(string user);
        void TableOnlineUsersAddRow(string user);

        void TableGroupUsersAddRow(GroupUser groupUser);
        void TableGroupUsersRemoveRow(GroupUser groupUser);
        void TableGroupUsersRemoveMultipleRows(ICollection<User> users, Group g);

        void TableGroupsAddRow(Group g);
        void TableGroupsRemoveRow(Group g);

        void TableGroupNotificationsAddRow(GroupNotification g);
        void TableGroupNotificationsRemoveRow(GroupNotification g);
        void TableGroupNotificationsRemoveMultipleRows(string groupName);

        void TableFriendshipNotificationsAddRow(FriendshipNotification f);
        void TableFriendshipNotificationsRemoveRow(FriendshipNotification f);

        void TableFriendsAddRow(Friend f1, Friend f2);
        void TableFriendsRemoveRow(Friend f1, Friend f2);

        //these are for inserting temporary anonymous user...
        void TableUsersAddRow(string userName);
        void TableUsersRemoveRow(string userName);
    }
}
