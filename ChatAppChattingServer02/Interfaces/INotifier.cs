﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;
namespace ChatAppChattingServer02
{
    interface INotifier
    {
        Task FriendChangedStatusSingleFriendAsync(string friend, string whoToNotify, UserStatus status);
        Task FriendChangedStatusAsync(string user, UserStatus status);
        Task GroupUserChangedStatusAllGroupsAsync(string user, UserStatus status);
        Task GroupUserChangedStatusAsync(string user, string groupName, UserStatus status);
        Task ServerGroupChangedStatusAsync(string groupName, GroupState groupState, GroupType groupType, string admin, string description);
    }
}
