﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppChattingServer02
{
    interface IMessageWriter
    {
        void WriteMessages();
        void AddMessage(Message m);
    }
}
