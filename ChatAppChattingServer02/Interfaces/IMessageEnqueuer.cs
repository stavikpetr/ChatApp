﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    interface IMessageEnqueuer
    {
        //messages related to joining of user
        //these three are also used for refresh..., they start either with INIT| or with REFRESH...|, depends on prepender...
        Task SendServerGroupsAsync(ICollection<Group> groups, IConnectionContext connectionContext, string prepender);
        Task SendUserGroupsAsync(ICollection<Group> groups, IConnectionContext connectionContext, string prepender);
        Task SendFriendsInfoAsync(ICollection<UserWithStatus> friends, IConnectionContext connectionContext, string prepender);

        Task SendFriendshipNotificationsAsync(ICollection<FriendshipNotification> notifications, IConnectionContext connectionContext);
        Task SendGroupNotificationsAsync(ICollection<GroupNotification> notifications, IConnectionContext connectionContext);
        Task SendSupportedCommandsAsnyc(ICollection<SupportedCommand> supportedCommands, IConnectionContext connectionContext);
        void SendIncorrectCredentials(IConnectionContext connectionContext);
        void SendAlreadyConnected(IConnectionContext connectionContext);
        void SendSignOK(IConnectionContext connectionContext);
        void SendInitPhaseString(IConnectionContext connectionContext, string user);
        void SendInitPhaseOverString(IConnectionContext connectionContext);

        //GROUPRELATED
        void SendGroupCreateSuccess(string groupName, string type, string admin, string description, IConnectionContext connectionContext);
        void SendGroupCreateFail(string groupName, IConnectionContext connectionContext);

        Task SendShowGroupUsersOKAsync(ICollection<UserWithStatus> usersInGroup, IConnectionContext connectionContext, string groupName);
        void SendShowGroupUsersERR(IConnectionContext connectionContext, string groupName);

        void SendGroupJoinSuccess(IConnectionContext connectionContext, string groupName);
        void SendGroupJoinFailBeingDeleted(IConnectionContext connectionContext, string groupName);
        void SendGroupJoinFailNonExisting(IConnectionContext connectionContext, string groupName);

        void SendGroupNotification(IConnectionContext connectionContext, GroupNotificationType groupNotificationType, string groupName, string sender, string receiver, string alias);


        //FRIENDSHIP RELATED
        void SendFriendshipNotification(IConnectionContext connectionContext, FriendshipNotificationType friendshipNotificationType, string sender, string receiver);
        
        //MESSAGES
        void SendGroupMessage(IConnectionContext connectionContext, string sender, string groupName, string message);
        void SendFriendMessage(IConnectionContext connectionContext, string sender, string receiver, string message);

        //SEARCHES
        void SendSearchResultOK(IConnectionContext connectionContext);
        void SendSearchResultFail(IConnectionContext connectionContext);



        void SendFriendChangedStatus(string userThatChangedStatus, IConnectionContext connectionContext, UserStatus status);
        void SendUserInGroupChangedStatus(string userThatChangedStatus, IConnectionContext connectionContext, UserStatus status, string groupName);
        void SendServerGroupChangedStatus(string groupThatChangedStatus, IConnectionContext connectionContext, GroupState state, GroupType groupType, string admin, string description);


        void SendServerHello(IConnectionContext connectionContext, string myAlias);
        void SendWrongCommandErrorMessage(IConnectionContext connectionContext);

        //REFRESH
        void SendRefreshOver(IConnectionContext connectionContext);

        //dummy - used for disconnect...
        void SendDummy(IConnectionContext connectionContext);
    }
}
