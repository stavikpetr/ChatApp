﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    interface IInputHandler
    {
        //join
        Task HandleSignInputAsync(IConnectionContext connectionContext, string[] tokens);

        //----------
        //GROUP RELATED
        //----------
        Task HandleGroupNewInputAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleGroupShowInputAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleAdminLeavesGroupAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleUserLeavesGroupAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleGroupJoinRequestAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleGroupKickOrderAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleGroupKickInfoAsync(IConnectionContext connectionContext, string[] tokens);
        Task HandleGroupInfoUnderstood(IConnectionContext connectionContext, string[] tokens, GroupNotificationType groupNotificationType);

        //----------
        //FRIENDSHIP RELATED
        //----------
        Task HandleFriendshipOrder(IConnectionContext connectionContext, string[] tokens, FriendshipNotificationType friendshipNotification);
        Task HandleFriendshipAcceptedInfo(IConnectionContext connectionContext, string[] tokens, FriendshipNotificationType friendshipNotificationType);
        Task HandleFriendshipInfoUnderstood(IConnectionContext connectionContext, string[] tokens, FriendshipNotificationType friendshipNotificationType);

        //----------
        //MESSAGES
        //----------
        Task HandleGroupMessage(IConnectionContext connectionContext, string[] tokens);
        Task HandleFriendMessage(IConnectionContext connectionContext, string[] tokens);

        //----------
        //SEARCHES
        //----------
        Task HandleSearchByUsername(IConnectionContext connectionContext, string[] tokens);
        Task HandleSearchByEMAIL(IConnectionContext connectionContext, string[] tokens);

        Task HandleDisconnect(IConnectionContext connectionContext, string userName, bool isFatal);

        Task WrongCommand(IConnectionContext connectionContext);

        Task HandleUnexpectedEndAsync(IConnectionContext connectionContext);

        //---------
        //FRIEND CHANGED STATUS (from another server...)
        //--------
        Task HandleFriendChangedStatusAsync(IConnectionContext connectionContext, string[] tokens);
        
    }
}
