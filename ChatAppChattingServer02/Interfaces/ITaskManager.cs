﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppChattingServer02
{
    interface ITaskManager
    {
         Task NewListeningTaskAsync();
         Task NewClientServingTaskAsync(IConnectionContext connectionContext);
    }
}
