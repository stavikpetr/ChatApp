﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppChattingServer02
{
    interface IDataAccesser
    {
        IDataRetriever dataRetriever { get; set; }
        IDataModifier dataModifier { get; set; }
    }
}
