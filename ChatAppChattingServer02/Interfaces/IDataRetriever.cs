﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    interface IDataRetriever
    {

        //----------
        //WORKING WITH DB
        //----------

        //initialization part
        Task<ICollection<Server>> GetServersAsync();
        Task<ICollection<Group>> GetGroupsAsync(string alias);
        Task<ICollection<GroupUser>> GetGroupUsersAsync(string alias);
      
        //user joined
        Task<ICollection<FriendshipNotification>> GetFriendshipNotificationsAsync(string receiver);
        Task<ICollection<GroupNotification>> GetGroupNotificationsAsync(string receiver);
        Task<ICollection<SupportedCommand>> GetSupportedCommands();

        Task<ICollection<OnlineUser>> GetOnlineFriendsWithServersAsync(string user);
        Task<ICollection<UserWithStatus>> GetFriendsWithStatusesAsync(string user);

        //helpers
        Task<UserWithStatus> GetFriendStatusAsync(string user, string friend);
        UserWithStatus GetFriendStatus(string user, string friend);
        Task<bool> IsValidUserAsync(string login, string password);
        Task<OnlineUser> TryGetOnlineUser(string userName);
        Task<string> DoesUserExistsByEmail(string email);
        Task<bool> DoesUserExistsByName(string userName);
        string GetUserNameFromConnectionContext(IConnectionContext connectionContext);
        ICollection<IConnectionContext> GetConnections();//serves for initialize part, to read from other servers

        //----------
        //WORKING WITH SHARED DATA
        //----------
        Task<ICollection<Group>> GetServerGroupsAsync();
        Task<ICollection<Group>> GetUserGroupsAsync(string user);
        Task<ICollection<UserWithStatus>> GetGroupUsersToShowAsync(string groupName);

        //these methods are used for notifying users about user status change, group change on server etc
        //such methods already run in separate notifying task, there is no point in wrapping them as tasks
        //Task<ICollection<Group>> GetUserGroups(string user);
        Task<ICollection<User>> GetUsersInGroupAsync(string group);
        Task<ICollection<string>> GetAllOnlineServerUsersAsync();

        //wrapping these in task would be huge overkill...
        User TryGetUser(string login);
        bool IsValidServerGroup(string groupName, string admin, GroupType groupType);
        IConnectionContext TryRetrieveConnectionContext(string of);
    }
}
