﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;

namespace ChatAppChattingServer02
{
    interface IDataModifier
    {
        void AddNewConnection(string key, IConnectionContext connectionContext);


        //-----------
        //SHARED DATA MODIFYING
        //-----------
        //initialization mainly
        Task AddServersAsync(ICollection<Server> servers);
        void AddNewServer(Server server);      
        Task AddGroupsAsync(ICollection<Group> groups);
        void AddNewGroup(Group group);
        Task AddGroupUsersAsync(ICollection<GroupUser> groupUsersQuery);
        void AddNewGroupUser(Group group, User user);
        void AddNewUser(User user);

        void RemoveGroupUserLocked(Group g, string userName);
        void RemoveUser(string userName);

        bool TryAddGroup(Group g, string groupName);

        void ModifyUserStatus(string login, UserStatus userStatus);

        //-----------
        //DB AND SHARED DATA MODIFYING
        //-----------
        Task DeleteGroupAsync(string groupName, string admin);       
        Task<bool> ModifyIsGroupUserAsync(string groupName, string userName, ModificationType modificationType); //will also modify db
        bool ModifyIsGroupUser(string groupName, string userName, ModificationType modificationType); //non async option - beacuse of transanctions and their problem with async/await

        //-----------
        //ONLY DB MODIFYING
        //-----------
        //non async option - because of transactions...
        void ModifyGroupNotificationExists(GroupNotificationType groupNotificationType, string groupName, string sender, string receiver, ModificationType modificationType, string serverAlias);

        Task ModifyGroupNotificationExistsAsync(GroupNotificationType groupNotificationType, string groupName, string sender, string receiver, ModificationType modificationType, string serverAlias);
        Task ChangeUserIsOnlineAsync(string user, UserStatus status);
        Task ModifyFriendshipNotificationExistsAsync(FriendshipNotificationType friendshipNotification, string sender, string receiver, ModificationType modificationType);
        void ModifyFriendshipNotificationExists(FriendshipNotificationType friendshipNotification, string sender, string receiver, ModificationType modificationType);
        Task ModifyFriendshipRelationAsync(string friend1, string friend2, ModificationType modificationType);
        void ModifyFriendshipRelation(string friend, string friend2, ModificationType modificationType);
        Task ModifyGroupExistsAsync(Group g, GroupState groupState);
        Task ModifyAnonymousUser(string userName, ModificationType modificationType);

    }
}
