﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace ChatAppChattingServer02
{
    class Program
    {
        static void Main(string[] args)
        {
            int port;
            IPAddress ipAddress;
            string wrongArgumentsMessage = "wrong arguments, [0] = ipadress, [1] = port, [2] = alias";
            if (args.Length != 3)
            {
                Console.WriteLine(wrongArgumentsMessage);
                return;
            }
            else
            {
                if (!Int32.TryParse(args[1], out port))
                {
                    Console.WriteLine(wrongArgumentsMessage);
                    return;
                }
                if (!IPAddress.TryParse(args[0], out ipAddress))
                {
                    Console.WriteLine(wrongArgumentsMessage);
                    return;
                }
            }

            ChatAppServer chatServer = new ChatAppServer(ipAddress, port, args[2]);
            chatServer.StartWorking();

        }
    }
}
