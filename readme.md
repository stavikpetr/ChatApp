# ChatApp

ChatApp is a desktop chatting application written in C# similiar to irc and icq. Similiarity with irc is in the concepts of different chatting servers and groups, similiarity with icq is in the concept of friends.

Aim of this applications is to improve my understating of following technologies or concepts: asynchronous programming, parallel programming, WPF, database communication, LINQ to SQL, database design. This project was developed as semester project for .NET platform related courses at Charles University in Prague.

Check out folder images for the result, database diagram and simple architecture image. More detailed documentation will be added later.
