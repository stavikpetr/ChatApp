﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace ChatAppCommon
{
    public static class CommonData
    {
        public static readonly int RegServer_ListeningPort = 8283;
        public static readonly string RegServer_IpAddress_String;
        public static readonly IPAddress RegServer_IpAdress;

        public static readonly ICollection<int> ChatServer1_ListeningPorts;
        public static readonly string ChatServer1_IpAddress_String;
        public static readonly IPAddress ChatServer1_IpAddress;

        public static readonly Dictionary<string, ICollection<int>> chatServerPortValues_String;
        public static readonly Dictionary<IPAddress, ICollection<int>> chatServerPortValues_IpAddress;

        static CommonData()
        {
            RegServer_IpAddress_String = IPAddress.Loopback.ToString();
            ChatServer1_IpAddress_String = IPAddress.Loopback.ToString();

            RegServer_IpAdress = IPAddress.Loopback;
            ChatServer1_IpAddress = IPAddress.Loopback;

            ChatServer1_ListeningPorts = new LinkedList<int>();
            ChatServer1_ListeningPorts.Add(6183);
            ChatServer1_ListeningPorts.Add(6283);


            chatServerPortValues_String = new Dictionary<string, ICollection<int>>();
            chatServerPortValues_IpAddress = new Dictionary<IPAddress, ICollection<int>>();
            
            chatServerPortValues_IpAddress.Add(ChatServer1_IpAddress, ChatServer1_ListeningPorts);
            chatServerPortValues_String.Add(ChatServer1_IpAddress_String, ChatServer1_ListeningPorts);

        }
    }

    public static class MessageConstants
    {
        public static readonly string SIGN = "SIGN";
        public static readonly string INIT = "INIT";
        public static readonly string BEGIN = "BEGIN";
        public static readonly string OVER = "OVER";
        public static readonly string OK = "OK";
        public static readonly string ERR = "ERR";
        public static readonly char DELIMITER = '|';

        public static readonly string SERVER = "SERVER";
        public static readonly string USER = "USER";
        public static readonly string GROUP = "GROUP";
        public static readonly string LEAVE = "LEAVE";
        public static readonly string ADMIN = "ADMIN";
        public static readonly string FRIEND = "FRIEND";
        public static readonly string FRIENDSHIP = "FRIENDSHIP";
        public static readonly string NOTIFICATION = "NOTIFICATION";
        public static readonly string COMMAND = "COMMAND";
        public static readonly string NEW = "NEW";
        public static readonly string SHOW = "SHOW";
        public static readonly string JOIN = "JOIN";
        public static readonly string MESSAGE = "MESSAGE";
        public static readonly string SEARCH = "SEARCH";
        public static readonly string SEARCHBYUSERNAME = "SEARCHBYUSERNAME";
        public static readonly string SEARCHBYEMAIL = "SEARCHBYEMAIL";
        public static readonly string CHANGEDSTATUS = "CHANGEDSTATUS";
        public static readonly string IN = "IN";
        public static readonly string ORDER = "ORDER";
        public static readonly string UNDERSTOOD = "UNDERSTOOD";
        public static readonly string INFO = "INFO";
        public static readonly string DISCONNECT = "DISCONNECT";
        public static readonly string REFRESH = "REFRESH";
        public static readonly string ALL = "ALL";
        public static readonly string DUMMY = "DUMMY";

        public static readonly string GROUPKICKORDER = GroupNotificationType.GROUPKICKORDER.ToString();
        public static readonly string GROUPKICKINFO = GroupNotificationType.GROUPKICKINFO.ToString();
        public static readonly string GROUPKICKINFOUNDERSTOOD = GroupNotificationType.GROUPKICKINFOUNDERSTOOD.ToString();

        public static readonly string FRIENDSHIPREQUESTORDER = FriendshipNotificationType.FRIENDSHIPREQUESTORDER.ToString();
        public static readonly string FRIENDSHIPREFUSEDORDER = FriendshipNotificationType.FRIENDSHIPREFUSEORDER.ToString();
        public static readonly string FRIENDSHIPACCEPTORDER = FriendshipNotificationType.FRIENDSHIPACCEPTORDER.ToString();
        public static readonly string FRIENDSHIENDEDORDER = FriendshipNotificationType.FRIENDSHIPENDORDER.ToString();

        public static readonly string FRIENDSHIPREQUESTINFO = FriendshipNotificationType.FRIENDSHIPREQUESTINFO.ToString();
        public static readonly string FRIENDSHIPREFUSEDINFO = FriendshipNotificationType.FRIENDSHIPREFUSEINFO.ToString();
        public static readonly string FRIENDSHIPACCEPTINFO = FriendshipNotificationType.FRIENDSHIPACCEPTINFO.ToString();
        public static readonly string FRIENDSHIENDEDINFO = FriendshipNotificationType.FRIENDSHIPENDINFO.ToString();

        public static readonly string FRIENDSHIPREFUSEDINFOUNDERSTOOD = FriendshipNotificationType.FRIENDSHIPREFUSEINFOUNDERSTOOD.ToString();
        public static readonly string FRIENDSHIPACCEPTINFOUNDERSTOOD = FriendshipNotificationType.FRIENDSHIPACCEPTINFOUNDERSTOOD.ToString();
        public static readonly string FRIENDSHIENDEDINFOUNDERSTOOD = FriendshipNotificationType.FRIENDSHIPENDINFOUNDERSTOOD.ToString();

    }

    public enum FriendshipNotificationType
    {
        FRIENDSHIPACCEPTINFO, FRIENDSHIPREFUSEINFO, FRIENDSHIPENDINFO,
        FRIENDSHIPREQUESTORDER, FRIENDSHIPREQUESTINFO,
        FRIENDSHIPACCEPTORDER, FRIENDSHIPREFUSEORDER, FRIENDSHIPENDORDER,
        FRIENDSHIPACCEPTINFOUNDERSTOOD, FRIENDSHIPREFUSEINFOUNDERSTOOD, FRIENDSHIPENDINFOUNDERSTOOD
    };

    public enum GroupNotificationType
    {
        GROUPKICKINFO, GROUPJOINREQUEST, GROUPKICKORDER, GROUPKICKINFOUNDERSTOOD
    };

    public enum GroupType { PUBLIC, PROTECTED, PRIVATE };
    public enum GroupState { CREATED, DELETED };
    public enum UserStatus { ONLINE, OFFLINE, LEFT, JOINED };
    public enum ModificationType { ADD, REMOVE };
}
