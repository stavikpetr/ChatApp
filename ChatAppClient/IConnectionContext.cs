﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace ChatAppClient
{
    public interface IConnectionContext
    {
        StreamReader input { set; get; }
        StreamWriter output { set; get; }
        Socket socket { set; get; }
        bool inputEnded { get; set; }
        bool outputEnded { get; set; }

    }
}
