﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for UserPanel.xaml
    /// </summary>
    public partial class UserPanel : UserControl
    {
        public string userName { get; set; }
        public string groupName { get; set; }
        //MainWindowChatting mainWindow;
        MainWindowChatting mainWindow;

        public void ChangeStatus(UserStatus userStatus)
        {
            if (userStatus == UserStatus.ONLINE)
            {
                UserName.Foreground = new SolidColorBrush(Colors.Green);

            }
            else if (userStatus == UserStatus.OFFLINE)
            {
                UserName.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        public void ChangeKickButtonIsVisible(bool isVisible)
        {
            if (isVisible)
            {
                KickIcon.Visibility = Visibility.Visible;
            }
            else
            {
                KickIcon.Visibility = Visibility.Hidden;
            }
        }

        public void ChangeAddAsFriendButtonIsVisible(bool isVisible)
        {
            if (isVisible)
            {
                AddAsFriendIcon.Visibility = Visibility.Visible;
            }
            else
            {
                AddAsFriendIcon.Visibility = Visibility.Hidden;
            }
        }

        public UserPanel(MainWindowChatting mainWindow, string userName, string groupName, UserStatus userStatus, bool kickButton, bool addAsFriendButton)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.userName = userName;
            this.groupName = groupName;
            if (userName.StartsWith("Anonymous"))
            {
                UserName.Text = userName.Substring(0, 9) + "_" + userName.Substring(9, 4);
            }
            else
                UserName.Text = userName;
            ChangeStatus(userStatus);

            if (kickButton)
            {
                KickIcon.Visibility = Visibility.Visible;
            }
            if (addAsFriendButton)
            {
                AddAsFriendIcon.Visibility = Visibility.Visible;
            }

            KickIcon.Click += (_, __) =>
            {
                mainWindow.KickUser_User(userName, groupName);
            };

            AddAsFriendIcon.Click += (_, __) =>
             {
                 mainWindow.AddUserAsFriend_User(userName, groupName);
             };
            
        }
    }
}
