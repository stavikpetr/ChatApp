﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for NewGroupWindow.xaml
    /// </summary>
    public partial class NewGroupWindow : Window
    {
        IUserInputHandler userInputHandler;

        public NewGroupWindow(IUserInputHandler userInputHandler)
        {
            this.userInputHandler = userInputHandler;
            InitializeComponent();
        }

        public void CreateGroupStatus(string message, bool success)
        {

            OperationStatus.Text = "Status: " + message;
            if (!success)
            {
                OperationStatus.Foreground = Brushes.Red;
                GroupNameBox.IsEnabled = true;
                DescriptionBox.IsEnabled = true;
                PublicRadioButton.IsEnabled = true;
                PrivateRadioButton.IsEnabled = true;
                CreateButton.IsEnabled = true;
            }
            else
            {
                //the create part has been already called, just close
                OperationStatus.Foreground = Brushes.Green;
                this.Close();
            }
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            if (GroupNameBox.Text.Contains(MessageConstants.DELIMITER) || DescriptionBox.Text.Contains(MessageConstants.DELIMITER))
            {
                OperationStatus.Text = "Status: fail, one of textboxes contains character '|'";
                OperationStatus.Foreground = Brushes.Red;
                return;
            }

            if (GroupNameBox.Text == "" || DescriptionBox.Text == "")
            {
                OperationStatus.Text = "Status: fail, one of textboxes is empty";
                OperationStatus.Foreground = Brushes.Red;
                return;
            }

            if (!(bool)PublicRadioButton.IsChecked && !(bool)PrivateRadioButton.IsChecked)
            {
                OperationStatus.Text = "Status: fail, select group type:";
                OperationStatus.Foreground = Brushes.Red;
                return;
            }
            GroupType g;
            if ((bool)PublicRadioButton.IsChecked)
                g = GroupType.PUBLIC;
            else
                g = GroupType.PRIVATE;
            userInputHandler.HandleGroupNew(GroupNameBox.Text, StaticData.myUserName, g, DescriptionBox.Text);
            GroupNameBox.IsEnabled = false;
            DescriptionBox.IsEnabled = false;
            PublicRadioButton.IsEnabled = false;
            PrivateRadioButton.IsEnabled = false;
            CreateButton.IsEnabled = false;
            OperationStatus.Text = "Status: Operation is pending, please wait for result";
            OperationStatus.Foreground = Brushes.Purple;


        }


    }
}
