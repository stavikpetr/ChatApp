﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;
namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for FriendChatPage.xaml
    /// </summary>
    public partial class FriendChatPage : UserControl, IChatPage
    {
        public string pageName { get; set; }
        MainWindowChatting mainWindow;
        //MainWindowChatting mainWindow;

        public void AddMessage(string sender, string message)
        {
            if (sender.StartsWith("Anonymous"))
                sender = sender.Substring(0, 9) + "_" + sender.Substring(9, 4);
            ChatTextBox.Text += Environment.NewLine;
            ChatTextBox.Text += StaticData.getCurrentTime() + sender + ": " + message;
            ChatTextBox.Text += Environment.NewLine;
            if (ChatWindowScroller.VerticalOffset == ChatWindowScroller.ScrollableHeight)
                ChatWindowScroller.ScrollToBottom();
        }

        public FriendChatPage(MainWindowChatting mainWindow, string chatPageName)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.pageName = chatPageName;

            SendButton.Click += (_, __) =>
            {
                NewMessageHandler();
            };
        }

        private void NewMessageHandler()
        {
            if (MessageBox.Text.Contains(MessageConstants.DELIMITER))
            {
                AddMessage("Server", "error, message can't contain character: " + MessageConstants.DELIMITER);
            }
            else
            {
                AddMessage(StaticData.myUserName, MessageBox.Text);
                mainWindow.NewMessage(pageName, StaticData.myUserName, MessageBox.Text);
                MessageBox.Text = "";
            }
        }

        private void MessageBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                NewMessageHandler();
            }
        }
    }
}
