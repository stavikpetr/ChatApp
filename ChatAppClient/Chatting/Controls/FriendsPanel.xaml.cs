﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for FriendsPanel.xaml
    /// </summary>
    public partial class FriendsPanel : UserControl
    {
        public string friendName { get; set; }
        // MainWindowChatting mainWindow;
        MainWindowChatting mainWindow;

        public void ChangeStatus(UserStatus userStatus)
        {
            if (userStatus == UserStatus.ONLINE)
            {
                FriendName.Foreground = new SolidColorBrush(Colors.Green);
                EnvelopeIcon.Visibility = Visibility.Visible;

            }
            else if (userStatus == UserStatus.OFFLINE)
            {
                FriendName.Foreground = new SolidColorBrush(Colors.Black);
                EnvelopeIcon.Visibility = Visibility.Hidden;
                NewMessageIcon.Visibility = Visibility.Collapsed;
            }
        }

        public void ChangeNewMessage(bool newMessage)
        {

            if (newMessage)
            {
                NewMessageIcon.Visibility = Visibility.Visible;
            }
            else
                NewMessageIcon.Visibility = Visibility.Collapsed;
        }

        public FriendsPanel(string friendName, UserStatus userStatus, MainWindowChatting mainWindow)
        {
            InitializeComponent();
            this.friendName = friendName;
            this.mainWindow = mainWindow;
            FriendName.Text = friendName;
            ChangeStatus(userStatus);
            RemoveFriendIcon.Click += (_, __) =>
            {
                mainWindow.RemoveFriend_User(friendName);
            };
            EnvelopeIcon.Click += (_, __) =>
            {
                mainWindow.ChangeChatPage_User(friendName);
            };

        }
    }
}
