﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for NotifyingNotificationPanel.xaml
    /// </summary>
    public partial class NotifyingNotificationPanel : UserControl, INotificationPanel
    {
        MainWindowChatting mainWindow;
        public string text { get; set; }

        public NotifyingNotificationPanel(MainWindowChatting mainWindow, string text)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.text = text;
            NotificationText.Text = text;
            OKButton.Click += (_, __) =>
            {
                mainWindow.NotifyingNotificationUnderstood_User(text);
            };
            
        }
    }
}
