﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for GroupsPanel.xaml
    /// </summary>
    public partial class GroupsPanel : UserControl
    {
        public string groupName { get; set; }
        public bool amIJoined { get; set; }
        //MainWindowChatting mainWindow;
        MainWindowChatting mainWindow;

        public void ChangeIAmJoined(bool amIJoined)
        {
            if (amIJoined)
            {
                EnvelopeIcon.Visibility = Visibility.Visible;
                LeaveButton.Visibility = Visibility.Visible;
                JoinIcon.Visibility = Visibility.Collapsed;
            }
            else
            {
                EnvelopeIcon.Visibility = Visibility.Hidden;
                LeaveButton.Visibility = Visibility.Collapsed;
                JoinIcon.Visibility = Visibility.Visible;
            }
        }

        public void ChangeNewMessage(bool newMessage)
        {

            if (newMessage)
            {
                NewMessageIcon.Visibility = Visibility.Visible;
            }
            else
                NewMessageIcon.Visibility = Visibility.Collapsed;
        }

        public GroupsPanel(string groupName, GroupType groupType, MainWindowChatting mainWindow, string description, bool joined)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.groupName = groupName;

            GroupName.Text = groupName;
            //if (groupType == GroupType.PRIVATE)
            //{
            //    ChangeIAmJoined(true);
            //}
            //else
            //{
            //    ChangeIAmJoined(false);
            //}
            ChangeIAmJoined(joined);

            EnvelopeIcon.Click += (_, __) =>
            {
                mainWindow.ChangeChatPage_User(groupName);
            };

            JoinIcon.Click += (_, __) =>
            {
                mainWindow.JoinGroup_User(groupName);
            };

            LeaveButton.Click += (_, __) =>
              {
                  mainWindow.LeaveGroup_User(groupName);
              };

            InfoButton.ToolTip = description;

          
        }
    }
}
