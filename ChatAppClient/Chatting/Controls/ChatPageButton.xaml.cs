﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for ChatPageButton.xaml
    /// </summary>
    public partial class ChatPageButton : UserControl
    {
        public string pageName { get; set; }
        MainWindowChatting mainWindow;

        public ChatPageButton(string pageName, MainWindowChatting mainWindow, bool isCloseable)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.pageName = pageName;

            if (!isCloseable)
            {
                ClosePageButton.IsEnabled = false;
            }

            ChatPageName.Text = pageName;
            ClosePageButton.Click += (_, e) =>
            {
                mainWindow.CloseChatPage_User(pageName);
                e.Handled = true; //prevents bubbling of event
            };
            ShowPageButton.Click += (_, __) =>
            {
                mainWindow.ChangeChatPage_User(pageName);
            };
           
        }
    }
}
