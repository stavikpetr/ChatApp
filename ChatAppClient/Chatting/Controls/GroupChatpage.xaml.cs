﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;
namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for GroupChatpage.xaml
    /// </summary>
    public partial class GroupChatpage : UserControl, IChatPage
    {
        public string pageName { get; set; } //essentially... groupname
        //MainWindowChatting mainWindow;
        MainWindowChatting mainWindow;
        public Dictionary<string, UserPanel> groupUsers { get; set; }

        public void AddGroupUserPanel(UserPanel userPanel)
        {
            if (!groupUsers.ContainsKey(userPanel.userName))
            {
                groupUsers.Add(userPanel.userName, userPanel);
                GroupUsersStackPanel.Children.Add(userPanel);
            }
        }

        public void RemoveGroupUsersPanel(string userName)
        {
            if (groupUsers.ContainsKey(userName))
            {
                var panel = groupUsers[userName];
                groupUsers.Remove(userName);
                GroupUsersStackPanel.Children.Remove(panel);
            }
        }
        //when going offline/online
        public void ChangeGroupUserStatus(string username, UserStatus userStatus)
        {

        }

        public void AddMessage(string sender, string message)
        {
            if (sender.StartsWith("Anonymous"))
                sender = sender.Substring(0, 9) + "_" + sender.Substring(9, 4);

            //if( scrollShit.VerticalOffset == scrollShit.ScrollableHeight)
            //scrollShit.ScrollToBottom();
            ChatTextBox.Text += Environment.NewLine;
            ChatTextBox.Text += StaticData.getCurrentTime() + sender + ": " + message;
            ChatTextBox.Text += Environment.NewLine;
            if (ChatWindowScroller.VerticalOffset == ChatWindowScroller.ScrollableHeight)
                ChatWindowScroller.ScrollToBottom();
        }

        private void NewMessageHandler()
        {

            if (MessageBox.Text.Contains(MessageConstants.DELIMITER))
            {
                AddMessage("Server", "error, message can't contain character: " + MessageConstants.DELIMITER);
            }
            else
            {
                AddMessage(StaticData.myUserName, MessageBox.Text);
                mainWindow.NewMessage(pageName, StaticData.myUserName, MessageBox.Text);
                MessageBox.Text = "";

            }
        }

        public GroupChatpage(MainWindowChatting mainWindow, string chatPageName)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.pageName = chatPageName;
            groupUsers = new Dictionary<string, UserPanel>();
            SendButton.Click += (_, __) =>
            {
                NewMessageHandler();
            };
        
        }

        private void MessageBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                NewMessageHandler();
            }
        }
    }
}
