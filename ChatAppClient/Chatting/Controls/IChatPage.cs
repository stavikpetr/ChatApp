﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient.Chatting.Controls
{
    interface IChatPage
    {
        string pageName { get; set; }
        void AddMessage(string sender, string message);
    }
}
