﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Interfaces;

namespace ChatAppClient.Chatting.Controls
{
    /// <summary>
    /// Interaction logic for ServerChatPage.xaml
    /// </summary>
    public partial class ServerChatPage : UserControl, IChatPage
    {
        //MainWindowChatting mainWindow;
        MainWindowChatting mainWindow;
        public string pageName { get; set; }

        public void AddMessage(string sender, string message)
        {
            if (sender.StartsWith("Anonymous"))
                sender = sender.Substring(0, 9) + "_" + sender.Substring(9, 4);
            ChatTextBox.Text += Environment.NewLine;
            ChatTextBox.Text += StaticData.getCurrentTime() + sender + ": " + message;
            ChatTextBox.Text += Environment.NewLine;
            if (ChatWindowScroller.VerticalOffset == ChatWindowScroller.ScrollableHeight)
                ChatWindowScroller.ScrollToBottom();
        }

        public ServerChatPage(string chatPageName,MainWindowChatting mainWindow)
        {
            InitializeComponent();
            this.pageName = chatPageName;
            this.mainWindow = mainWindow;
            AddMessage(StaticData.serverAlias, $"Hello !!, welcome to ChatApp server {StaticData.serverAlias}, to see all suported commands, write HELP");

            SendButton.Click += (_, __) =>
            {
                NewMessageHandler();
            };
           
        }

        private void NewMessageHandler()
        {
            AddMessage(StaticData.myUserName, MessageBox.Text);
            mainWindow.NewServerMessage_User(MessageBox.Text);
            MessageBox.Text = "";
        }

        private void MessageBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                NewMessageHandler();
            }
        }
    }
}
