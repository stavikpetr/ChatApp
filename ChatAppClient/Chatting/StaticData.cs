﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient.Chatting
{
    static class StaticData
    {
        public static string myUserName { get; set; }
        public static string serverAlias { get; set; }
        public static string userNameCut { get; set; }
        public static bool asAnonymous { get; set; } = false;

        public static string getCurrentTime()
        {
            return $"{DateTime.Now.ToString("H:mm")} ";
        }
    }
}
