﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient.Chatting
{
    class Logger
    {
        MainWindowChatting mainWindow;


        public void LogInput(string message)
        {
            mainWindow.Dispatcher.Invoke(new Action<string, bool>(mainWindow.GroupOperationResult_Server), "Input Message: " + message, false);
        }
        public void LogOutput(string message)
        {
            mainWindow.Dispatcher.Invoke(new Action<string, bool>(mainWindow.GroupOperationResult_Server), "Output Message: " + message + "\n", false);
        }

        public Logger(MainWindowChatting mainWindow)
        {
            this.mainWindow = mainWindow;
        }
    }
}
