﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ChatAppClient.Chatting.Controls;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting
{
    class SharedData
    {
        public HashSet<string> groupsWithMeAsAdmin { get; set; }
        public Dictionary<string, FriendsPanel> friendsPanels { get; set; }
        public Dictionary<string, GroupsPanel> serverGroupsPanels { get; set; }
        public Dictionary<string, GroupsPanel> myGroupsPanels { get; set; }
        public Dictionary<string, IChatPage> chatPages { get; set; }
        public Dictionary<string, ChatPageButton> chatPageButtons { get; set; }

        public Dictionary<string, INotificationPanel> notificationPanels { get; set; }
        public Dictionary<string, FriendshipNotification> friendshipNotifications { get; set; }
        public Dictionary<string, GroupNotification> groupNotifications { get; set; }

        public List<Command> serverCommands { get; set; }
        public Dictionary<string, Group> groups {get; set;}


        public string userName { get; set; }

        

        public ReaderWriterLockSlim rwGroupsLock { get; set; }
        public ReaderWriterLockSlim rwFriendsLock { get; set; }
        public ReaderWriterLockSlim rwFriendshipNotificationsLock { get; set; }
        public ReaderWriterLockSlim rwGroupNotificationsLock { get; set; }
        public ReaderWriterLockSlim rwSupportedCommandsLock { get; set; }

        public SharedData()
        {
            groups = new Dictionary<string, Group>();
            friendshipNotifications = new Dictionary<string, FriendshipNotification>();
            groupNotifications = new Dictionary<string, GroupNotification>();
           
            rwGroupNotificationsLock = new ReaderWriterLockSlim();
            rwGroupsLock = new ReaderWriterLockSlim();
            rwFriendsLock = new ReaderWriterLockSlim();
            rwFriendshipNotificationsLock = new ReaderWriterLockSlim();
            rwSupportedCommandsLock = new ReaderWriterLockSlim();

            groupsWithMeAsAdmin = new HashSet<string>();
            friendsPanels = new Dictionary<string, FriendsPanel>();
            serverGroupsPanels = new Dictionary<string, GroupsPanel>();
            myGroupsPanels = new Dictionary<string, GroupsPanel>();
            chatPages = new Dictionary<string, IChatPage>();
            chatPageButtons = new Dictionary<string, ChatPageButton>();
            notificationPanels = new Dictionary<string, INotificationPanel>();
            serverCommands = new List<Command>();
        }

    }

    public class FriendshipNotification
    {
        public FriendshipNotificationType friendshipNotificationType { get; set; }
        public string sender { get; set; }
        public string receiver { get; set; }
    }

    public class GroupNotification
    {
        public string alias { get; set; }
        public string groupName { get; set; }
       // public GroupType groupType { get; set; }
        public string sender { get; set; }
        public string receiver { get; set; }
        public GroupNotificationType groupNotificationType { get; set; }
    }

    class Group
    {
        public string admin { get; set; }
        public string description { get; set; }
        public GroupType groupType { get; set; }
        public string name { get; set; }
        //public Dictionary<string, User> users { get; set; }
    }

    class Friend
    {
        public string name { get; set; }
        public UserStatus userStatus { get; set; }
    }

    class User
    {
        public string name { get; set; }
        public UserStatus userStatus { get; set; }
    }

    class Command
    {
        public string description { get; set; }
        public string signature { get; set; }
    }
}
