﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Implementations
{
    class ServerInputHandler:IServerInputHandler
    {
        MainWindowChatting mainWindow;

        //INIT|BEGIN|userName
        public Task HandleInitBegin(string[] tokens)
        {
            return Task.Factory.StartNew(() =>
            {
                if (tokens[2].StartsWith("Anonymous"))
                {
                    StaticData.asAnonymous = true;
                    StaticData.userNameCut = tokens[2].Substring(0, 9) + "_" + tokens[2].Substring(9, 4);
                }
                StaticData.myUserName = tokens[2];
            });
        }
        //INIT|OVER
        public async Task HandleInitOver()
        {           
            await mainWindow.Dispatcher.BeginInvoke(new Action(mainWindow.InitOver_Server));
        }

        //--------------
        //-----
        //      FRIENDS RELATED
        //-----
        //--------------

        //FRIEND|userName|status
        public async Task HandleAddFriend(string[] tokens)
        {
            var status = (UserStatus)Enum.Parse(typeof(UserStatus), tokens[2].ToUpper());
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, UserStatus>(mainWindow.AddFriend_Server), tokens[1], status);

        }

        //FRIENDCHANGEDSTATUS|friendName|STATUS
        public async Task HandleFriendChangedStatus(string[] tokens)
        {
            var status = (UserStatus)Enum.Parse(typeof(UserStatus), tokens[2].ToUpper());
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, UserStatus>(mainWindow.FriendChangedStatus_Server), tokens[1], status);
        }

        //FRIENDSHIPREQUESTINFO|sender(friendshipRequester)|receiver (me)
        //FRIENDSHIPACCEPTINFO|(sender)userThatAccepted|friendshipRequester(me)
        //FRIENDHIPREFUSINFO|(sender)userThatRefused|friendshipRequeter(me)
        //FRIENSHIPENDINFO|(sender)userThatEnded|friendToNotify(me)
        public async Task HandleFriendshipNotificationInfo(string[] tokens, bool isInit)
        {
            var type = (FriendshipNotificationType)Enum.Parse(typeof(FriendshipNotificationType), tokens[0].ToUpper());
            string text = null;
            if (type == FriendshipNotificationType.FRIENDSHIPREQUESTINFO)
            {
                text = $"user {tokens[1]} would like to add you as friend";
                await mainWindow.Dispatcher.BeginInvoke(new Action<string,FriendshipNotificationType, string, string>(mainWindow.AddDecidingFriendshipNotification_Server),
                    text, type, tokens[1], tokens[2]);
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.ChangeVisibilityOfAddUserButton), tokens[1], false);
            }
            else
            {
                if (type == FriendshipNotificationType.FRIENDSHIPACCEPTINFO)
                {
                    text = $"user {tokens[1]} accepted your friendship request";
                    if (!isInit)
                    {
                        await mainWindow.Dispatcher.BeginInvoke(new Action<string, UserStatus>(mainWindow.AddFriend_Server), tokens[1], UserStatus.OFFLINE);
                    }               
                }
                else if (type == FriendshipNotificationType.FRIENDSHIPREFUSEINFO)
                {
                    text = $"user {tokens[1]} refused your friendship request";
                    if (!isInit)
                    {
                        await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.ChangeVisibilityOfAddUserButton), tokens[1], true);
                    }
                }
                else if (type == FriendshipNotificationType.FRIENDSHIPENDINFO)
                {
                    text = $"user {tokens[1]} ended your mutual friendship";
                    if (!isInit)
                    {
                        await mainWindow.Dispatcher.BeginInvoke(new Action<string>(mainWindow.RemoveFriend_Server), tokens[1]);
                    }
                }
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, FriendshipNotificationType, string, string>(mainWindow.AddNotifyingFriendshipNotification_Server),
                    text, type, tokens[1], tokens[2]);
            }
            //if request new friendshipDecidingNotification
            //else new friendshipNotifyingNotification

        }

        //unresolved infos...
        //FRIENDSHIPNOTIFICATION|type|sender|receiver
        public async Task HandleFriendshipNotification(string[] tokens, bool isInit) //case for init phase
        {
            await HandleFriendshipNotificationInfo(new string[] { tokens[1], tokens[2], tokens[3] }, isInit);
            //if request new friendshipDecidingNotification
            //else new friendshipNotifyingNotification
        }



        //--------------
        //-----
        //      GROUPS RELATED
        //-----
        //--------------
        //SERVERGROUP|groupName|groupType|admin|description
        public async Task HandleAddServerGroup(string[] tokens)
        {
            var groupType = (GroupType)Enum.Parse(typeof(GroupType), tokens[2].ToUpper());
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, GroupType, string, string>(mainWindow.AddServerGroup_Server),
                tokens[1], groupType, tokens[3], tokens[4]);
        }

        //USERGROUP|groupName|groupType|admin|description
        public async Task HandleAddUserGroup(string[] tokens)
        {
            var groupType = (GroupType)Enum.Parse(typeof(GroupType), tokens[2].ToUpper());
            if (groupType != GroupType.PRIVATE)
            {
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, GroupType, string, string>(mainWindow.AddServerGroup_Server),
                    tokens[1], groupType, tokens[3], tokens[4]);
            }
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, GroupType, string, string>(mainWindow.AddUserGroup_Server),
                tokens[1], groupType, tokens[3], tokens[4]);
        }

        //GROUPNOTIFICATION|alias|groupName|type|sender|receiver(me)
        public async Task HandleGroupNotification(string[] tokens, bool isInit)
        {
            await HandleGroupNotificationInfo(new string[] { tokens[3], tokens[2], tokens[4], tokens[5], tokens[1] }, isInit);
        }

        //GROUPKICKINFO|groupName|kicker(sender)|kickedUser(receiver)|serverAlias
        public async Task HandleGroupNotificationInfo(string[] tokens, bool isInit)
        {
            var type = (GroupNotificationType)Enum.Parse(typeof(GroupNotificationType), tokens[0].ToUpper());
            string text = null;
            if (type == GroupNotificationType.GROUPKICKINFO)
            {
                text = $"user {tokens[2]} just kicked you from group {tokens[1]} on server {tokens[4]}";
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, GroupNotificationType, string, string, string, string>(mainWindow.AddNotifyingGroupNotification_Server),
                    text, type, tokens[4], tokens[1], tokens[2], tokens[3]);
                if (!isInit)
                {
                    await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.RemoveUserGroup_Server),
                        tokens[1], true);
                }
            }
        }

        //GROUPNEWERR|customGroupCreateFailMessage
        //GROUPNEWOK|groupName|userName|type|description|customGroupCreatedSuccessfulyMessage
        public async Task HandleGroupNew(string[] tokens)
        {
            if (tokens[0] == MessageConstants.GROUP + MessageConstants.NEW + MessageConstants.OK)
            {
                await HandleAddUserGroup(tokens);
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.GroupNewResult_Server), tokens[5], true);
            }
            else if(tokens[0] == MessageConstants.GROUP + MessageConstants.NEW+MessageConstants.ERR)
            {
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.GroupNewResult_Server), tokens[1], false);
            }
        }

        //GROUPSHOWOK|groupName
        //GROUPSHOWERR|groupName|customGroupShowErrorMessage
        //GROUPSHOW|groupName|userName|userStatus
        public async Task HandleGroupShow(string[] tokens)
        {
            string text = null;
            if (tokens[0] == MessageConstants.GROUP + MessageConstants.SHOW + MessageConstants.OK)
            {
                text = $"group {tokens[1]} has been showed successfuly";
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.GroupOperationResult_Server), text, true); 
            }
            else if (tokens[0] == MessageConstants.GROUP + MessageConstants.SHOW + MessageConstants.ERR)
            {
                text = $"group {tokens[1]} not showed successfuly";
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.GroupOperationResult_Server), text, false);
            }
            else if (tokens[0] == MessageConstants.GROUP + MessageConstants.SHOW)
            {
                var userStatus = (UserStatus)Enum.Parse(typeof(UserStatus), tokens[3].ToUpper());
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, string, UserStatus>(mainWindow.AddGroupUser_Server),
                    tokens[1], tokens[2], userStatus);
            }
        }

        //GROUPJOINOK|groupName|customJoinSuccessfulMessage
        //GROUPJOINERR|groupName|customErrorMessage
        public async Task HandleGroupJoin(string[] tokens)
        {
            string text = null;
            if (tokens[0] == MessageConstants.GROUP + MessageConstants.JOIN + MessageConstants.OK)
            {
                text = $"group join to group {tokens[1]} was successful";
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.GroupOperationResult_Server), text, true);
            }
            else if (tokens[0] == MessageConstants.GROUP + MessageConstants.JOIN + MessageConstants.ERR)
            {
                text = $"group join to group {tokens[1]} was not successful";
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.GroupOperationResult_Server), text, false);

            }
        }

        //GROUPINSERVERCHANGEDSTATUS|groupName|status|groupType|admin|description   //if created...
        //GROUPINSERVERCHANGEDSTATUS|groupName|status  //if deleted
        public async Task HandleGroupChangedStatus(string[] tokens) //new group, group deleted...
        {
            var groupStatus = (GroupState)Enum.Parse(typeof(GroupState), tokens[2].ToUpper());
            
            if (groupStatus == GroupState.CREATED)
            {
                var groupType = (GroupType)Enum.Parse(typeof(GroupType), tokens[3].ToUpper());
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, GroupType, string, string>(mainWindow.AddServerGroup_Server),
                    tokens[1], groupType, tokens[4], tokens[5]);
            }
            else if (groupStatus == GroupState.DELETED)
            {
                await mainWindow.Dispatcher.BeginInvoke(new Action<string>(mainWindow.RemoveServerGroup_Server), tokens[1]);
            }
        }

        //USERINGROUPCHANGEDSTATUS|groupName|userName|status
        public async Task HandleUserInGroupChangedStatus(string[] tokens)
        {
            var userStatus = (UserStatus)Enum.Parse(typeof(UserStatus), tokens[3].ToUpper());
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, string, UserStatus>(mainWindow.GroupUserChangedStatus_Server),
                tokens[1], tokens[2], userStatus);
        }
    

        //--------------
        //-----
        //      OTHERS
        //-----
        //--------------
        //SERVERCOMMAND|commandSignature|commandDescription
        public async Task HandleAddServerCommand(string commandSignature, string commandDescription)
        {
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, string>(mainWindow.AddServerCommand_Server),
                commandSignature, commandDescription);
        }

        //SEARCHOK|customSearchOKMessage
        //SEARCHERR|customSearchErrorMessage
        public async Task HandleSearchResult(string[] tokens)
        {
            if (tokens[0] == MessageConstants.SEARCH + MessageConstants.OK)
            {
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.SearchResult_Server),
                    tokens[1], true);
            }
            else if (tokens[0] == MessageConstants.SEARCH + MessageConstants.ERR)
            {
                await mainWindow.Dispatcher.BeginInvoke(new Action<string, bool>(mainWindow.SearchResult_Server),
                    tokens[1], false);
            }
        }

        //MESSAGEGROUP|groupName|sender|message
        public async Task HandleGroupMessage(string[] tokens)
        {
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, string,string>(mainWindow.NewMessage),
                tokens[1], tokens[2], tokens[3]);
        }

        //MESSAGEFRIEND|receiver(me)|sender|message
        public async Task HandleFriendMessage(string[] tokens)
        {
            //target is chatpage that is under key of friendName...
            await mainWindow.Dispatcher.BeginInvoke(new Action<string, string, string>(mainWindow.NewMessage),
                tokens[2], tokens[2], tokens[3]);
        }

        public ServerInputHandler(MainWindowChatting mainWindow)
        {
            this.mainWindow = mainWindow;
        }

    }
}
