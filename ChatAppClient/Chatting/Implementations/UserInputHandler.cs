﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppClient.Chatting.Interfaces;
using ChatAppClient.Chatting.Controls;
using ChatAppCommon;
namespace ChatAppClient.Chatting.Implementations
{
    class UserInputHandler:IUserInputHandler
    {
        IMessageWriter messageWriter;

        //DISCONNECT|userName
        public Task HandleDisconnect()
        {
            return Task.Factory.StartNew(() =>
            {
                messageWriter.AddMessage(new Message { message = MessageConstants.DISCONNECT + MessageConstants.DELIMITER + StaticData.myUserName });
            });
        }

        //GROUPNEW|groupName|type|userName|description
        public Task HandleGroupNew(string groupName, string userName, GroupType groupType, string description)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(MessageConstants.GROUP);
                sb.Append(MessageConstants.NEW);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupName);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupType.ToString());
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(userName);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(description);
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        //GROUPSHOW|groupname
        public Task HandleGroupShow(string groupName)
        {
            return Task.Factory.StartNew(() =>
            {
                messageWriter.AddMessage(new Message { message = MessageConstants.GROUP + MessageConstants.SHOW + MessageConstants.DELIMITER + groupName });
            });
        }

        //GROUPJOIN|groupName|groupType|userName|admin
        public Task HandleGroupJoin(string groupName, GroupType groupType, string userName, string admin)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(MessageConstants.GROUP + MessageConstants.JOIN);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupName);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupType.ToString());
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(userName);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(admin);
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        //GROUPLEAVE|groupName|userName
        //GROUPLEAVEADMIN|groupName|userName
        public Task HandleGroupLeave(string groupName, string userName, bool asAdmin)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                //jen rozskok podle bool isAdmin
                if (asAdmin)
                {
                    sb.Append(MessageConstants.GROUP + MessageConstants.LEAVE+MessageConstants.ADMIN);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(groupName);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(userName);
                }
                else
                {
                    sb.Append(MessageConstants.GROUP + MessageConstants.LEAVE);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(groupName);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(userName);
                }
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        //GROUPKICKORDER|groupName|kicker|kickedUser|serverAlias
        //GROUPKICKINFOUNDERSTOOD|groupName|kicker|kickedUser|serverAlias
        public Task HandleGroupNotification(GroupNotification groupNotification)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(groupNotification.groupNotificationType.ToString());
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupNotification.groupName);
                sb.Append(MessageConstants.DELIMITER);

                switch (groupNotification.groupNotificationType)
                {
                    case GroupNotificationType.GROUPKICKINFOUNDERSTOOD:
                        sb.Append(groupNotification.receiver);
                        sb.Append(MessageConstants.DELIMITER);
                        sb.Append(groupNotification.sender);
                        break;
                    case GroupNotificationType.GROUPKICKORDER:
                        sb.Append(groupNotification.sender);
                        sb.Append(MessageConstants.DELIMITER);
                        sb.Append(groupNotification.receiver);
                        break;
                }

                sb.Append(MessageConstants.DELIMITER);
                sb.Append(groupNotification.alias);
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        //FRIENDSHIPREQUESTORDER|sender(friendshipRequester)|receiver
        //FRIENDSHIPREFUSEORDER|userThatRefusedFriendship|friendshipRequester
        //FRINEDSHIPACCEPTORDER|userThatAcceptedFriendship|friendshipRequester
        //FRINEDSHIPENDORDER|userThatEndedFriendship|otherUser

        //FRIENDSHIPACCEPTINFOUNDERSTOOD|userThatPerformedAccept|userThatUnderstands
        //FRIENDSHIPREFUSEINFOUNDERSTOOD|userThatPerformedRefuse|userThatUnderstands
        //FRIENDSHIPENDINFOUNDERSTOOD|userThatPerformedEnd|userThatUnderstands
        public Task HandleFriendshipNotification(FriendshipNotification friendshipNotification)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(friendshipNotification.friendshipNotificationType.ToString());
                sb.Append(MessageConstants.DELIMITER);
                if (friendshipNotification.friendshipNotificationType.ToString().EndsWith(MessageConstants.UNDERSTOOD))
                {
                    //these are understoods, i want to send the receiver first
                    sb.Append(friendshipNotification.receiver);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(friendshipNotification.sender);
                }
                else if (friendshipNotification.friendshipNotificationType.ToString().EndsWith(MessageConstants.ORDER))
                {
                    //these are orders, first sender then receiver
                    sb.Append(friendshipNotification.sender);
                    sb.Append(MessageConstants.DELIMITER);
                    sb.Append(friendshipNotification.receiver);
                }
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        //just direct command
        public Task HandleSendDirectCommand(string message)
        {
            return Task.Factory.StartNew(() =>
            {
                messageWriter.AddMessage(new Message { message = message });
            });
        }

        //MESSAGEFRIEND|receiver|sender|message
        public Task HandleFriendMessage(string receiver, string sender, string message)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(MessageConstants.MESSAGE + MessageConstants.FRIEND);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(receiver);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(sender);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(message);
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        //MESSAGEGROUP|receiver(groupName)|sender|message
        public Task HandleGroupMessage(string receiver, string sender, string message)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(MessageConstants.MESSAGE + MessageConstants.GROUP);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(receiver);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(sender);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(message);
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }

        public Task HandleSearchByName(string searchedValue)
        {
            return Task.Factory.StartNew(() =>
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(MessageConstants.SEARCHBYUSERNAME);
                sb.Append(MessageConstants.DELIMITER);
                sb.Append(searchedValue);
                messageWriter.AddMessage(new Message { message = sb.ToString() });
            });
        }


        public UserInputHandler( IMessageWriter messageWriter)
        {
            this.messageWriter = messageWriter;
        }
    }
}
