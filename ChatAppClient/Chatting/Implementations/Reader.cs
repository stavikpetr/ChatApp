﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Implementations
{
    class Reader:IReader
    {
        IConnectionContext connectionContext;
        IServerInputParser serverInputParser;
        MainWindowChatting mainWindow;
        bool terminationSignal = false;

        public async void StartReading()
        {
            //initialization phase (viz communication protocol)
            string message;
            string initOver = MessageConstants.INIT + MessageConstants.DELIMITER + MessageConstants.OVER;
            Queue<string> debt = new Queue<string>();
            Task task;
            List<Task> tasksToAwait = new List<Task>();

            //this is INIT|BEGIN|UserName string

            message = await connectionContext.input.ReadLineAsync();
            task = serverInputParser.ParseInputAndForward(message);
            tasksToAwait.Add(task);

            while ((message = await connectionContext.input.ReadLineAsync()) != initOver)
            {
                if (!message.StartsWith(MessageConstants.INIT))
                {
                    //i don't want the init part of string here
                    debt.Enqueue(message.Substring(5,message.Length-5));
                }
                else
                {
                    task = serverInputParser.ParseInputAndForward(message);
                    tasksToAwait.Add(task);
                }                
            }

            //let's just send the initOver message to parser so he can call showing of gui
            //this is INIT|OVER string...

            await Task.WhenAll(tasksToAwait);
            //now let's wait for the init|over string to be handled
            await serverInputParser.ParseInputAndForward(message);

            //now, first, handle messages that interupted the init stream

            while (debt.Count != 0)
            {
                message = debt.Dequeue();
                task = serverInputParser.ParseInputAndForward(message);
                tasksToAwait.Add(task);
            }
            //again, i want to be sure that these tasks are done...
            //i když, asi ne...
            await Task.WhenAll(tasksToAwait);

            //now again, start receiving messages from server
            try
            {
                while (!terminationSignal)
                {
                    message = await connectionContext.input.ReadLineAsync();
                    serverInputParser.ParseInputAndForward(message); //no point of awaiting, the reader job is done, it can move to another message
                }
                //the reader is terminated, let's close this

            }
            catch (Exception e)
            {
                mainWindow.Dispatcher.BeginInvoke(new Action(mainWindow.HandleUnexpectedEnd));
            }
            connectionContext.inputEnded = true;
        }

        public void InitTermination()
        {
            terminationSignal = true;
        }

        public Reader(IConnectionContext connectionContext, IServerInputParser serverInputParser, MainWindowChatting mainWindow)
        {
            this.mainWindow = mainWindow;
            this.connectionContext = connectionContext;
            this.serverInputParser = serverInputParser;

        }
    }
}
