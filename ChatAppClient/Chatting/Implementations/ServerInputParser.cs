﻿//#define LOG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppClient.Chatting.Interfaces;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Implementations
{
    class ServerInputParser : IServerInputParser
    {
        IServerInputHandler serverInputHandler;
        Logger logger;

        public async Task ParseInputAndForward(string line)
        {
            string[] tokens = line.Split(MessageConstants.DELIMITER);
            string[] tokensPrep = line.Split(MessageConstants.DELIMITER);
            if (line.StartsWith(MessageConstants.INIT))
            {
                if (tokensPrep[1] == MessageConstants.BEGIN || tokensPrep[1] == MessageConstants.OVER || tokensPrep[1] == MessageConstants.GROUP + MessageConstants.NOTIFICATION
                    || tokensPrep[1] == MessageConstants.FRIENDSHIP+MessageConstants.NOTIFICATION)
                    tokens = tokensPrep;
                else
                    tokens = line.Remove(0, 5).Split(MessageConstants.DELIMITER);
            }
#if LOG
            if (line != "DUMMY")
                logger.LogInput(line);
#endif
            if (tokens[0].StartsWith(MessageConstants.INIT))
            {
                //either init|Begin or init|over
                if (tokens[1] == MessageConstants.BEGIN)
                {
                    await serverInputHandler.HandleInitBegin(tokens);
                }
                else if (tokens[1] == MessageConstants.OVER)
                {
                    await serverInputHandler.HandleInitOver();
                }
                else if (tokens[1] == MessageConstants.GROUP + MessageConstants.NOTIFICATION)
                {
                    tokens = line.Remove(0, 5).Split(MessageConstants.DELIMITER);
                    await serverInputHandler.HandleGroupNotification(tokens, true);
                }
                else if (tokens[1] == MessageConstants.FRIENDSHIP + MessageConstants.NOTIFICATION)
                {
                    tokens = line.Remove(0, 5).Split(MessageConstants.DELIMITER);
                    await serverInputHandler.HandleFriendshipNotification(tokens, true);
                }
            }
            else if (tokens[0].StartsWith(MessageConstants.FRIEND))
            {
                //information about friends - either new friend, or changed status
                if (tokens[0] == MessageConstants.FRIEND)
                {
                    await serverInputHandler.HandleAddFriend(tokens);
                }
                else if (tokens[0] == MessageConstants.FRIEND + MessageConstants.CHANGEDSTATUS)
                {
                    await serverInputHandler.HandleFriendChangedStatus(tokens);
                }
                else if (tokens[0].StartsWith(MessageConstants.FRIENDSHIP))
                {
                    //var type = (FriendshipNotificationType)Enum.Parse(typeof(FriendshipNotificationType), tokens[0].ToUpper());

                    if (tokens[0].EndsWith(MessageConstants.ORDER))
                    {
                        //shouldn't be needed
                    }
                    else if (tokens[0].EndsWith(MessageConstants.INFO))
                    {
                        await serverInputHandler.HandleFriendshipNotificationInfo(tokens, false);
                    }
                    //else if (tokens[0].EndsWith(MessageConstants.UNDERSTOOD))
                    //{

                    //}
                    //case in init phase, sending unresolved notifications to user

                }
            }
            //endswith case is fro USERGROUP, SERVERGROUP in init phase
            else if (tokens[0].StartsWith(MessageConstants.GROUP) || tokens[0]==MessageConstants.USER + MessageConstants.GROUP || tokens[0] == MessageConstants.SERVER + MessageConstants.GROUP)
            {
                if (tokens[0] == MessageConstants.SERVER + MessageConstants.GROUP)
                {
                    await serverInputHandler.HandleAddServerGroup(tokens);
                }
                else if (tokens[0] == MessageConstants.USER + MessageConstants.GROUP)
                {
                    await serverInputHandler.HandleAddUserGroup(tokens);
                }
                //GROUP...INFO  - maybe like GROUPKICKINFO
                else if (tokens[0].EndsWith(MessageConstants.INFO))
                {
                    await serverInputHandler.HandleGroupNotificationInfo(tokens, false);
                }
                //else if (tokens[0].StartsWith(MessageConstants.GROUP + MessageConstants.NOTIFICATION))
                //{
                //    //only one message - groupkickinfo can come from the server
                   
                       
                    
                //}
                else if (tokens[0].StartsWith(MessageConstants.GROUP + MessageConstants.NEW))
                {
                    await serverInputHandler.HandleGroupNew(tokens);
                }
                else if (tokens[0].StartsWith(MessageConstants.GROUP + MessageConstants.SHOW))
                {
                    await serverInputHandler.HandleGroupShow(tokens);
                }
                else if (tokens[0].StartsWith(MessageConstants.GROUP + MessageConstants.JOIN))
                {
                    await serverInputHandler.HandleGroupJoin(tokens);
                }
                else if (tokens[0] == MessageConstants.GROUP + MessageConstants.IN + MessageConstants.SERVER + MessageConstants.CHANGEDSTATUS)
                {
                    await serverInputHandler.HandleGroupChangedStatus(tokens);
                }
            }
            else if (tokens[0].StartsWith(MessageConstants.SERVER))
            {
                if (tokens[0] == MessageConstants.SERVER + MessageConstants.COMMAND)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 1; i < tokens.Length - 2; i++)
                    {
                        sb.Append(tokens[i]);
                        sb.Append(MessageConstants.DELIMITER);
                    }
                    sb.Append(tokens[tokens.Length - 2]);
                    await serverInputHandler.HandleAddServerCommand(sb.ToString(), tokens[tokens.Length - 1]);
                }

            }
            else if (tokens[0] == MessageConstants.USER + MessageConstants.IN + MessageConstants.GROUP+MessageConstants.CHANGEDSTATUS)
            {
                await serverInputHandler.HandleUserInGroupChangedStatus(tokens);
            }
            else if (tokens[0].StartsWith(MessageConstants.SEARCH))
            {
                await serverInputHandler.HandleSearchResult(tokens);
                //server can reply either search ok or serach err
                //dát si pozor na to abych si nemohl přidat usera, kterýho už mám v přátelích
            }
            else if (tokens[0].StartsWith(MessageConstants.MESSAGE))
            {
                if (tokens[0] == MessageConstants.MESSAGE + MessageConstants.GROUP)
                {
                    await serverInputHandler.HandleGroupMessage(tokens);
                }
                else if (tokens[0] == MessageConstants.MESSAGE + MessageConstants.FRIEND)
                {
                    await serverInputHandler.HandleFriendMessage(tokens);
                }
            }
            else if (tokens[0] == MessageConstants.DUMMY)
            {
                //nothing, it is just to refresh reading loop...
            }
            
        }

        public ServerInputParser(IServerInputHandler serverInputHandler, Logger logger)
        {
            this.logger = logger;
            this.serverInputHandler = serverInputHandler;
        }
    }
}
