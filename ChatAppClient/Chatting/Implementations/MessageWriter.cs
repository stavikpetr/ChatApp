﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Threading;
using ChatAppClient.Chatting.Interfaces;
using System.IO;
using ChatAppCommon;

namespace ChatAppClient.Chatting.Implementations
{
    class MessageWriter : IMessageWriter
    {
        IConnectionContext connectionContext;
        BlockingCollection<Message> messages;
        MainWindowChatting mainWindow;
        Logger logger;
        bool terminationSignal = false;

        public async void StartWriting()
        {
            while (true)
            {
                Message m = messages.Take();
               // logger.LogOutput(m.message);
                try
                {
                    await connectionContext.output.WriteLineAsync(m.message);
                    await connectionContext.output.FlushAsync();
                    if (terminationSignal)
                    {
                        //the disconnect message has just been sent, we can terminate
                        if (m.message.StartsWith(MessageConstants.DISCONNECT))
                        {
                            break;
                        }
                    }
                }
                catch (IOException e)
                {
                    mainWindow.Dispatcher.BeginInvoke(new Action(mainWindow.HandleUnexpectedEnd));
                    break;
                }
            }

            //signal that the output ended
            connectionContext.outputEnded = true;
        }

        public void InitTermination()
        {
            terminationSignal = true;
        }

        public void AddMessage(Message m)
        {
            messages.Add(m);
        }

        public MessageWriter(IConnectionContext connectionContext, Logger logger, MainWindowChatting mainWindow)
        {
            this.connectionContext = connectionContext;
            this.logger = logger;
            this.mainWindow = mainWindow;
            messages = new BlockingCollection<Message>();
        }
    }
}
