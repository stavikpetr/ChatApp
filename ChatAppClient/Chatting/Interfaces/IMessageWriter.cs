﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient.Chatting.Interfaces
{
    interface IMessageWriter
    {
        void StartWriting();
        void AddMessage(Message m);
        void InitTermination();
    }
}
