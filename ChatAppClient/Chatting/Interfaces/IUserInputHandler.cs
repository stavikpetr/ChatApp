﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatAppCommon;
namespace ChatAppClient.Chatting.Interfaces
{
    public interface IUserInputHandler
    {
        Task HandleDisconnect();

        //grouprelated
        Task HandleGroupNew(string groupName, string userName, GroupType groupType, string description);
        Task HandleGroupShow(string groupName);
        Task HandleGroupJoin(string groupName, GroupType groupType, string userName, string admin);
        Task HandleGroupLeave(string groupName, string userName, bool asAdmin);
        
        Task HandleSendDirectCommand(string message);
        
        Task HandleGroupNotification(GroupNotification groupNotification);
        Task HandleFriendshipNotification(FriendshipNotification frinedshipNotification);

        //can be used either for friendMessages or for groupMessages, the one that changes is receiver - either groupName or friendName
        Task HandleFriendMessage(string receiver, string sender, string message);
        Task HandleGroupMessage(string receiver, string sender, string message);

        Task HandleSearchByName(string searchedValue);
        
    }
}
