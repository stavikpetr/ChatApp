﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient.Chatting.Interfaces
{
    interface IServerInputHandler
    {
        //--------
        //INIT PHASE
        //--------
        Task HandleInitOver();
        Task HandleInitBegin(string[] tokens);


        //friends related
        Task HandleAddFriend(string[] tokens);
        Task HandleFriendChangedStatus(string[] tokens);
        //Task HandleFriendshipNotificationOrder(string[] tokens);
        Task HandleFriendshipNotificationInfo(string[] tokens, bool isInit);
        //Task HandleFriendshipInfoUnderstood(string[] tokens);
        Task HandleFriendshipNotification(string[] tokens, bool isInit); //case for init phase


        //groups related
        Task HandleAddServerGroup(string[] tokens);
        Task HandleAddUserGroup(string[] tokens);
        Task HandleGroupNotification(string[] tokens, bool isInit);
        Task HandleGroupNotificationInfo(string[] tokens, bool isInit);
        Task HandleGroupNew(string[] tokens);
        Task HandleGroupShow(string[] tokens);
        Task HandleGroupJoin(string[] tokens);
        Task HandleGroupChangedStatus(string[] tokens); //new group, group deleted...
        Task HandleUserInGroupChangedStatus(string[] tokens);

        //server related
        Task HandleAddServerCommand(string commandSignature, string commandDescription);

        Task HandleSearchResult(string[] tokens);

        Task HandleGroupMessage(string[] tokens);
        Task HandleFriendMessage(string[] tokens);

    }
}
