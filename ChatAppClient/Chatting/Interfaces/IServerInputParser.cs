﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient.Chatting.Interfaces
{
    interface IServerInputParser
    {
        Task ParseInputAndForward(string line);
    }
}
