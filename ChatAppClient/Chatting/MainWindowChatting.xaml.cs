﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ChatAppClient.Chatting.Implementations;
using ChatAppClient.Chatting.Interfaces;
using ChatAppClient.Chatting.Controls;
using System.Threading;
using ChatAppCommon;
using System.ComponentModel;

namespace ChatAppClient.Chatting
{
    /// <summary>
    /// Interaction logic for MainWindowChatting.xaml
    /// </summary>
    public partial class MainWindowChatting : Window
    {
        SharedData sharedData;

        IChatPage activeChatPage;
        ChatPageButton activeButton;
        string activeChatPageName;

        IUserInputHandler userInputHandler;
        IConnectionContext connectionContext;

        NewGroupWindow newGroupWindow;

        IMessageWriter writer;
        IReader reader;
        bool endCalled = false;

        public MainWindowChatting(IConnectionContext connectionContext, string serverAlias)
        {
            this.Hide();
            StaticData.serverAlias = serverAlias;            
            InitializeComponent();
            Initialize(connectionContext);
        }

        private void Initialize(IConnectionContext connectionContext)
        {
            this.connectionContext = connectionContext;

            //přidání nové page pro server, její zaktivnění
            sharedData = new SharedData();
            ServerChatPage scp = new ServerChatPage(StaticData.serverAlias, this);
            ChatPageButton cpb = new ChatPageButton(StaticData.serverAlias, this, false);
            activeChatPage = scp;
            activeButton = cpb;
            activeChatPageName = StaticData.serverAlias;
            ChatPagesStackPanel.Children.Add(cpb);
            ChatWindowFrame.Content = scp;
            sharedData.chatPages.Add(StaticData.serverAlias, scp);
            sharedData.chatPageButtons.Add(StaticData.serverAlias, cpb);
            ActivateButton(true, cpb);
            AddButton.IsEnabled = false;
            Logger logger = new Logger(this);

            //initialize reading
            IServerInputHandler serverInputHandler = new ServerInputHandler(this);
            IServerInputParser serverInputParser = new ServerInputParser(serverInputHandler, logger);
            reader = new Reader(connectionContext, serverInputParser, this);

            Thread t1 = new Thread(() => reader.StartReading());
            t1.Start();
           
            //initialize writing
            writer = new MessageWriter(connectionContext, logger, this);
           
            userInputHandler = new UserInputHandler(writer);
            Thread t2 = new Thread(() => writer.StartWriting());
            t2.Start();
            //tady pak bude asi taky spuštění timeru na update group a podobně každých x minut....

            Closing += new CancelEventHandler(MainWindow_Closing);

        }



        //---------------------------------------
        //-----------
        //           SERVER
        //-----------
        //--------------------------------------

        public void InitOver_Server()
        {
            if (StaticData.asAnonymous)
            {
                UserNameLabel.Content = StaticData.userNameCut;
                NewGroupButton.IsEnabled = false;                
            }
            else
                UserNameLabel.Content = StaticData.myUserName;
            this.Show();
        }

        public void AddFriend_Server(string userName, UserStatus userStatus)
        {
            if (!sharedData.friendsPanels.ContainsKey(userName))
            {
                FriendsPanel fp = new FriendsPanel(userName, userStatus, this);
                sharedData.friendsPanels.Add(userName, fp);
                FriendsStackPanel.Children.Add(fp);

                ChangeVisibilityOfAddUserButton(userName, false);

                //actually best way is to add chat page and chat button, this way, i can have messages from users without the window open
                //this should always result to true
                if (!sharedData.chatPages.ContainsKey(userName) && !sharedData.chatPageButtons.ContainsKey(userName))
                {
                    sharedData.chatPages.Add(userName, new FriendChatPage(this, userName));
                    sharedData.chatPageButtons.Add(userName, new ChatPageButton(userName, this, true));
                }
                
            }
        }

        public void ChangeVisibilityOfAddUserButton(string userName, bool visible)
        {
            if (SearchBox.Text == userName)
                AddButton.IsEnabled = false;
            foreach (var chatPagePair in sharedData.chatPages)
            {
                if (chatPagePair.Value.GetType() == typeof(GroupChatpage))
                {
                    var groupChatPage = (GroupChatpage)chatPagePair.Value;
                    if (groupChatPage.groupUsers.ContainsKey(userName))
                    {
                        var panel = groupChatPage.groupUsers[userName];
                        panel.ChangeAddAsFriendButtonIsVisible(visible);
                    }
                }
            }
        }

        public void RemoveFriend_Server(string userName)
        {
            if (sharedData.friendsPanels.ContainsKey(userName))
            {
                var fp = sharedData.friendsPanels[userName];
                FriendsStackPanel.Children.Remove(fp);
                sharedData.friendsPanels.Remove(userName);
            }

            //this removes only chatPageButton
            CloseChatPage_User(userName);

            //lets remove the chatPage and chatpagebutton itself
            if (sharedData.chatPages.ContainsKey(userName))
            {
                sharedData.chatPages.Remove(userName);
            }
            if (sharedData.chatPageButtons.ContainsKey(userName))
            {
                sharedData.chatPageButtons.Remove(userName);
            }
            

            ChangeVisibilityOfAddUserButton(userName, true);
        }

        public void FriendChangedStatus_Server(string userName, UserStatus userStatus)
        {
            if (sharedData.friendsPanels.ContainsKey(userName))
            {
                sharedData.friendsPanels[userName].ChangeStatus(userStatus);

            }
        }

        public void AddServerGroup_Server(string groupName, GroupType groupType, string admin, string description)
        {
            if (!sharedData.groups.ContainsKey(groupName))
            {
                sharedData.groups.Add(groupName, new Group { admin = admin, description = description, groupType = groupType, name = groupName });
            }
            if (groupType == GroupType.PRIVATE)
                return;
            if (!sharedData.serverGroupsPanels.ContainsKey(groupName))
            {
                GroupsPanel gp = new GroupsPanel(groupName, groupType, this, description, false);
                sharedData.serverGroupsPanels.Add(groupName, gp);
                ServerGroupsStackPanel.Children.Add(gp);

                if (sharedData.myGroupsPanels.ContainsKey(groupName))
                    gp.ChangeIAmJoined(true);

                
            }
        }

        public void AddUserGroup_Server(string groupName, GroupType groupType, string admin, string description)
        {
            if (!sharedData.myGroupsPanels.ContainsKey(groupName))
            {
                GroupsPanel gp = new GroupsPanel(groupName, groupType, this, description, true);
                if (StaticData.myUserName == admin)
                {
                    if (!sharedData.groupsWithMeAsAdmin.Contains(groupName))
                        sharedData.groupsWithMeAsAdmin.Add(groupName);
                }
                sharedData.myGroupsPanels.Add(groupName, gp);
                MyGroupsStackPanel.Children.Add(gp);

                //actually best way is to add chat page and chat button, this way, i can have messages from users without the window open
                //this should always result to true...
                if (!sharedData.chatPages.ContainsKey(groupName) && !sharedData.chatPageButtons.ContainsKey(groupName))
                {
                    sharedData.chatPages.Add(groupName, new GroupChatpage(this, groupName));
                    sharedData.chatPageButtons.Add(groupName, new ChatPageButton(groupName, this, true));
                    userInputHandler.HandleGroupShow(groupName);
                }

                if (sharedData.serverGroupsPanels.ContainsKey(groupName))
                {
                    sharedData.serverGroupsPanels[groupName].ChangeIAmJoined(true);
                }

                if (!sharedData.groups.ContainsKey(groupName))
                {
                    sharedData.groups.Add(groupName, new Group { admin = admin, description = description, groupType = groupType, name = groupName });
                }
            }
        }

        public void AddGroupUser_Server(string groupName, string userName, UserStatus userStatus)
        {
            bool kickButton = false;
            bool addAsFriendButton = true;
            if (userStatus == UserStatus.JOINED)
                userStatus = UserStatus.ONLINE;
            if (sharedData.groupsWithMeAsAdmin.Contains(groupName) && userName!=StaticData.myUserName)
            {
                kickButton = true;
            }
            if (sharedData.friendsPanels.ContainsKey(userName) || userName==StaticData.myUserName)
            {   
                addAsFriendButton = false;
            }
            if (StaticData.asAnonymous)
            {
                addAsFriendButton = false;
            }
            if (userName.StartsWith("Anonymous"))
            {
                addAsFriendButton = false;
            }
            //if there is pending friendship request notification from the other user...
            foreach (var fn in sharedData.friendshipNotifications)
            {
                if (fn.Value.friendshipNotificationType == FriendshipNotificationType.FRIENDSHIPREQUESTINFO)
                {
                    if (fn.Value.sender == userName)
                        addAsFriendButton = false;
                }
            }
            if (sharedData.chatPages.ContainsKey(groupName))
            {
                UserPanel usp = new UserPanel(this, userName, groupName, userStatus, kickButton, addAsFriendButton);
                var groupChatPage = (GroupChatpage)sharedData.chatPages[groupName];
                groupChatPage.AddGroupUserPanel(usp);
            }
        }


        public void RemoveServerGroup_Server(string groupName)
        {
            //odstranění ze server groups
            //odstranění ze servergroupsStackPanelu
            //pokud jsem nebyl členem této skupiny tak je tovšechno, nicméně, pokud jsem byl, tak musím ještě zavolat
            //metodu removeusergroup_server
            if (sharedData.serverGroupsPanels.ContainsKey(groupName))
            {
                var groupsPanel = sharedData.serverGroupsPanels[groupName];
                ServerGroupsStackPanel.Children.Remove(groupsPanel);
                sharedData.serverGroupsPanels.Remove(groupName); 
            }
            if (sharedData.groups.ContainsKey(groupName))
                sharedData.groups.Remove(groupName);
            RemoveUserGroup_Server(groupName, false);

        }

        public void RemoveUserGroup_Server(string groupName, bool kicked)
        {
            if (sharedData.myGroupsPanels.ContainsKey(groupName))
            {
                var groupsPanel = sharedData.myGroupsPanels[groupName];
                MyGroupsStackPanel.Children.Remove(groupsPanel);
                sharedData.myGroupsPanels.Remove(groupName);

                CloseChatPage_User(groupName);

                if (sharedData.chatPages.ContainsKey(groupName))
                {
                    sharedData.chatPages.Remove(groupName);
                }
                if (sharedData.chatPageButtons.ContainsKey(groupName))
                {
                    sharedData.chatPageButtons.Remove(groupName);
                }

                if (!kicked)
                    RemoveServerGroup_Server(groupName);
                else
                {
                    //it i was kicked, enable join to that group and disable envelope icon
                    if (sharedData.serverGroupsPanels.ContainsKey(groupName))
                    {
                        sharedData.serverGroupsPanels[groupName].ChangeIAmJoined(false);
                    }
                }
            }
        }
        

        public void GroupOperationResult_Server(string message, bool result)
        {
            //different colours implied by result are not available now

            //if the operation was group join
            if (message.StartsWith("group join"))
            {
                if (!message.EndsWith("not successful"))
                {
                    string groupName = message.Split(' ')[4];

                    if (sharedData.groups.ContainsKey(groupName))
                    {
                        AddUserGroup_Server(groupName, sharedData.groups[groupName].groupType, sharedData.groups[groupName].admin, sharedData.groups[groupName].description); 
                    }
                }
            }
            
            sharedData.chatPages[StaticData.serverAlias].AddMessage(StaticData.serverAlias, message);
        }

        public void GroupNewResult_Server(string message, bool success)
        {
            if (newGroupWindow.IsLoaded)
            {
                newGroupWindow.CreateGroupStatus(message, success);
            }
        }

        public void GroupUserChangedStatus_Server(string groupName, string userName, UserStatus userStatus)
        {
            if (userStatus == UserStatus.ONLINE || userStatus == UserStatus.OFFLINE)
            {
                if (sharedData.chatPages.ContainsKey(groupName))
                {
                    var chatPage = (GroupChatpage)sharedData.chatPages[groupName];
                    if (chatPage.groupUsers.ContainsKey(userName))
                    {
                        chatPage.groupUsers[userName].ChangeStatus(userStatus);
                    }
                }
            }
            else if (userStatus == UserStatus.LEFT)
            {
                if (sharedData.chatPages.ContainsKey(groupName))
                {
                    var chatPage = (GroupChatpage)sharedData.chatPages[groupName];
                    if (chatPage.groupUsers.ContainsKey(userName))
                    {
                        var groupUserPanel = chatPage.groupUsers[userName];
                        chatPage.GroupUsersStackPanel.Children.Remove(groupUserPanel);
                        chatPage.groupUsers.Remove(userName);
                    }
                }
            }
            else if (userStatus == UserStatus.JOINED)
            {
                AddGroupUser_Server(groupName, userName, userStatus);
            }
        }

        public void SearchResult_Server(string message, bool result)
        {
            SearchStatusLabel.Text = $"status: {message}";
            if (result)
            {
                SearchStatusLabel.Foreground = Brushes.Green;
                if (SearchBox.Text != StaticData.myUserName && !sharedData.friendsPanels.ContainsKey(SearchBox.Text))
                {
                    foreach (var fn in sharedData.friendshipNotifications)
                    {
                        if (fn.Value.friendshipNotificationType == FriendshipNotificationType.FRIENDSHIPREQUESTINFO)
                        {
                            if (fn.Value.sender == SearchBox.Text)
                                return;
                        }
                    }
                    if(!StaticData.asAnonymous)
                        AddButton.IsEnabled = true;
                }
            }
            else
            {
                SearchStatusLabel.Foreground = Brushes.Red;
                AddButton.IsEnabled = false;
            }
        }

        public void AddServerCommand_Server(string commandSignature, string description)
        {
            sharedData.serverCommands.Add(new Command { description = description, signature = commandSignature });
        }

        public void AddDecidingFriendshipNotification_Server(string text, FriendshipNotificationType friendshipNotificationType, string sender, string receiver)
        {
            if (!sharedData.notificationPanels.ContainsKey(text) && !sharedData.friendshipNotifications.ContainsKey(text))
            {
                FriendshipNotification fn = new FriendshipNotification { friendshipNotificationType = friendshipNotificationType, receiver = receiver, sender = sender };
                DecidingNotificationPanel dnp = new DecidingNotificationPanel(this, text);

                sharedData.friendshipNotifications.Add(text, fn);
                sharedData.notificationPanels.Add(text, dnp);
                NotificationsStackPanel.Children.Add(dnp);
            }
        }

        public void AddNotifyingFriendshipNotification_Server(string text, FriendshipNotificationType friendshipNotificationType, string sender, string receiver)
        {
            if (!sharedData.notificationPanels.ContainsKey(text) && !sharedData.friendshipNotifications.ContainsKey(text))
            {
                FriendshipNotification fn = new FriendshipNotification { friendshipNotificationType = friendshipNotificationType, receiver = receiver, sender = sender };
                NotifyingNotificationPanel nnp = new NotifyingNotificationPanel(this, text);

                sharedData.friendshipNotifications.Add(text, fn);
                sharedData.notificationPanels.Add(text, nnp);
                NotificationsStackPanel.Children.Add(nnp);
            }
        }

        public void AddNotifyingGroupNotification_Server(string text, GroupNotificationType groupNotificationType, string alias, string groupName,  string sender, string receiver)
        {
            if (!sharedData.notificationPanels.ContainsKey(text) && !sharedData.groupNotifications.ContainsKey(text))
            {
                GroupNotification gn = new GroupNotification { alias = alias, groupName = groupName, receiver = receiver, sender = sender, groupNotificationType = groupNotificationType };
                NotifyingNotificationPanel nnp = new NotifyingNotificationPanel(this, text);

                sharedData.groupNotifications.Add(text, gn);
                sharedData.notificationPanels.Add(text, nnp);
                NotificationsStackPanel.Children.Add(nnp);
            }
        }
        
 
        //---------------------------------------
        //-----------
        //           UNIFIED
        //-----------
        //--------------------------------------

        public void NewMessage(string target, string sender, string message)
        {
            if (StaticData.myUserName == sender)
            {
                //i've sent the message, it is already displayed in my chat, need to send info about this message to other users
                if (sharedData.friendsPanels.ContainsKey(target))
                {
                    userInputHandler.HandleFriendMessage(target, sender, message);
                }
                else
                {
                    userInputHandler.HandleGroupMessage(target, sender, message);
                }
            }
            else
            {
                //this should be always true
                if (sharedData.chatPages.ContainsKey(target))
                {
                    //higlight the name of group / friend
                    if (!(activeChatPageName == target))
                    {
                        if (sharedData.friendsPanels.ContainsKey(target))
                        {
                            sharedData.friendsPanels[target].ChangeNewMessage(true);
                        }
                        else
                        {
                            if (sharedData.myGroupsPanels.ContainsKey(target))
                            {
                                sharedData.myGroupsPanels[target].ChangeNewMessage(true);
                            }
                            if (sharedData.serverGroupsPanels.ContainsKey(target))
                            {
                                sharedData.serverGroupsPanels[target].ChangeNewMessage(true);
                            }
                        }
                    }
                    sharedData.chatPages[target].AddMessage(sender, message);

                }
            }
        }

        //---------------------------------------
        //-----------
        //           USER
        //-----------
        //--------------------------------------

        public void KickUser_User(string userName, string groupName)
        {
            if (sharedData.chatPages.ContainsKey(groupName))
            {
                var chatPage = (GroupChatpage)sharedData.chatPages[groupName];
                chatPage.RemoveGroupUsersPanel(userName);
                userInputHandler.HandleGroupNotification(new GroupNotification
                { alias = StaticData.serverAlias, groupName = groupName, groupNotificationType= GroupNotificationType.GROUPKICKORDER, receiver = userName, sender = StaticData.myUserName });
            }
        }

        public void AddUserAsFriend_User(string userName, string groupName)
        {
            ChangeVisibilityOfAddUserButton(userName, false);
            userInputHandler.HandleFriendshipNotification(new FriendshipNotification { friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPREQUESTORDER, sender = StaticData.myUserName, receiver = userName });
        }

        public void JoinGroup_User(string groupName)
        {
            if (sharedData.groups.ContainsKey(groupName))
            {
                userInputHandler.HandleGroupJoin(groupName, sharedData.groups[groupName].groupType, StaticData.myUserName, sharedData.groups[groupName].admin);
            }
        }

        public void LeaveGroup_User(string groupName)
        {
            if (sharedData.groupsWithMeAsAdmin.Contains(groupName))
            {
                RemoveServerGroup_Server(groupName);
                userInputHandler.HandleGroupLeave(groupName, StaticData.myUserName, true);
            }
            else
            {
                CloseChatPage_User(groupName);
                if (sharedData.chatPages.ContainsKey(groupName) && sharedData.chatPageButtons.ContainsKey(groupName))
                {
                    var chatPageButton = sharedData.chatPageButtons[groupName];
                    sharedData.chatPages.Remove(groupName);
                    sharedData.chatPageButtons.Remove(groupName);
                    ChatPagesStackPanel.Children.Remove(chatPageButton);

                    if (sharedData.myGroupsPanels.ContainsKey(groupName))
                    {
                        var myGroupPanel = sharedData.myGroupsPanels[groupName];
                        sharedData.myGroupsPanels.Remove(groupName);
                        MyGroupsStackPanel.Children.Remove(myGroupPanel);

                    }
                    if (sharedData.serverGroupsPanels.ContainsKey(groupName))
                    {
                        sharedData.serverGroupsPanels[groupName].ChangeIAmJoined(false);
                    }
                }
                userInputHandler.HandleGroupLeave(groupName, StaticData.myUserName, false);
            }
        }

        public void RemoveFriend_User(string friendName)
        {
            RemoveFriend_Server(friendName);
            userInputHandler.HandleFriendshipNotification(new FriendshipNotification { friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPENDORDER, receiver = friendName, sender = StaticData.myUserName });
        }

        //...UNDERSTOOD|userThatPerformed..|userThatUnderstands
        public void NotifyingNotificationUnderstood_User(string text)
        {
            if (sharedData.friendshipNotifications.ContainsKey(text))
            {
                var notification = sharedData.friendshipNotifications[text];
                var newNotification = new FriendshipNotification { sender = StaticData.myUserName, receiver = notification.sender };
                //TADY JEN POZOR NA TO, POHLÍDAT SI , ŽE JÁ TO CHCI VE SKUTEČNOSTI POSLAT NAOPAK
                switch (notification.friendshipNotificationType)
                {
                    case FriendshipNotificationType.FRIENDSHIPACCEPTINFO:
                        newNotification.friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPACCEPTINFOUNDERSTOOD;
                        break;
                    case FriendshipNotificationType.FRIENDSHIPENDINFO:
                        newNotification.friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPENDINFOUNDERSTOOD;
                        break;
                    case FriendshipNotificationType.FRIENDSHIPREFUSEINFO:
                        newNotification.friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPREFUSEINFOUNDERSTOOD;
                        break;
                }
                var notificationPanel = (NotifyingNotificationPanel) sharedData.notificationPanels[text];
                NotificationsStackPanel.Children.Remove(notificationPanel);
                sharedData.notificationPanels.Remove(text);
                sharedData.friendshipNotifications.Remove(text);


                userInputHandler.HandleFriendshipNotification(newNotification);
                //odstranit notifikace
            }
            else if (sharedData.groupNotifications.ContainsKey(text))
            {
                var notification = sharedData.groupNotifications[text];
                var newNotification = new GroupNotification
                {
                    alias = StaticData.serverAlias,
                    groupName = notification.groupName,
                    sender = StaticData.myUserName,
                    receiver = notification.sender
                };
                switch (notification.groupNotificationType)
                {
                    case GroupNotificationType.GROUPKICKINFO:
                        newNotification.groupNotificationType = GroupNotificationType.GROUPKICKINFOUNDERSTOOD;
                        break;
                }
                var notificationPanel = (NotifyingNotificationPanel)sharedData.notificationPanels[text];
                NotificationsStackPanel.Children.Remove(notificationPanel);
                sharedData.notificationPanels.Remove(text);
                sharedData.groupNotifications.Remove(text);

                userInputHandler.HandleGroupNotification(newNotification);

            }
        }

        public void DecidingNotificationAccept_User(string text)
        {
            if (sharedData.friendshipNotifications.ContainsKey(text))
            {
                var notification = sharedData.friendshipNotifications[text];
                var newNotification = new FriendshipNotification {receiver=notification.sender, sender = StaticData.myUserName };
                switch (notification.friendshipNotificationType)
                {
                    case FriendshipNotificationType.FRIENDSHIPREQUESTINFO:
                        newNotification.friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPACCEPTORDER;
                        //lets just add panel representing that friend, let's add him as offline, server will send info about his status soon
                        AddFriend_Server(notification.sender, UserStatus.OFFLINE);
                        break;
                }
                var notificationsPanel = (DecidingNotificationPanel)sharedData.notificationPanels[text];
                NotificationsStackPanel.Children.Remove(notificationsPanel);
                sharedData.notificationPanels.Remove(text);
                sharedData.friendshipNotifications.Remove(text);

                userInputHandler.HandleFriendshipNotification(newNotification);
            }
            else
            {
                //nothing, at this time, there are only friendship deciding notifications
            }
        }

        public void DecidingNotificationRefuse_User(string text)
        {
            if (sharedData.friendshipNotifications.ContainsKey(text))
            {
                var notification = sharedData.friendshipNotifications[text];
                var newNotification = new FriendshipNotification { receiver = notification.sender, sender = StaticData.myUserName };
                switch (notification.friendshipNotificationType)
                {
                    case FriendshipNotificationType.FRIENDSHIPREQUESTINFO:
                        newNotification.friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPREFUSEORDER;
                        break;
                }
                var notificationsPanel = (DecidingNotificationPanel)sharedData.notificationPanels[text];
                NotificationsStackPanel.Children.Remove(notificationsPanel);
                sharedData.notificationPanels.Remove(text);
                sharedData.friendshipNotifications.Remove(text);
                ChangeVisibilityOfAddUserButton(notification.sender, true);
                userInputHandler.HandleFriendshipNotification(newNotification);
            }
        }

        public void NewServerMessage_User(string message)
        {
            if (message == "HELP")
            {
                foreach (var cmd in sharedData.serverCommands)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Description: ");
                    sb.Append(cmd.description);
                    sb.Append("\n");
                    sb.Append("Signature: ");
                    sb.Append(cmd.signature);
                    sb.Append("\n");
                    NewMessage(StaticData.serverAlias, StaticData.serverAlias, sb.ToString());
                }
            }
            else
            {
                if (MatchesCommandSignature(message))
                {
                    userInputHandler.HandleSendDirectCommand(message);
                }
                else
                    NewMessage(StaticData.serverAlias, StaticData.serverAlias, "unsupported command");
            }
        }

        private bool MatchesCommandSignature(string command)
        {
            string[] tokens = command.Split(MessageConstants.DELIMITER);
            foreach (var cmd in sharedData.serverCommands)
            {
                string[] tokensToMatch = cmd.signature.Split(MessageConstants.DELIMITER);
                if (tokens.Length == tokensToMatch.Length)
                {
                    if (tokens[0] == tokensToMatch[0])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void ActivateButton(bool activate, ChatPageButton button)
        {
            if (activate)
            {
                var sp = (StackPanel)button.Content;
                var innerButton = (Button)sp.Children[0];
                innerButton.Foreground = Brushes.LightGray;
                innerButton.Background = Brushes.ForestGreen;
            }
            else
            {
                var sp = (StackPanel)button.Content;
                var innerButton = (Button)sp.Children[0];
                innerButton.Foreground = Brushes.Black;
                innerButton.Background = Brushes.Gray;
            }
        }

        public void CloseChatPage_User(string pageName)
        {
            //ja ve skutečnosti nechci nutně odstraňovat i chatpage
            //odstranění chatpage budu dělat ve vyjímečných případech - delete od frienda a co já vím co eště
            

            //tato metody prostě neodstraňuje nakonec ani chatpageButton, je na mě si v changeChatPage pohlídat
            if (sharedData.chatPageButtons.ContainsKey(pageName))
            {
                if (activeChatPageName == pageName)
                {
                    var pageButton = sharedData.chatPageButtons[pageName];
                    ActivateButton(false, pageButton);
                    var index = ChatPagesStackPanel.Children.IndexOf(pageButton);
                    ChatPagesStackPanel.Children.Remove(pageButton);
                    var nowActive = (ChatPageButton)ChatPagesStackPanel.Children[index - 1];
                    ChatWindowFrame.Content = sharedData.chatPages[nowActive.pageName];
                    activeChatPage = sharedData.chatPages[nowActive.pageName];
                    activeChatPageName = nowActive.pageName;
                    ActivateButton(true, nowActive);
                    activeButton = nowActive;
                }
                else
                {
                    var pageButton = sharedData.chatPageButtons[pageName];
                    ChatPagesStackPanel.Children.Remove(pageButton);
                }
         
            }
        }

        public void ChangeChatPage_User(string pageName)
        {
            if (activeChatPageName == pageName)
                return;
            //should be always true
            if (sharedData.chatPages.ContainsKey(pageName))
            {
                var pageButton = sharedData.chatPageButtons[pageName];
                ActivateButton(false, activeButton);
                if (!ChatPagesStackPanel.Children.Contains(pageButton))
                {
                    ChatPagesStackPanel.Children.Add(pageButton);
                }
                ActivateButton(true, pageButton);
                activeButton = pageButton;
                activeChatPage = sharedData.chatPages[pageName];
                activeChatPageName = pageName;
                ChatWindowFrame.Content = activeChatPage;

                if (sharedData.friendsPanels.ContainsKey(pageName))
                {
                    sharedData.friendsPanels[pageName].ChangeNewMessage(false);
                }
                else
                {
                    if (sharedData.myGroupsPanels.ContainsKey(pageName))
                        sharedData.myGroupsPanels[pageName].ChangeNewMessage(false);
                    if (sharedData.serverGroupsPanels.ContainsKey(pageName))
                        sharedData.serverGroupsPanels[pageName].ChangeNewMessage(false);
                }
                //také, změn new message color příslušného panelu...
            }
        }

        private void PeroformCleanShutDown()
        {
            //close connection aswell
            reader.InitTermination();
            writer.InitTermination();
            userInputHandler.HandleDisconnect().Wait();

            while (connectionContext.inputEnded != true || connectionContext.outputEnded != true)
            {
            }

            connectionContext.input.Close();
            connectionContext.output.Close();
            connectionContext.socket.Close();

            //finally, every thread finished, we can close window
            if (newGroupWindow != null && newGroupWindow.IsLoaded)
                newGroupWindow.Close();
            
        }

        public void HandleUnexpectedEnd()
        {
            if (!endCalled)
            {
                endCalled = true;
                MessageBox.Show("Sorry, but unexpected event happened at sever, application will now be closed");
                if (newGroupWindow != null && newGroupWindow.IsLoaded)
                    newGroupWindow.Close();
                this.Close();
            }
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            PeroformCleanShutDown();
            StaticData.asAnonymous = false;
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        
        private void NewGroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (newGroupWindow == null || !newGroupWindow.IsLoaded)
            {
                NewGroupWindow ngw = new NewGroupWindow(userInputHandler);
                newGroupWindow = ngw;
                ngw.Show();
            }
            else
                newGroupWindow.Activate();         
        }

        public void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            PeroformCleanShutDown();
        }


        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            AddButton.IsEnabled = false;
            SearchStatusLabel.Text = "Status:";
            SearchStatusLabel.Foreground = Brushes.Black;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            userInputHandler.HandleSearchByName(SearchBox.Text);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            ChangeVisibilityOfAddUserButton(SearchBox.Text, false);
            userInputHandler.HandleFriendshipNotification(new FriendshipNotification { friendshipNotificationType = FriendshipNotificationType.FRIENDSHIPREQUESTORDER, sender = StaticData.myUserName, receiver = SearchBox.Text });

        }
    }
}
