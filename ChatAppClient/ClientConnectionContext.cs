﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace ChatAppClient
{
    public class ClientConnectionContext : IConnectionContext
    {
        public Socket socket { get; set; }
        public StreamReader input { get; set; }
        public StreamWriter output { get; set; }
        public bool inputEnded { get; set; } = false;
        public bool outputEnded { get; set; } = false;
      
    }
}
