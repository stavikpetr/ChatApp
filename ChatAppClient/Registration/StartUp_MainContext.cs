﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ChatAppClient
{
    public class StartUp_MainContext
    {
        enum ConnectionType { REGSERVER, CHATSERVER };

        MainWindow mainWindow;
        Regex mailRegex;
        MessageReporter messageReporter;

        ClientConnectionContext registraionServerConnection;
        ClientConnectionContext chattingServerConnection;
        Action<string, bool> ButtonClickResult;
        Action<string, IConnectionContext,string> JoinButtonClickResult;


        public void CreateAccountAsync(string name, string pass, string passAgain, string mail)
        {
            ButtonClickResult = new Action<string, bool>(mainWindow.mainPage.regPage.CreateAccountOperationResult);

            Task.Factory.StartNew(async () =>
            {
                if (CheckValidityOfParameters(name, pass, passAgain, mail))
                {
                    bool connectionResult = await ConnectionResult(ConnectionType.REGSERVER, ChatAppCommon.CommonData.RegServer_IpAdress, ChatAppCommon.CommonData.RegServer_ListeningPort);
                    if (!connectionResult)
                    {
                        mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.CantOpenConnection(), false);
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("REG ");
                        sb.Append(name + " ");
                        sb.Append(pass + " ");
                        sb.Append(mail);
                        try
                        {
                            await registraionServerConnection.output.WriteLineAsync(sb.ToString());
                            await registraionServerConnection.output.FlushAsync();
                            string message = await registraionServerConnection.input.ReadLineAsync();
                            if (message.StartsWith("ERR"))
                            {
                                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, message.Substring(3, message.Length - 3), false);
                            }
                            else
                            {
                                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, message.Substring(3, message.Length - 3), true);
                            }
                        }
                        catch (IOException)
                        {
                            mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.ConnectionToRegServerFailed(), false);
                        }
                    }
                }

            }).ConfigureAwait(false);
        }

        public void TryConnectAsAnonAsync(string ipAddress, int port)
        {
            JoinButtonClickResult = new Action<string, IConnectionContext,string>(mainWindow.mainPage.JoinAsAnonOperationResult);
            IPAddress address = IPAddress.Parse(ipAddress);
            Task.Factory.StartNew(async() =>
            {
                bool connectionResult = await ConnectionResult(ConnectionType.CHATSERVER, address, port);
                if (!connectionResult)
                {
                    mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "sorry, can't connect to chatting server", null, null);
                }
                else
                {
                    try
                    {
                        await chattingServerConnection.output.WriteLineAsync("SIGN|ANON");
                        await chattingServerConnection.output.FlushAsync();
                        string response = await chattingServerConnection.input.ReadLineAsync();
                        if (response.StartsWith("SIGNOK"))
                        {
                            //SIGNOK|serverAlias
                            string[] tokens = response.Split('|');
                            CloseRegServerConnection();
                            mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "join successful, fetching data", chattingServerConnection, tokens[1]);
                        }
                        else
                        {
                            mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "sorry, connection error", null, null);
                        }
                    }
                    catch (IOException)
                    {
                        mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "sorry, can't connect to chatting server", null, null);
                    }
                }
            }).ConfigureAwait(false);
        }

        public void TryConnectAsUserAsync(string userName, string password, string ipAddress, int port)
        {
            JoinButtonClickResult = new Action<string, IConnectionContext, string>(mainWindow.mainPage.JoinAsUserOperationResult);
            IPAddress address = IPAddress.Parse(ipAddress);
            Task.Factory.StartNew(async () =>
            {
                bool connectionResult = await ConnectionResult(ConnectionType.CHATSERVER, address, port);
                if (!connectionResult)
                {
                    mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "sorry, can't connect to chatting server", null, null);
                }
                else
                {
                    try
                    {
                        await chattingServerConnection.output.WriteLineAsync("SIGN|LOGIN"+"|"+userName+"|"+password);
                        await chattingServerConnection.output.FlushAsync();
                        string response = await chattingServerConnection.input.ReadLineAsync();
                        if (response.StartsWith("SIGNOK"))
                        {
                            string[] tokens = response.Split('|');
                            CloseRegServerConnection();
                            mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "join successful, fetching data", chattingServerConnection, tokens[1]);
                        }
                        else
                        {
                            mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "sorry, connection error", null, null);
                        }
                    }
                    catch (IOException)
                    {
                        mainWindow.mainPage.Dispatcher.BeginInvoke(JoinButtonClickResult, "sorry, can't connect to chatting server", null, null);
                    }
                }
            }).ConfigureAwait(false);

        }

        private void CloseRegServerConnection()
        {
            if (registraionServerConnection.socket != null)
            {
                registraionServerConnection.output.WriteLineAsync("END").ContinueWith(_ =>
                {
                    registraionServerConnection.output.FlushAsync().ContinueWith(__ =>
                    {
                        registraionServerConnection.output.Close();
                        registraionServerConnection.input.Close();
                        registraionServerConnection.socket.Close();
                    });
                });
            }
        }

        public void ValidateKeyAsync(string key)
        {
            ButtonClickResult = new Action<string, bool>(mainWindow.mainPage.valPage.ValidateKeyOperationResult);

            Task.Factory.StartNew(async () =>
            {
                if (key == "")
                    mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.FieldEmpty(), false);
                else
                {
                    bool connectionResult = await ConnectionResult(ConnectionType.REGSERVER, ChatAppCommon.CommonData.RegServer_IpAdress, ChatAppCommon.CommonData.RegServer_ListeningPort);
                    if (!connectionResult)
                    {
                        mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.CantOpenConnection(), false);
                    }
                    else
                    {
                        try
                        {
                            await registraionServerConnection.output.WriteLineAsync("VAL " + key);
                            await registraionServerConnection.output.FlushAsync();
                            string message = await registraionServerConnection.input.ReadLineAsync();
                            if (message.StartsWith("ERR"))
                            {
                                mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, message.Substring(3, message.Length - 3), false);
                            }
                            else
                            {
                                mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, message.Substring(3, message.Length - 3), true);
                            }
                        }
                        catch (IOException)
                        {
                            mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.ConnectionToRegServerFailed(), false);
                        }
                    }
                }
            }).ConfigureAwait(false);
        }

        private async Task<bool> ConnectionResult(ConnectionType connectionType, IPAddress ipAddress, int port)
        {
            if (connectionType == ConnectionType.REGSERVER)
            {
                if (registraionServerConnection.socket != null) 
                if (registraionServerConnection.socket.Connected)
                    return true;
            }
            else if (connectionType == ConnectionType.CHATSERVER)
            {
                if(chattingServerConnection.socket!= null)
                if (chattingServerConnection.socket.Connected)
                    return true;
            }
            try
            {
                Socket s = await ConnectAsync(ipAddress, port);
                if (s == null)
                    return false;
                ClientConnectionContext c = new ClientConnectionContext();
                c.socket = s;
                NetworkStream ns = new NetworkStream(s);
                c.input = new StreamReader(ns);
                c.output = new StreamWriter(ns);
                if (connectionType == ConnectionType.CHATSERVER)
                {
                    chattingServerConnection = c;
                    return true;
                }
                else if (connectionType == ConnectionType.REGSERVER)
                {
                    registraionServerConnection = c;
                    return true;
                }
            }
            catch (SocketException)
            {
                return false;
            }
            return false;
        }

        private Task<Socket> ConnectAsync(IPAddress ipAdress, int port)
        {

            TaskCompletionSource<Socket> tcs = new TaskCompletionSource<Socket>();

            IPEndPoint EP = new IPEndPoint(ipAdress, port);
            Socket socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            
            SocketAsyncEventArgs args = new SocketAsyncEventArgs();
            
            args.RemoteEndPoint = EP;
            args.Completed += new EventHandler<SocketAsyncEventArgs>((_, e) =>
            {
                tcs.SetResult(args.ConnectSocket);
            });

            bool willRaiseEvent = socket.ConnectAsync(args);
            if (!willRaiseEvent)
            {
                tcs.SetResult(args.ConnectSocket);
            }

            return tcs.Task;
        }

        //true if mail format is correct
        private bool CheckMail(string mail)
        {
            if (mailRegex.IsMatch(mail))
                return true;
            return false;
        }

        private bool CheckSamePasswords(string pass, string passAgain)
        {
            if (pass != passAgain)
                return false;
            return true;
        }

        private bool CheckValidityOfParameters(string name, string pass, string passAgain, string mail)
        {
            if (name == "" || pass == "" || passAgain == "" || mail == "")
            {
                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.FieldEmpty(), false);
                return false;
            }
            else if (!CheckMail(mail))
            {
                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.NotValidMailString(), false);
                return false;
            }
            else if (!CheckSamePasswords(pass, passAgain))
            {
                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, messageReporter.PasswordsAreDifferentError(), false);
                return false;
            }
            return true;
        }

        public StartUp_MainContext(MainWindow mainWindow)
        {
            mailRegex = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            this.mainWindow = mainWindow;
            registraionServerConnection= new ClientConnectionContext();
            chattingServerConnection = new ClientConnectionContext();
            messageReporter = MessageReporter.messageReporter;
        }
    }

    #region AsContinueWith

    /*
     *   private Task<bool> ConnectIfNeeded()
        {
            if (clientConnectionContext.alreadyConnected)
                return Task.FromResult<bool>(true);
            else
            {
                TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
                try
                {
                    ConnectAsync().ContinueWith((t1) =>
                    {
                        clientConnectionContext.socket = t1.Result;
                        NetworkStream ns = new NetworkStream(t1.Result);
                        clientConnectionContext.input = new StreamReader(ns);
                        clientConnectionContext.output = new StreamWriter(ns);
                        clientConnectionContext.alreadyConnected = true;
                        tcs.SetResult(true);
                    });
                }
                catch (SocketException e)
                {
                    tcs.SetResult(false);
                }
                return tcs.Task;
            }
        }
     * */

    /*
     *    public void CreateAccountAsync(string name, string pass, string passAgain, string mail)
        {
            ButtonClickResult = new Action<string, bool>(mainWindow.mainPage.regPage.CreateAccountOperationResult);
            try
            {
                Task.Factory.StartNew(() =>
                {
                    string result = null;
                    if (name == "" || pass == "" || passAgain == "" || mail == "")
                    {
                        result = messageReporter.FieldEmpty();
                        mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, result, false);
                    }
                    else if (!CheckMail(mail))
                    {
                        result = messageReporter.NotValidMailString();
                        mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, result, false);
                    }
                    else if (!CheckSamePasswords(pass, passAgain))
                    {
                        result = messageReporter.PasswordsAreDifferentError();
                        mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, result, false);
                    }
                    else
                    {
                        ConnectIfNeeded().ContinueWith((t1) =>
                        {
                            if (!t1.Result)
                            {
                                result = messageReporter.ConnectionToRegServerFailed();
                                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, result, false);
                            }
                            else
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append("REG ");
                                sb.Append(name + " ");
                                sb.Append(pass + " ");
                                sb.Append(mail);
                                //clientConnectionContext.output.WriteLine("bullshit");
                                //clientConnectionContext.output.Flush();
                                clientConnectionContext.output.WriteLineAsync(sb.ToString()).ContinueWith(_ =>
                                {
                                    clientConnectionContext.output.FlushAsync().ContinueWith(__ =>
                                    {
                                        clientConnectionContext.input.ReadLineAsync().ContinueWith((t2) =>
                                        {
                                            if (t2.Result.StartsWith("ERR"))
                                            {
                                                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, t2.Result.Substring(3, t2.Result.Length-3), false);
                                            }
                                            else
                                            {
                                                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, t2.Result.Substring(3, t2.Result.Length - 3), true);
                                            }
                                        });
                                    });
                                });
                            }
                        });
                    }
                });
            }
            catch (Exception ex)
            {
                mainWindow.mainPage.regPage.Dispatcher.BeginInvoke(ButtonClickResult, "some weird error", false);
            }
        }
     * */

    /*
     *     public void ValidateKeyAsync(string key)
        {
            ButtonClickResult = new Action<string, bool>(mainWindow.mainPage.valPage.ValidateKeyOperationResult);
            try
            {
                Task.Factory.StartNew(() =>
                {
                    string result = null;
                    if (key == "")
                    {
                        result = messageReporter.FieldEmpty();
                        mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, result, false);
                    }
                    else
                    {
                        ConnectIfNeeded().ContinueWith((t1) =>
                        {
                            if (!t1.Result)
                            {
                                result = messageReporter.ConnectionToRegServerFailed();
                                mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, result, false);
                            }
                            else
                            {
                                clientConnectionContext.output.WriteLineAsync("VAL " + key).ContinueWith(_ =>
                                {
                                    clientConnectionContext.output.FlushAsync().ContinueWith(__ =>
                                    {
                                        clientConnectionContext.input.ReadLineAsync().ContinueWith((t2) =>
                                        {
                                            string[] tokens = t2.Result.Split(' ');
                                            if (tokens[0] == "ERR")
                                            {
                                                mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, t2.Result.Substring(3, t2.Result.Length - 3), false);
                                            }
                                            else
                                            {
                                                mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, t2.Result.Substring(3, t2.Result.Length - 3), true);
                                            }
                                        });
                                    });
                                });
                            }
                        });
                    }
                });
            }
            catch (Exception e)
            {
                mainWindow.mainPage.valPage.Dispatcher.BeginInvoke(ButtonClickResult, "some weird error2", false);

            }
            //ButtonClickResult = new Action<string, bool>(mainWindow.mainPage.valPage
        }
     * */

    #endregion
}
