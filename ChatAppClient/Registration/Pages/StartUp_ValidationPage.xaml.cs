﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChatAppClient
{
    /// <summary>
    /// Interaction logic for StartUp_ValidationPage.xaml
    /// </summary>
    public partial class StartUp_ValidationPage : Page
    {
        StartUp_MainContext mainContext;
        MainWindow mainWindow;
        StartUp_MainPage mainPage;

        public StartUp_ValidationPage(StartUp_MainContext mainContext, MainWindow mainWindow, StartUp_MainPage mainPage)
        {
            this.mainPage = mainPage;
            this.mainWindow = mainWindow;
            this.mainContext = mainContext;
            InitializeComponent();
        }

        public StartUp_ValidationPage()
        {
            InitializeComponent();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.MainContent.Content = mainPage;
        }

        public void ValidateKeyOperationResult(string message, bool result)
        {
            ValidateKeyStatus.Content = "Status: " + message;
            ValidateKeyButton.IsEnabled = true;
            BackButton.IsEnabled = true;

            if (result)
                ValidateKeyStatus.Foreground = Brushes.Green;
            else
                ValidateKeyStatus.Foreground = Brushes.Red;
        }

        private void ValidateKeyButton_Click(object sender, RoutedEventArgs e)
        {
            ValidateKeyStatus.Content = "Status: Operation is pending, please wait";
            ValidateKeyStatus.Foreground = Brushes.Purple;
            ValidateKeyStatus.Visibility = Visibility.Visible;
            mainContext.ValidateKeyAsync(ValidateKeyTextBox.Text);
            ValidateKeyButton.IsEnabled = false;
            BackButton.IsEnabled = false;

        }
    }
}
