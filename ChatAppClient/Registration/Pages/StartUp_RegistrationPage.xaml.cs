﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChatAppClient
{
    /// <summary>
    /// Interaction logic for StartUp_RegistrationPage.xaml
    /// </summary>
    public partial class StartUp_RegistrationPage : Page
    {
        StartUp_MainContext mainContext;
        MainWindow mainWindow;
        StartUp_MainPage mainPage;
        StartUp_ValidationPage valPage;

        public StartUp_RegistrationPage(StartUp_MainContext mainContext, MainWindow mainWindow, 
            StartUp_MainPage mainPage, StartUp_ValidationPage valPage)
        {
            this.mainContext = mainContext;
            this.mainWindow = mainWindow;
            this.mainPage = mainPage;
            this.valPage = valPage;
            InitializeComponent();
        }

        public StartUp_RegistrationPage()
        {
            InitializeComponent();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.MainContent.Content = mainPage;
        }

        private void ValidationButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindow.MainContent.Content = valPage;
        }

        private void CreateAccountButton_Click(object sender, RoutedEventArgs e)
        {
            CreateAccountStatus.Content = "Status: Operation is pending, please wait";
            CreateAccountStatus.Foreground = Brushes.Purple;
            CreateAccountStatus.Visibility = Visibility.Visible;
            mainContext.CreateAccountAsync(LoginTextBox.Text, PasswordBox1.Password, PasswordBox2.Password, EmailTextBox.Text);
            CreateAccountButton.IsEnabled = false;
            ValidationButton.IsEnabled = false;
            BackButton.IsEnabled = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="result"> true if operation was successful - green text
        /// false if wasn't - red text</param>
        public void CreateAccountOperationResult(string message, bool result)
        {
            CreateAccountStatus.Content = "Status: " + message;
            CreateAccountButton.IsEnabled = true;
            ValidationButton.IsEnabled = true;
            BackButton.IsEnabled = true;

            if (result)
                CreateAccountStatus.Foreground = Brushes.Green;
            else
                CreateAccountStatus.Foreground = Brushes.Red;
        }
    }
}
