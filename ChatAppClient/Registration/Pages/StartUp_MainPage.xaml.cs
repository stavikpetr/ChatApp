﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatAppCommon;
using System.Net.Sockets;

namespace ChatAppClient
{
    /// <summary>
    /// Interaction logic for StartUp_MainPage.xaml
    /// </summary>
    public partial class StartUp_MainPage : Page
    {
        StartUp_MainContext mainContext;
        MainWindow mainWindow;
        public StartUp_RegistrationPage regPage { get; set; }
        public StartUp_ValidationPage valPage { get; set; }

        public StartUp_MainPage()
        {
            InitializeComponent();
        }

        public StartUp_MainPage(StartUp_MainContext mainContext, MainWindow mainWindow)
        {
            this.mainContext = mainContext;
            this.mainWindow = mainWindow;
            valPage = new StartUp_ValidationPage(mainContext, mainWindow, this);
            regPage = new StartUp_RegistrationPage(mainContext, mainWindow, this, valPage);
            InitializeComponent();
        }

        private void RegistrationButton_Click(object sender, RoutedEventArgs e)
        {
            
            mainWindow.MainContent.Content = regPage;
        }

        private void ValidationButton_Click(object sender, RoutedEventArgs e)
        {
            if (valPage == null)
            {
                valPage = new StartUp_ValidationPage(mainContext, mainWindow, this);
            }
            mainWindow.MainContent.Content = valPage;
        }

        private void ServerComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            ServerComboBox.ItemsSource = CommonData.chatServerPortValues_String.Keys.ToList();
            ServerComboBox.SelectedIndex = 0;
        }

        private void PortComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            string val = (string)ServerComboBox.SelectedItem;
            var x = CommonData.chatServerPortValues_String[val].ToList();
            List<string> data = new List<string>();
            foreach (var item in x)
            {
                data.Add(item.ToString());
            }
            PortComboBox.ItemsSource = data;
            PortComboBox.SelectedIndex = 0;
        }

        private void ServerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var val = (string)ServerComboBox.SelectedItem;
            var x = CommonData.chatServerPortValues_String[val].ToList();
            List<string> data = new List<string>();
            foreach (var item in x)
            {
                data.Add(item.ToString());
            }
            PortComboBox.ItemsSource = data;
            PortComboBox.SelectedIndex = 0;
        }

        public void JoinAsUserOperationResult(string message, IConnectionContext connectionContext, string serverAlias)
        {
            JoinAsUserStatus.Content = "Status: " + message;
            JoinAsAnonButton.IsEnabled = true;
            JoinAsUserButton.IsEnabled = true;
            RegistrationButton.IsEnabled = true;
            ValidationButton.IsEnabled = true;
            if (connectionContext != null)
            {
                JoinAsUserStatus.Foreground = Brushes.Green;
                mainWindow.SpawnChattingWindow(connectionContext, serverAlias);
            }
            else
            {
                JoinAsUserStatus.Foreground = Brushes.Red;
            }
        }

        public void JoinAsAnonOperationResult(string message, IConnectionContext connectionContext,string serverAlias)
        {
            JoinAsAnonStatus.Content = "Status: " + message;
            JoinAsAnonButton.IsEnabled = true;
            JoinAsUserButton.IsEnabled = true;
            RegistrationButton.IsEnabled = true;
            ValidationButton.IsEnabled = true;
            if (connectionContext != null)
            {
                JoinAsAnonStatus.Foreground = Brushes.Green;
                mainWindow.SpawnChattingWindow(connectionContext, serverAlias);
            }
            else
            {
                JoinAsAnonStatus.Foreground = Brushes.Red;
            }
        }

        private void JoinAsUserButton_Click(object sender, RoutedEventArgs e)
        {
            JoinAsUserStatus.Content = "Status: Operation is pending, please wait";
            JoinAsUserStatus.Foreground = Brushes.Purple;
            JoinAsUserStatus.Visibility = Visibility.Visible;
            mainContext.TryConnectAsUserAsync(LoginTextBox.Text, PasswordTextBox.Password, (string)ServerComboBox.SelectedItem, Int32.Parse((string)PortComboBox.SelectedItem));
            JoinAsAnonButton.IsEnabled = false;
            JoinAsUserButton.IsEnabled = false;
            RegistrationButton.IsEnabled = false;
            ValidationButton.IsEnabled = false;
        }

        private void JoinAsAnonButton_Click(object sender, RoutedEventArgs e)
        {
            JoinAsAnonStatus.Content = "Status: Operation is pending, please wait";
            JoinAsAnonStatus.Foreground = Brushes.Purple;
            JoinAsAnonStatus.Visibility = Visibility.Visible;
            mainContext.TryConnectAsAnonAsync((string)ServerComboBox.SelectedItem, Int32.Parse((string)PortComboBox.SelectedItem));
            JoinAsAnonButton.IsEnabled = false;
            JoinAsUserButton.IsEnabled = false;
            RegistrationButton.IsEnabled = false;
            ValidationButton.IsEnabled = false;
        }
    }
}
