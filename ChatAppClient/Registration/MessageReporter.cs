﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAppClient
{
    class MessageReporter
    {
        MessageReporter() { }
        static MessageReporter mr = new MessageReporter();
        internal static MessageReporter messageReporter { get { return mr; } }
        

        const string fieldEmpty = "Error, one or more fields are empty";
        const string connectionToRegServerFailed = "Error, problem with connection to server";
        const string notValidMailString = "Error, incorrect mail format";
        const string passwordsAreDifferentError = "Error, passwords are different";

        const string cantOpenConnection = "Connection error, please try again later";

        public string CantOpenConnection()
        {
            return cantOpenConnection;
        }

        public string FieldEmpty()
        {
            return fieldEmpty;
        }

        public string NotValidMailString()
        {
            return notValidMailString;
        }

        public string PasswordsAreDifferentError()
        {
            return passwordsAreDifferentError;
        }

        public string ConnectionToRegServerFailed()
        {
            return connectionToRegServerFailed;
        }
    }
}
