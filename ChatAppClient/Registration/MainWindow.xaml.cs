﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using ChatAppClient.Chatting;
namespace ChatAppClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// druhá polovina partial třídy která se vygeneruje z toho xaml parseru, kde ta druhá půlka
    /// mě nemusí zajímat
    public partial class MainWindow : Window
    {
        public StartUp_MainPage mainPage { get; set; }

        StartUp_MainContext mainContext;

        public void SpawnChattingWindow(IConnectionContext connectionContext, string serverAlias)
        {
            //Thread t = new Thread(() =>
            //{
            //    MainWindowChatting mainWindow = new MainWindowChatting(connectionContext, serverAlias);
            //    mainWindow.Show();
            //    System.Windows.Threading.Dispatcher.Run();
            //});
            //t.SetApartmentState(ApartmentState.STA);
            //t.Start();
            
            MainWindowChatting mainWindow = new MainWindowChatting(connectionContext, serverAlias);
            mainWindow.Show();
            this.Close();
        }

        public MainWindow()
        {
            mainContext = new StartUp_MainContext(this);
            mainPage = new StartUp_MainPage(mainContext, this);
            InitializeComponent();
        }

        private void MainContent_Loaded(object sender, RoutedEventArgs e)
        {
            MainContent.Content = mainPage;
        }
    }
}
